<?php


require __DIR__ . '/paths.php';
require ROOT . DS . 'vendor' . DS . 'autoload.php';
require CORE_PATH . 'config' . DS . 'bootstrap.php';
if (!extension_loaded('intl')) {
    trigger_error('You must enable the intl extension to use CakePHP.', E_USER_ERROR);
}

use Cake\Cache\Cache;
use Cake\Console\ConsoleErrorHandler;
use Cake\Core\App;
use Cake\Core\Configure;
use Cake\Core\Configure\Engine\PhpConfig;
use Cake\Core\Plugin;
use Cake\Database\Type;
use Cake\Datasource\ConnectionManager;
use Cake\Error\ErrorHandler;
use Cake\Log\Log;
use Cake\Mailer\Email;
use Cake\Network\Request;
use Cake\Routing\DispatcherFactory;
use Cake\Utility\Inflector;
use Cake\Utility\Security;
use Cake\Routing\Router;

use Cake\I18n\I18n;
use Cake\I18n\Time;
use Cake\I18n\FrozenTime;
use Cake\I18n\Date;
use Cake\I18n\FrozenDate;


Router::extensions(['json']);


try {
    Configure::config('default', new PhpConfig());
    Configure::load('app', 'default', false);
} catch (\Exception $e) {
    die($e->getMessage() . "\n");
}


date_default_timezone_set('UTC');

mb_internal_encoding(Configure::read('App.encoding'));

$isCli = PHP_SAPI === 'cli';
if ($isCli) {
    (new ConsoleErrorHandler(Configure::read('Error')))->register();
} else {
    (new ErrorHandler(Configure::read('Error')))->register();
}

if ($isCli) {
    require __DIR__ . '/bootstrap_cli.php';
}

if (!Configure::read('App.fullBaseUrl')) {
    $s = null;
    if (env('HTTPS')) {
        $s = 's';
    }

    $httpHost = env('HTTP_HOST');
    if (isset($httpHost)) {
        Configure::write('App.fullBaseUrl', 'http' . $s . '://' . $httpHost);
    }
    unset($httpHost, $s);
}

Cache::setConfig(Configure::consume('Cache'));
ConnectionManager::setConfig(Configure::consume('Datasources'));
Email::setConfigTransport(Configure::consume('EmailTransport'));
Email::setConfig(Configure::consume('Email'));
Log::setConfig(Configure::consume('Log'));
Security::setSalt(Configure::consume('Security.salt'));

Request::addDetector('mobile', function ($request) {
    $detector = new \Detection\MobileDetect();
    return $detector->isMobile();
});
Request::addDetector('tablet', function ($request) {
    $detector = new \Detection\MobileDetect();
    return $detector->isTablet();
});

//Plugin::loadAll();
Plugin::load('ADmad/I18n');

if (Configure::read('debug') == 1) {
    Plugin::load('DebugKit', ['bootstrap' => true]);
}

 
DispatcherFactory::add('Asset');
DispatcherFactory::add('Routing');
DispatcherFactory::add('ControllerFactory');

ini_set('intl.default_locale', 'en');
 
//Type::build('date')->useLocaleParser();
//Type::build('datetime')->useLocaleParser();

Type::build('time')
    ->useImmutable()
    ->useLocaleParser();
Type::build('date')
    ->useImmutable()
    ->useLocaleParser();
Type::build('datetime')
    ->useImmutable()
    ->useLocaleParser();



Configure::write('AdminRoles', ['admin.admin'=>'admin.admin', 'admin.editor_ar'=>'admin.editor_ar', 'admin.editor_en'=>'admin.editor_en', 'admin.editor_tr'=>'admin.editor_tr', 'user.free'=>'user.free', 'user.paid'=>'user.paid']);
Configure::write('UserRoles', ['user.free'=>'user.free', 'user.paid'=>'user.paid']);
Configure::write('CURRENCIES', [1=>'USD', 2=>'SAR', 3=>'TL']);
Configure::write('LANGUAGES_IDS', [1=>'ar', 2=>'en']);
Configure::write('LANGUAGES_MENU', [1=>__('ar'), 2=>__('tr')]);
Configure::write('I18n.languages', ['en', 'ar', 'tr']);
Configure::write('app_folder', strpos($_SERVER['HTTP_HOST'], "localhost") === false ? '/' : '/turkeyde');
Configure::write('lcl_langs', [
	"ar"=>["الشركات-في-تركيا", "الاستيراد-من-تركيا", "articles"],
	"en"=>["companies-in-turkey", "import-from-turkey", "articles"],
	"tr"=>["türk-şirketleri", "ithalat-türkiyeden", "articles"]
	]);

$lng = explode("/", str_replace(Configure::read('app_folder'),"", $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']) );
@Configure::write('pages_name', Configure::read('lcl_langs')[$lng[1]]);
	
Configure::write('ROLES', [
	'user.free'=>[
		'articles'=>['create'=>0, 'read'=>1, 'update'=>0, 'delete'=>0, 'allids'=>0],
		'companies'=>['create'=>1, 'read'=>1, 'update'=>1, 'delete'=>0, 'allids'=>0],
		'requests'=>['create'=>1, 'read'=>1, 'update'=>1, 'delete'=>0, 'allids'=>0],
		'users'=>['create'=>1, 'read'=>1, 'update'=>1, 'delete'=>0, 'allids'=>0],
		'contacts'=>['create'=>1, 'read'=>1, 'update'=>1, 'delete'=>0, 'allids'=>0],
	], 
	'user.paid'=>[
		'articles'=>['create'=>0, 'read'=>1, 'update'=>0, 'delete'=>0, 'allids'=>0],
		'companies'=>['create'=>1, 'read'=>1, 'update'=>1, 'delete'=>0, 'allids'=>0],
		'requests'=>['create'=>1, 'read'=>1, 'update'=>1, 'delete'=>0, 'allids'=>0],
		'users'=>['create'=>1, 'read'=>1, 'update'=>1, 'delete'=>0, 'allids'=>0],
		'contacts'=>	['create'=>1, 'read'=>1, 'update'=>1, 'delete'=>0, 'allids'=>0],
	], 
	'admin.root'=>[
		'articles'=>	['create'=>1, 'read'=>1, 'update'=>1, 'delete'=>1, 'allids'=>1],
		'companies'=>	['create'=>1, 'read'=>1, 'update'=>1, 'delete'=>1, 'allids'=>1],
		'requests'=>	['create'=>1, 'read'=>1, 'update'=>1, 'delete'=>1, 'allids'=>1],
		'categories'=>  ['create'=>1, 'read'=>1, 'update'=>1, 'delete'=>1, 'allids'=>1],
		'users'=>	    ['create'=>1, 'read'=>1, 'update'=>1, 'delete'=>1, 'allids'=>1],
		'contacts'=>	['create'=>1, 'read'=>1, 'update'=>1, 'delete'=>0, 'allids'=>0],
		'assignments'=>	['create'=>1, 'read'=>1, 'update'=>1, 'delete'=>0, 'allids'=>0],
	],
	'admin.admin'=>[
		'articles'=>	['create'=>1, 'read'=>1, 'update'=>1, 'delete'=>0, 'allids'=>1],
		'companies'=>	['create'=>1, 'read'=>1, 'update'=>1, 'delete'=>0, 'allids'=>1],
		'requests'=>	['create'=>1, 'read'=>1, 'update'=>1, 'delete'=>0, 'allids'=>1],
		'categories'=>  ['create'=>1, 'read'=>1, 'update'=>1, 'delete'=>0, 'allids'=>1],
		'users'=>	    ['create'=>1, 'read'=>1, 'update'=>1, 'delete'=>0, 'allids'=>1],
		'contacts'=>	['create'=>1, 'read'=>1, 'update'=>1, 'delete'=>0, 'allids'=>0],
		'assignments'=>	['create'=>1, 'read'=>1, 'update'=>1, 'delete'=>0, 'allids'=>0],
	],
	'admin.editor_ar'=>[
		'articles'=>	['create'=>1, 'read'=>1, 'update'=>1, 'delete'=>0, 'allids'=>0],
		'companies'=>	['create'=>1, 'read'=>1, 'update'=>1, 'delete'=>0, 'allids'=>0],
		'requests'=>	['create'=>1, 'read'=>1, 'update'=>1, 'delete'=>0, 'allids'=>0],
		'categories'=>  ['create'=>1, 'read'=>1, 'update'=>1, 'delete'=>0, 'allids'=>0],
		'users'=>	    ['create'=>1, 'read'=>1, 'update'=>1, 'delete'=>0, 'allids'=>0],
		'contacts'=>	['create'=>1, 'read'=>1, 'update'=>1, 'delete'=>0, 'allids'=>0],
		'assignments'=>	['create'=>1, 'read'=>1, 'update'=>1, 'delete'=>0, 'allids'=>0],
	], 
	'admin.editor_en'=>[
		'articles'=>	['create'=>0, 'read'=>0, 'update'=>0, 'delete'=>0, 'allids'=>0],
		'companies'=>	['create'=>1, 'read'=>1, 'update'=>1, 'delete'=>0, 'allids'=>0],
		'requests'=>	['create'=>1, 'read'=>1, 'update'=>1, 'delete'=>0, 'allids'=>0],
		'categories'=>  ['create'=>1, 'read'=>1, 'update'=>1, 'delete'=>0, 'allids'=>0],
		'users'=>	    ['create'=>1, 'read'=>1, 'update'=>1, 'delete'=>0, 'allids'=>0],
		'contacts'=>	['create'=>1, 'read'=>1, 'update'=>1, 'delete'=>0, 'allids'=>0],
		'assignments'=>	['create'=>1, 'read'=>1, 'update'=>1, 'delete'=>0, 'allids'=>0],
	], 
	'admin.editor_tr'=>[
		'articles'=>	['create'=>1, 'read'=>1, 'update'=>1, 'delete'=>0, 'allids'=>0],
		'companies'=>	['create'=>1, 'read'=>1, 'update'=>1, 'delete'=>0, 'allids'=>1],
		'requests'=>	['create'=>1, 'read'=>1, 'update'=>1, 'delete'=>0, 'allids'=>1],
		'categories'=>  ['create'=>0, 'read'=>0, 'update'=>0, 'delete'=>0, 'allids'=>0],
		'users'=>	    ['create'=>0, 'read'=>0, 'update'=>0, 'delete'=>0, 'allids'=>0],
		'contacts'=>	['create'=>1, 'read'=>1, 'update'=>1, 'delete'=>0, 'allids'=>0],
		'assignments'=>	['create'=>1, 'read'=>1, 'update'=>1, 'delete'=>0, 'allids'=>0],
	], 
	]
);