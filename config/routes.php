<?php

use Cake\Core\Plugin;
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;
use Cake\Core\Configure;

Router::scope('/', function ($routes) {
    $routes->extensions(['json', 'xml', 'html']);
    $routes->resources('/login');
});

Router::defaultRouteClass(DashedRoute::class);

Router::scope('/', function (RouteBuilder $routes) {
	
	$pages_name = Configure::read('pages_name');
    //$routes->extensions(['json']);
    //$routes->resources('Users');
    $routes->connect('/', ['lang'=>'/:lang', 'controller' => 'Pages', 'action' => 'display', 'home'],
        ['routeClass' => 'ADmad/I18n.I18nRoute']);
    
    $routes->connect('/delphoto', ['lang'=>'/:lang', 'controller' => 'Companies', 'action' => 'delphoto'], ['routeClass' => 'ADmad/I18n.I18nRoute']);
    
    $routes->connect('/sharecounter', ['lang'=>'/:lang', 'controller' => 'Companies', 'action' => 'sharecounter'], ['routeClass' => 'ADmad/I18n.I18nRoute']);
    
	$routes->connect('/pages/*', ['lang'=>'/:lang', 'controller' => 'Pages', 'action' => 'display'],
        ['routeClass' => 'ADmad/I18n.I18nRoute']);
    
	$routes->connect('/login', ['lang'=>'/:lang', 'controller' => 'Users', 'action' => 'login'],
        ['routeClass' => 'ADmad/I18n.I18nRoute']);
	
	$routes->connect('/logout', ['lang'=>'/:lang', 'controller' => 'Users', 'action' => 'logout'],
        ['routeClass' => 'ADmad/I18n.I18nRoute']);
	
	$routes->connect('/register', ['lang'=>'/:lang', 'controller' => 'Users', 'action' => 'register'],
        ['routeClass' => 'ADmad/I18n.I18nRoute']);
    
	$routes->connect('/getpassword', ['lang'=>'/:lang', 'controller' => 'Users', 'action' => 'getpassword'],
        ['routeClass' => 'ADmad/I18n.I18nRoute']);
	
	$routes->connect('/dashboard', ['lang'=>'/:lang', 'controller' => 'Users', 'action' => 'dashboard'],
        ['routeClass' => 'ADmad/I18n.I18nRoute']);
	
	$routes->connect('/myaccount', ['lang'=>'/:lang', 'controller' => 'Users', 'action' => 'edit' ],
        ['routeClass' => 'ADmad/I18n.I18nRoute']);
	
	$routes->connect('/mycompany', ['lang'=>'/:lang', 'controller' => 'Companies', 'action' => 'edit' ], ['routeClass' => 'ADmad/I18n.I18nRoute']);
	
	$routes->connect('/packages', ['lang'=>'/:lang', 'controller' => 'Users', 'action' => 'packages' ], ['routeClass' => 'ADmad/I18n.I18nRoute']);
	
	
	
	
	$routes->connect('/'.$pages_name[1].'/:slug', ['lang'=>'/:lang', 'controller' => 'Requests', 'action' => 'view'], 
		['routeClass' => 'ADmad/I18n.I18nRoute' , 'pass'=>['slug'] ]);
	
	$routes->connect('/'.$pages_name[1], ['lang'=>'/:lang', 'controller' => 'Requests', 'action' => 'index'],
        ['routeClass' => 'ADmad/I18n.I18nRoute']);
		
	$routes->connect('/'.$pages_name[0].'/:slug', ['lang'=>'/:lang', 'controller' => 'Companies', 'action' => 'view'], 
		['routeClass' => 'ADmad/I18n.I18nRoute',  'pass'=>['slug'] ]);
		
	$routes->connect('/'.$pages_name[0], ['lang'=>'/:lang', 'controller' => 'Companies', 'action' => 'index'], ['routeClass' => 'ADmad/I18n.I18nRoute']);

		
		
		
	$routes->connect('/companies/add', ['lang'=>'/:lang', 'controller' => 'Companies', 'action' => 'add'],
        ['routeClass' => 'ADmad/I18n.I18nRoute']);
	
	$routes->connect('/articles', ['lang'=>'/:lang', 'controller' => 'Articles', 'action' => 'index'], 
		['routeClass' => 'ADmad/I18n.I18nRoute' ]);
	
	$routes->connect('/articles/:slug', ['lang'=>'/:lang', 'controller' => 'Articles', 'action' => 'view'], 
		['pass'=>['slug'], 'routeClass' => 'ADmad/I18n.I18nRoute' ]);
	
	$routes->connect('/requests/mylist', ['lang'=>'/:lang', 'controller' => 'Requests', 'action' => 'mylist'],
        ['routeClass' => 'ADmad/I18n.I18nRoute']);
	
	$routes->connect('/requests/edit/:id', ['lang'=>'/:lang', 'controller' => 'Requests', 'action' => 'edit'],
        ['routeClass' => 'ADmad/I18n.I18nRoute' ,'pass' => ['id']]);
	
	$routes->connect('/requests/add', ['lang'=>'/:lang', 'controller' => 'Requests', 'action' => 'add'],
        ['routeClass' => 'ADmad/I18n.I18nRoute']);
    
	$routes->connect('/users/activate/:id', ['lang'=>'/:lang', 'controller' => 'Users', 'action' => 'activate'], ['pass'=>['id'], 'routeClass' => 'ADmad/I18n.I18nRoute']);
	
	
	$routes->fallbacks('DashedRoute');
    //$routes->fallbacks(DashedRoute::class);
});

Router::prefix('admin', function ($routes) {
	$routes->connect('/', ['prefix' => 'admin', 'controller' => 'Users', 'action'=>'dashboard']);
	$routes->connect('/dashboard', ['prefix' => 'admin', 'controller' => 'Users', 'action'=>'dashboard']);
    
    $routes->connect('/delphoto', ['prefix' => 'admin', 'controller' => 'Companies', 'action' => 'delphoto']);
    $routes->fallbacks('InflectedRoute');
});
Plugin::routes();
