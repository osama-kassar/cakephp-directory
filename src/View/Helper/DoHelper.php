<?php

namespace App\View\Helper;

use Cake\View\Helper;
use Cake\ORM\TableRegistry;
use Cake\Utility\Security;
//use Cake\I18n\I18n;
use Cake\Core\Configure;
use Cake\View\Helper\HtmlHelper;

class DoHelper extends Helper{
	
	public function get($id){
		
		switch($id){
			case 'lang':
				$cats = Configure::read('I18n.languages');
				break;
				
				
			case 'prefered_langs':
				$cats = ['ar', 'en', 'tr'];
				break;
				
			case 'prefered_contacts':
				$cats = ['whatsapp', 'phone', 'email'];
				break;
                
			case 'days':
				$cats = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
				break;
				
			case 'contact_reason':
				$cats = ["question"=>__("question"), "buy_request"=>__("buy_request"), "complain"=>__("complain"), "other"=>__("other")];
				break;
				
			case 'stat_priority':
				$cats = ["1"=>__("not_read"), "2"=>__("read"), "3"=>__("important")];
				break;
				
			case 'stat_type':
				$cats = ["1"=>__("send"), "2"=>__("response")];
				break;
				
			case 'filters':
				$cats = ['country', 'category', 'subcategory', 'keyword'];
				break;
				
			case 'socialmedia':
				$cats = ['whatsapp', 'facebook', 'twitter', 'linkedin', 'instagram'];
				break;
				
			case 'UserRoles':
				$cats = Configure::read('UserRoles');
				break;
				
			case 'AdminRoles':
				$cats = Configure::read('AdminRoles');
				break;
				
			case 'sitelangs':
				$cats = Configure::read('I18n.languages');
				break;
				
			case 'currlang':
				$cats = I18n::locale();
				break;
				
			case 'currlang_id':
				$cats = array_flip(Configure::read('LANGUAGES_IDS'))[ I18n::locale() ];
				break;
			
			case 'localizedTables':
				$cats = array('products'=>__('products'), 'offers'=>__('offers'), 'categories'=>__('categories'));
				break;
			
			case 'parents':
				$cats = $this->Categories->find('list',['conditions'=>['parent_id'=>0], 'fields'=>['id','category_name']]);
				break;
		}
		return $cats;
	}
	
	
	public function getMachineLang($val){
		foreach(Configure::read('I18n.languages') as $lang){
			if(strpos($val, $lang) !== false){
				return $lang;
			}
		}
	}
    
	public function getCatName($param){
		if( is_numeric($param[1]) && $param[0] !== 'keyword'){
			$this->Categories = TableRegistry::get('Categories');
			$res = $this->Categories->find('list',['conditions'=>['id'=>$param[1]] ])->toList();
			return __($res[0]);
		}
		return urldecode (__($param[1]));
	}
	
	public function adrs($text){
		$AddHtmlEnd = false;
		$text = trim ($text);
		$text = preg_replace('{(.)\1+}','$1',$text);
		$text = preg_replace('/ /', '_', $text);
		$text = preg_replace('/أ|إ|آ/', 'ا', $text);
		$text = preg_replace('/~/', '_', $text);
		$text = preg_replace('/`|\!|\@|\#|\^|\&|\(|\)|\|/', '', $text);
		$text = preg_replace('/{|}|\[|\]/', '_', $text);
		$text = preg_replace('/\+|\-|\*|\/|\=|\%|\$|×/', '', $text);
		$text = preg_replace('/,|\.|\'|;|\?|\<|\>|"|:/', '', $text);
		$text = preg_replace('/^_/', '', $text);
		$text = preg_replace('/_$/', '', $text);
		$text = preg_replace('/_/', '-', $text);
		if ($AddHtmlEnd) {
			$text .= '.html';
		}
		return $text;
	}
	
	public function appendURL($var, $cond=false){
		$url_arr = @explode('?', $_SERVER['REQUEST_URI']);
		$url = @explode('&', $url_arr[1]) ;
		if (empty($url)) {  return false;  }
		if($cond == 'array'){
			return $url;
		}
		if($cond == 'remove'){
			$ind = array_search($var, $url);
			unset($url[$ind]);
		}else{
			array_push($url, $var);
		}
		$newurl=[]; $complex_arr=[];
		foreach($url as $uVal){
			$ind = @explode('=',$uVal);
			if(!empty($ind[0])){
				$complex_arr[$ind[0]] = [ $ind[0], $ind[1] ];
			}
		}
		
		foreach($complex_arr as $itm){
			$newurl[]= $itm[0].'='.$itm[1] ;
		}
		
		if($cond == 'params'){
			return '?'.implode('&',$newurl);
		}
		return $url_arr[0].'?'.implode('&',$newurl);
	}
	
	public function lcl($arr, $isObj=false){
        if(!is_array($arr)){ return __($arr, true); }
		$res=[]; 
		foreach($arr as $k=>$v){
			$res[$k] = __($v, true);
		}
		return $res;
	}
    
	/*public function SortableLink($title, $url, $fieldName ,$var){
		if($var['orderdir'] == 'DESC'){
			$var['orderdir'] = 'ASC';
		}else{
			$var['orderdir'] = 'DESC';
		}
		$var['orderby'] = $fieldName;
		$url = $this->appendURL ($url, $var);
		return sprintf ('<a href="%s">%s</a>', $url, $title);
	}*/
	
	/*public function paging($total, $type='numbers', $separator=' ', $perpage=PPAGE){
		$prms_arr=array();
		$url = $this->here.'?';
		if(!empty($_GET)){
			foreach($_GET as $key => $val){
				if($key !== 'p'){
					$prms_arr[] =$key.'='.$val;
				}
			}
			if(!empty($prms_arr)){
				$url .= implode('&',$prms_arr) . '&';
			}
		}
		!isset($_GET['p']) ? $_GET['p']=1 : 1==1;
		if($total>$perpage){
			for($i=0; $i<floor($total / $perpage); $i++){
				if($_GET['p'] == ($i+1)){
					echo '<span>'.($i+1).'</span>'.$separator;
				}else{
					echo '<a href='.$url.'p='.($i+1).'><span>'.($i+1).'</span></a>'.$separator;
				}
			}
		}
	}*/
	
	/*public function setLen($txt, $len=40){
		if(strlen($txt) > $len){
			strpos($txt, ' ', $len) > 0 ? $spacepos = strpos($txt, ' ', $len) : $spacepos = strrpos($txt, ' ');
			$newtxt = substr($txt, 0, $spacepos).'...';
			return $newtxt;
		}else{
			return $txt.'...';
		}
	}*/
	
	public function captcha($tar, $type='alphaNumeric', $chars = 4){
		$salt=Security::salt();
		$canvas_w = $chars * 20;
		
		echo '<table style="width:auto;"><tr><td>';
		echo "<div id='inp".$tar."'></div>";
		echo $this->_View->Form->input('CaptchaCode', 
			['id'=>'CaptchaCode', 'name'=>'CaptchaCode', 'style'=>'width:100px;display:block-inline;', 'label'=>__('CaptchaCode'), 'maxlength'=>$chars]);
			/*
			<label for='CaptchaCode'>".__('CaptchaCode')."</label>
			
			<div class='input text required'>
			<input type='text' id='CaptchaCode' name='CaptchaCode' maxlength=".$chars." style='width:100px; display:block-inline;'>
			*/
		echo "</td><td><br>
			<a href=\"javascript:void();\" onclick=\"setCaptcha('".$salt."', '".$tar."','".$type."', ".$chars.");\" title='".__('click_to_refresh')."'>
				<canvas width='".$canvas_w."' height='30' id='can".$tar."' style='direction:ltr; display:block-inline;'></canvas>
			
			</a></td></tr></table><div style='height:10px'></div>
			
			
			<script> 
			$( document ).ready(function() {
			  setCaptcha('".$salt."', '".$tar."','".$type."', ".$chars.");
			});
			</script>";
	}
	
	
	public function num($num){
		return number_format(floor($num));
	}
	
	
	/*public function currencyCalc($from_id, $sum, $ext=true){				
		
			convrting conditions and rules
			
			USD -> SAR = N * SAR[c_rate]
			USD -> TRY = N * TRY[c_rate]
			
			TRY -> USD = ( USD[c_rate] / TRY[c_rate] ) * N
			SAR -> USD = ( USD[c_rate] / SAR[c_rate] ) * N
			
			TRY -> SAR = ( USD[c_rate] / TRY[c_rate] ) * ( N * SAR[c_rate] )
			SAR -> TRY = ( USD[c_rate] / SAR[c_rate] ) * ( N * TRY[c_rate] )
		
		$res = $sum;
		$to = $this->_View->viewVars['currency'];
		$currencies = $this->_View->viewVars['currencies'];
		$to_id = $unit = null;
		$currencies_arr=[];
		
		foreach($currencies as $v){
			if($v->c_name == $to){$to_id = $v->id;}
			if($v->c_name == 'USD'){$unit = $v;}
			$currencies_arr[$v->id]=$v;
		}
		
		if(($from_id == $to_id) == false){
			if($currencies_arr[$from_id]->c_name == 'USD'){
				$res = $sum * $currencies_arr[$to_id]->c_rate;
			}
			
			if($currencies_arr[$to_id]->c_name == 'USD'){
				$res = ($currencies_arr[ $unit->id ]->c_rate / $currencies_arr[$from_id]->c_rate) * $sum;
			}
			
			if($currencies_arr[$from_id]->c_name !== 'USD' && $currencies_arr[$to_id]->c_name !== 'USD'){

				$res = ($currencies_arr[ $unit->id ]->c_rate / $currencies_arr[$from_id]->c_rate) * ( $sum *  $currencies_arr[$to_id]->c_rate ) ;
			}
		}
		$ext ? $extension = ' '. __($to) : $extension = '';
		return $this->num($res).$extension;
	}*/
	
	public function readFolder($dir, $page=0){
		if(!empty($_GET['p'])){
			$page = $_GET['p'];
		}else{
			$page = 0;
		}
		$appf = $this->_View->viewVars['app_folder'];
		$res=[]; $inc=0; $offset=50;
		if (is_dir($dir)) {
			if (chdir($dir)) {
				array_multisort(array_map('filemtime', ($files = glob("*.*"))), SORT_DESC, $files);
				for ($i=($page*$offset); $i<($page+1)*$offset; $i++) {
					if(!empty($files[$i])){
						if($files[$i] != '..' && $files[$i] != '.' && $files[$i] != 'thumb'){
							$inc++;
							$res[] = $files[$i];
						}
					}
				}
				$pages = ceil(count($files) / $offset);
				echo "<div class='photoPaginator'>";
				for($j=0; $j<$pages; $j++){
					if($j == $page){
						echo '<a class="active">'.($j+1).'</a>';
					}else{
						echo '<a href="?p='.$j.'">'.($j+1).'</a>';
					}
				}
				echo "</div>";
				//krsort($res);
				//debug($res);
				echo '<div>';
				foreach($res as $photo){
					echo '
					<span>
						<a href="javascript:setLink(\''.$appf.'/'.$dir.$photo.'\');">
							<img src="'.$appf.'/'.$dir.$photo.'">
						</a>'.$photo.'
					</span>';
				}
				echo '</div>';
			}
		}else{
			echo 'not dir';
		}
	}
	
	public function tags($txt){
		$txtArr = explode(',',$txt);
		echo '<ul>';
		foreach($txtArr as $val){
			echo '<li>'.$this->_View->Html->link($val.' <i class="fa fa-tag" aria-hidden="true"></i>',
				["controller"=>"Articles", "action"=>"index", "keyword"=>$val],
				["escape"=>false]).'</li>';
		}
		echo '</ul>';
	}
	
}

?>