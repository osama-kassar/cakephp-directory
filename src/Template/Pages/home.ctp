<div id="slider-banner-section">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 text-center nopadding" >
				<div id="home-slider-item" class="hideMob">
					<h1><?=__('slogan')?></h1>
				</div>
				<div id="search-categorie-item-block">
					<form ng-submit="goTo('/<?=$currlang?>/'+pages_name[ fsearch.page ]+'?keyword='+fsearch.keyword+'&country='+fsearch.country)" novalidate="novalidate">
						<h1 ><?=__('what2search')?></h1><br />

						<!-- TASKS TAB --------------------------------->
						<div class="tabsStyle row">
							<ul class="nav nav-tabs">
								<li class="active">
									<a data-toggle="tab" href 
										ng-click="currTab='1';fsearch.page=pages_name.companies;searchResult={};">
										<?=__('companies')?></a>
								</li>
								<li>
									<a data-toggle="tab" href 
										ng-click="currTab='2';fsearch.page=pages_name.requests;searchResult={};">
										<?=__('requests')?></a></li>
								<li>
									<a data-toggle="tab" href 
										ng-click="currTab='3';fsearch.page=pages_name.articles;searchResult={};">
										<?=__('articles')?></a></li>
							</ul>
							
							<div class="tab-content col-xs-12">
							  <div id="search-input">
							  
								
								<div class="col-sm-5 col-xs-12 nopadding " ng-if="currTab == '2'">
									<?php echo $this->Form->input('category_id', 
									['type' => 'select', 
									'options' => @$countries, 'empty' => __('all_countries'), 'label'=>false, 
									'class'=>'form-control ', "ng-model"=>"fsearch.country"])?>
								</div>
								
								<div class="col-sm-5 col-xs-12 nopadding " ng-if="currTab == '1'">
									<?php echo $this->Form->input('category_id', 
									['type' => 'select', 
									'options' => @$countries_tr, 'empty' => __('turkey_cities'), 'label'=>false, 
									'class'=>'form-control ', "ng-model"=>"fsearch.country"])?>
								</div>
								
								<div class="{{currTab == '3' ? 'col-sm-10' : 'col-sm-5'}} col-xs-12 nopadding ">
									<input class="form-control"  
									ng-model="fsearch.keyword" ng-change="loadSearch()"
									placeholder="<?=__('add_word2search')?>" required>
									<div class="col-xs-12 autocomplete_div" ng-if="searchResult.length>0">
										<div ng-repeat="itm in searchResult" class="autocomplete_item">
											<a href ng-click="goTo('/<?=$currlang?>/'+fsearch.page +'/'+itm.slug);">
												{{itm[ fields[ currTab ] [1] ]}}
											</a>
										</div>
									</div>
								</div>
								
								<div id="location-search-btn" class="col-sm-2 col-xs-12 nopadding ">
									<button type="submit" id="search-btn ">
										<?=__('search')?>
										<i class="fa fa-search"></i>
									</button>
								</div>
								
							</div>
						</div>
					</form>
					
					
					<div class="row"  >
						<div class="col-xs-12" style="margin-top:5px; z-index:1">
							<div class="col-sm-6 col-xs-12 nopadding">
									<?php 
										echo $this->Html->link('
											  <button type="button" class="btn-quote" style="width:100%;">
												<i class="fa fa-plus-circle"></i>
											  '.__('add_your_request').'</button>',  
												['controller'=>'requests', 'action'=>'add'], ['escape'=>false])?>
								
							</div>
							<div class="col-sm-6 col-xs-12 nopadding">
									<?php 
										echo $this->Html->link('
										  <button type="button" class="btn-quote" style="width:100%">
										  <i class="fa fa-plus-circle"></i>
										  '.__('add_your_company').'</button>',  '/mycompany', ['escape'=>false])?>
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="sponsers">
	<div class="container">
		<div class=" row">
			<div class="col-xs-12 feature-item-listing-heading bt_heading_3">
				<h1>
					<?=__('thecompanies')?>
					<span>
					<?=__('sposers')?>
					</span></h1>
				<div class="blind line_1"></div>
				<div class="flipInX-1 blind icon"><span class="icon"><i class="fa fa-stop"></i>&nbsp;&nbsp;<i class="fa fa-stop"></i></span></div>
				<div class="blind line_2"></div>
			</div>
			
			<?php for($i=0; $i<6; $i++):?>
				<div class="col-xs-2 ">
					<div class="imgDiv">
						<?=$this->Html->image("/img/sponsers_photos/thumb/".($i+1).".png",["width"=>"100%"]);?>
					</div>
				</div>
			<?php endfor?>
		</div>
	</div>
</div>
