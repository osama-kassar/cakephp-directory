<style>

.photoPaginator a{
	border:1px solid var(--lightblue); 
	text-decoration:none;
	display:inline-block;
	margin:2px;
	height:20px;
	width:20px;
	line-height:20px;
	text-align:center;
}
.photoPaginator .active{
	background:var(--lightblue);
	font-weight:bold;
}
.photolist span{
	width:140px;
	height:110px;
	display:inline-block;
	overflow:hidden;
	margin:4px;
	border:1px solid var(--lightblue);
	font-size:10px;
}
.photolist img{
	width:100%;
	height:70px;
}
</style>
<div id="upload-wrapper" class="photolist">
	<?php echo $this->Do->readFolder("img/media/thumb/");?>   
</div>

<script>

/**************** add photo to CFEditor from selector *******************/
function setLink(url) {
	window.opener.CKEDITOR.tools.callFunction(1, url);
	window.close();
}
/**************** end add photo to CFEditor from selector *******************/
</script>