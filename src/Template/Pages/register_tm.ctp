
<br />
<br />

<div class="msg_tm"
style="
  @import url(http://fonts.googleapis.com/earlyaccess/droidarabickufi.css);
  font-family: 'Droid Arabic Kufi', serif;
  direction: rtl;
  text-align:center;
  max-width:1000px;
  margin:auto;
  padding:20px;
  border: 1px solid #ccc;"
>
    <header style="border-bottom:  1px solid #ccc;">
        <span style="width: 50%;text-align: left; float: left">
            <img src='http://<?=$_SERVER['HTTP_HOST'].$app_folder?>/webroot/images/logo.png' style="max-width:100%; width:250px;">
        </span>
        <span style="width: 50%;text-align: right; ">
            <h1><?=__('welcome').' '.$content['firstname']?></h1>
        </span>
    </header>
    
	<br />
	<br />
	<br />
	<div><?=__('register_email_msg')?></div>
	<br />
	<div><?=__('click_here_activiate')." <button style='
                        color:#386167; 
                        border:1px solid #386167; 
                        border-radius: 50px;
                        padding:4px 16px;'>".
			$this->Html->link(__('register_activition_link'),[
				'_full' => true,
				"controller"=>"Users", "action"=>"activate", $content['email_activate']
			])."</button>"?>
    </div>
    
	<br />
	<br />
	<br />
    
        <div><?=__('email_signature')?></div>
    
	<br />
    <footer style="text-align: left; border-top: 1px solid #ccc;">
        <div style="text-align: left;">
            TURYKEYDE © 2019 <br>
            ALTINKEY DIJITAL PAZARLAMA VE TİCARET LIMITES SIRKETI <br>
            Turkey - Istanbul - Beylikduzu 34524 
        </div>
        <div>
            <?=$this->Html->link(__('terms-conditions'),[
                    '_full' => true,
                    "controller"=>"Pages", "action"=>"display",  "terms-conditions"])?> 
            - 
            <?=$this->Html->link(__('privacy-policy'),[
                    '_full' => true,
                    "controller"=>"Pages", "action"=>"display",  "privacy-policy"])?>
        </div>
    </footer>
</div>
	<br />
	<br />