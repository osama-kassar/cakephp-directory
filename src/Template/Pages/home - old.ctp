<div id="slider-banner-section">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 text-center nopadding" >
				<div id="home-slider-item" class="hideMob">
					<span class="helpyou_item">We <span>Help</span> You to</span>
					<h1>AMAZING <span>CLASSIFIED</span> HTML <span>TEMPLATE</span></h1>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit...</p>
				</div>
				<div id="search-categorie-item-block">
					<form ng-submit="goTo('/'+fsearch.page+'?keyword='+fsearch.keyword+'&country='+fsearch.country)" novalidate="novalidate">
						<h1>search any business listing</h1>
						
						<!-- TASKS TAB --------------------------------->
						<div class="tabsStyle row">
							<ul class="nav nav-tabs">
								<li class="active"><a data-toggle="tab" href ng-click="currTab='1'">
									<?=__('companies')?></a>
								</li>
								<li><a data-toggle="tab" href ng-click="currTab='2';"><?=__('requests')?></a></li>
								<li><a data-toggle="tab" href ng-click="currTab='3';"><?=__('articles')?></a></li>
							</ul>
							
							<div class="tab-content col-sm-12" >
							  <div id="search-input">
								<div class="col-sm-5  nopadding " ng-if="currTab == '1'">
									<?php echo $this->Form->input('category_id', 
									['type' => 'select', 
									'options' => @$countries, 'empty' => __('all_countries'), 'label'=>false, 
									'class'=>'form-control ', "ng-model"=>"fsearch.country"])?>
								</div>
								<div class="col-sm-5  nopadding " ng-if="currTab == '2'">
									<?php echo $this->Form->input('category_id', 
									['type' => 'select', 
									'options' => @$countries_tr, 'empty' => __('turkey_cities'), 'label'=>false, 
									'class'=>'form-control ', "ng-model"=>"fsearch.country"])?>
								</div>
								<div class="{{currTab == '3' ? 'col-sm-10' : 'col-sm-5'}}  nopadding ">
									<input class="form-control"  
									ng-model="fsearch.keyword" 
									placeholder="<?=__('add_word2search')?>" required>
								</div>
								<div id="location-search-btn" class="col-sm-2 nopadding">
									<button type="submit" id="search-btn "><i class="fa fa-search"></i>
										<?=__('search')?>
									</button>
								</div>
							</div>
						</div>
					</form>
				</div>
				<!--<div id="location_slider_item_block" class="hideMob">
					<button id="map_mark"><i class="fa fa-caret-down"></i></button>
				</div>-->
			</div>
		</div>
	</div>
</div>

<!-- COMPANIES CATEGORIES -->
<div id="search-categorie-item">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 text-center">
				<div class="row">
					<div class="col-md-12 categories-heading bt_heading_3">
						<h1>
							<?=__('categories')?>
							<span>
							<?=__('companies')?>
							</span></h1>
						<div class="blind line_1"></div>
						<div class="flipInX-1 blind icon"><span class="icon"><i class="fa fa-stop"></i>&nbsp;&nbsp;<i class="fa fa-stop"></i></span></div>
						<div class="blind line_2"></div>
					</div>
					<?php foreach($companies_cat as $com):?>
					<div class="col-md-3 col-sm-6 col-xs-12">
						<div class="categorie_item">
							<div class="cate_item_block hi-icon-effect-8">
								<a href ng-click="goTo('/companies?category=<?=$com->id?>')">
									<div class="cate_item_social hi-icon">
										<i class="<?=$com->category_params?>"></i>
									</div>
								</a>
								<h1>
									<a href ng-click="goTo('/companies?category=<?=$com->id?>')">
										<?=$com->category_name?>
									</a>
								</h1>
							</div>
						</div>
					</div>
					<?php endforeach?>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- REQUESTS CATEGORIES -->
<div id="feature-item_listing_block">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 text-center">
				<div class="col-md-12 feature-item-listing-heading bt_heading_3">
					<h1>
						<?=__('categories')?>
						<span>
						<?=__('requests')?>
						</span></h1>
					<div class="blind line_1"></div>
					<div class="flipInX-1 blind icon"><span class="icon"><i class="fa fa-stop"></i>&nbsp;&nbsp;<i class="fa fa-stop"></i></span></div>
					<div class="blind line_2"></div>
				</div>
				<div class="row request_cat_list">
					<?php foreach($requests_cat as $req):?>
					<div class="col-md-3 col-sm-6 col-xs-12">
						<div class="categorie_item">
							<div class="cate_item_block hi-icon-effect-8">
								<a href ng-click="goTo('/requests?category=<?=$req->id?>')">
									<div class="cate_item_social hi-icon">
										<i class="<?=$req->category_params?>"></i>
									</div>
								</a>
								<h1>
									<a href ng-click="goTo('/requests?category=<?=$req->id?>')">
										<?=$req->category_name?>
									</a>
								</h1>
							</div>
						</div>
					</div>
					<?php endforeach?>
				</div>
			</div>
		</div>
	</div>
</div>
</div>

<!-- COUNTER -->
<div class="vfx-counter-block">
	<div class="vfx-item-container-slope vfx-item-bottom-slope vfx-item-left-slope"></div>
	<div class="container">
		<div class="vfx-item-counter-up">
			<div class="row">
				<div class="col-md-3 col-sm-6 col-xs-12">
					<div class="vfx-item-countup">
						<div class="vfx-item-black-top-arrow"><i class="fa fa-briefcase"></i></div>
						<div id="count-1" class="vfx-coutter-item count_number vfx-item-count-up">
							<?=$counts->companies+757*1?>
						</div>
						<div class="counter_text">
							<?=__('companies')?>
						</div>
					</div>
				</div>
				<div class="col-md-3 col-sm-6 col-xs-12">
					<div class="vfx-item-countup">
						<div class="vfx-item-black-top-arrow"><i class="fa fa-globe"></i></div>
						<div id="count-2" class="vfx-coutter-item count_number vfx-item-count-up">
							<?=$counts->requests+575*1?>
						</div>
						<div class="counter_text">
							<?=__('requests')?>
						</div>
					</div>
				</div>
				<div class="col-md-3 col-sm-6 col-xs-12">
					<div class="vfx-item-countup">
						<div class="vfx-item-black-top-arrow"><i class="fa fa-users"></i></div>
						<div id="count-3" class="vfx-coutter-item count_number vfx-item-count-up">
							<?=$counts->users+955*1?>
						</div>
						<div class="counter_text">
							<?=__('users')?>
						</div>
					</div>
				</div>
				<div class="col-md-3 col-sm-6 col-xs-12">
					<div class="vfx-item-countup last-countup">
						<div class="vfx-item-black-top-arrow"><i class="fa fa-rss"></i></div>
						<div id="count-4" class="vfx-coutter-item count_number vfx-item-count-up">
							<?=$counts->articles+585*1?>
						</div>
						<div class="counter_text">
							<?=__('articles')?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- LATEST ARTICLS -->
<div id="recent-product-item-listing">
<div class="container">
	<div class="row">
		<div class="col-sm-12 text-center">
			<div class="col-md-12 recent-item-listing-heading bt_heading_3">
				<h1>
					<?=__('latest')?>
					<span>
					<?=__('articles')?>
					</span></h1>
				<div class="blind line_1"></div>
				<div class="flipInX-1 blind icon"><span class="icon"><i class="fa fa-stop"></i>&nbsp;&nbsp;<i class="fa fa-stop"></i></span></div>
				<div class="blind line_2"></div>
			</div>
			<div class="row">
				<?php foreach($articles as $article):?>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<div class="recent-listing-box-container-item">
						<div class="col-md-6 col-sm-12 nopadding">
							<div class="recent-listing-box-image">
								<h1>
									<?=$article->category->category_name?>
								</h1>
								<?=$this->Html->image('/img/media/thumb/'.$article->article_photo, ['alt'=>''])?>
							</div>
							<!--<div class="hover-overlay">
                  <div class="hover-overlay-inner">
                    <ul class="listing-links">
                      <li><a href="#"><i class="fa fa-heart green-1"></i></a></li>
                      <li><a href="#"><i class="fa fa-map-marker blue-1"></i></a></li>
                      <li><a href="#"><i class="fa fa-share yallow-1"></i></a></li>
                    </ul>
                  </div>
                </div>-->
						</div>
						<div class="col-md-6 col-sm-12 nopadding">
							<div class="recent-listing-box-item">
								<div class="listing-boxes-text">
									<?=$this->Html->link( '<h3 style="height:36px;overflow: hidden;">'.
								$article->article_title.'</h3>',
							['controller'=>'Articles', 'action'=>'view', $article->id],
							['escape'=>false])?>
									<a href="#">
										<i class="fa fa-user"></i>
										<?=$article->article_author?>
									</a>
									<p style="height:90px; margin:0; overflow: hidden">
										<?=$article->seo_desc?>
									</p>
								</div>
								<div class="recent-feature-item-rating">
									<h2><i class="fa fa-eye"></i>
										<?=$article->stat_views?>
									</h2>
									<span>
									<?=$article->article_postdate?>
									</span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php endforeach?>
			</div>
		</div>
	</div>
</div>
<div id="recent-product-item-listing">
	<div class="container">
		<div class="row">
			<?php echo $this->Element('packages_elm');?>
		</div>
	</div>
</div>
