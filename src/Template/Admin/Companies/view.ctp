<?php
$fields = ['id', 'slug', 'language_id', 'user_id', 'category_id', 'subcategory_id', 'country_id', 'city_id', 'company_name', 'company_desc', 'company_phone', 'company_email', 'company_address', 'company_location', 'company_logo', 'company_photos', 'company_videos', 'company_links', 'company_worktime', 'company_preferred_langs', 'company_priority', 'seo_desc', 'seo_keywords', 'stat_created', 'stat_views', 'stat_shares', 'status'];
?>
<div class="view_page">
    <h3><?=__('show').' '.__($this->request->controller.'_rec').' '.h($rec->id) ?>
		<?=$this->element('actions_btns', ["rec"=>$rec]);?></h3>
    <table class="vertical-table">
    
    <?php foreach($fields as $field):?>
        <tr>
            <th><?= __($field) ?></th>
            <td><?= $rec->$field ?></td>
        </tr>
	<?php endforeach?>
    </table>
</div>
<br />
<div ><?=$this->element('actions_btns', ["rec"=>$rec]);?></div>
<br />