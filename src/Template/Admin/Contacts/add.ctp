

<div class="white_bg_block ">
	<div class="row ">
		<!-- body -->
		<div class="col-xs-12  ">
			<h1>
				<?=__('add_contact')?>
			</h1>
			<div class="from-list-lt row">
				<div class="col-md-12">
					<div class="col-sm-12" >
						<?php echo  $this->Form->create($contact, 
							['enctype'=>'multipart/form-data', 
							'type'=>'post', 'class'=>'form-float form-alt', 
							'novalidate' => true]) ?>
						<div class="row">
						
							<div class="form-group col-md-6 col-xs-12">
								<?php echo $this->Form->input('parent_id', [
									'options' =>$parents, "chk"=>"isSelectEmpty",
									'empty'=>__('parent_id'), 'label'=>__('parent_id'), 'class'=>'form-control']);?>
							</div>
							
							<div class="form-group col-md-12 col-xs-12">
								<?php echo $this->Form->input('contact_name', ["chk"=>"isEmpty", 
									'type'=>'text', 'label'=>__('contact_name'), 
									'placeholder'=>__('contact_name'), 'class'=>'form-control'])?>
							</div>
							
							<div class="form-group col-md-6 col-xs-12">
								<?php echo $this->Form->input('contact_phone', ["chk"=>"isEmpty", 
									'type'=>'text', 'label'=>__('contact_phone'), 
									'placeholder'=>__('contact_phone'), 'class'=>'form-control'])?>
							</div>
							
							<div class="form-group col-md-6 col-xs-12">
								<?php echo $this->Form->input('contact_email', ["chk"=>"isEmpty", 
									'type'=>'text', 'label'=>__('contact_email'), 
									'placeholder'=>__('contact_email'), 'class'=>'form-control'])?>
							</div>
							
							<div class="form-group col-md-4 col-xs-12">
								<?php echo $this->Form->input('contact_reason', ["chk"=>"isEmpty", 
									'type'=>'select', 'label'=>__('contact_reason'), 
									"options"=>$this->Do->get('contact_reason'), 'class'=>'form-control'])?>
							</div>
							
							<div class="form-group col-md-4 col-xs-12">
								<?php echo $this->Form->input('stat_type', ["chk"=>"isEmpty", 
									'type'=>'select', 'label'=>__('stat_type'), 
									"options"=>$this->Do->get('stat_type'), 'class'=>'form-control'])?>
							</div>
							
							<div class="form-group col-md-4 col-xs-12">
								<?php echo $this->Form->input('stat_priority', ["chk"=>"isEmpty", 
									'type'=>'select', 'label'=>__('stat_priority'), 
									"options"=>$this->Do->get('stat_priority'), 'class'=>'form-control'])?>
							</div>
							
							<div class="form-group col-md-12 col-xs-12">
								<?php echo $this->Form->input('contact_subject', ["chk"=>"isEmpty", 
									'type'=>'text', 'label'=>__('contact_subject'), 
									'placeholder'=>__('contact_subject'), 'class'=>'form-control'])?>
							</div>
							
							<div class="form-group col-md-12 col-xs-12">
								<?php echo $this->Form->input('contact_message', ["chk"=>"isEmpty", 
									'type'=>'textarea', 'label'=>__('contact_message'), 
									'placeholder'=>__('contact_message'), 'class'=>'form-control'])?>
							</div>
							
							<div class="form-group col-md-6 col-xs-12">
								<button class="btn btn-info " type="submit"><?php echo __('Submit')?></button>
							</div>
						</div>
						<br />
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

