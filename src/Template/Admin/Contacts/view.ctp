<?php
$fields = ['id', 'parent_id', 'country_id', 'city_id', 'contact_name', 'contact_phone', 'contact_email', 'contact_subject', 'contact_message', 'contact_reason', 'stat_info', 'stat_type', 'stat_created', 'status'];
?>
<div class="view_page">
    <h3><?=__('show').' '.__($this->request->controller.'_rec').' '.h($rec->id) ?>
		<?=$this->element('actions_btns', ["rec"=>$rec]);?></h3>
    <table class="vertical-table">
    
    <?php foreach($fields as $field):?>
        <tr>
            <th><?= __($field) ?></th>
            <td><?= $rec->$field ?></td>
        </tr>
	<?php endforeach?>
    </table>
</div>
<br />
<div ><?=$this->element('actions_btns', ["rec"=>$rec]);?></div>
<br />