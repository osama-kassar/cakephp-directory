<div>
    <h3>
		<?= __('Contacts') ?>
    </h3>
    <div style="overflow-x: scroll;">
        <table class="table">
            <thead>
                <tr>
					<th><?php echo $this->Paginator->sort('id', __('id')) ?></th>
					<th><?php echo $this->Paginator->sort('contact_name', __('contact_name')) ?></th>
					<th class="hideMob"><?php echo $this->Paginator->sort('stat_priority', __('stat_priority')) ?></th>
					<th class="hideMob"><?php echo $this->Paginator->sort('contact_subject', __('contact_subject')) ?></th>
					<th class="hideMob"><?php echo $this->Paginator->sort('contact_reason', __('contact_reason')) ?></th>
					<th class="hideMob"><?php echo $this->Paginator->sort('status', __('status')) ?></th>
					<th><?php echo __('Actions') ?></th>
                </tr>
            </thead>
			<tbody>
				<?php foreach ($contacts as $contact): ?>
				<tr>
					<td><?php echo $contact->id ?></td>
					<td><?php echo $this->Html->link(__($contact->contact_name), 
						"javascript:void();", 
						['onclick'=>'filter("parent_id", '.$contact->id.')'])?></td>
					<td class="hideMob"><?php echo $this->Number->format($contact->stat_priority)?></td>
					<td class="hideMob"><?php echo $contact->contact_subject?></td>
					<td class="hideMob"><?php echo __($contact->contact_reason)?></td>
					<td class="hideMob"><?php echo $contact->status ?></td>                    
					<td class="actions">
						<?php $contact->status == 0 ? $enable = '<i class="fa fa-check-circle" aria-hidden="true"></i>' : 
							$enable = '<i class="fa fa-times-circle-o" aria-hidden="true"></i>';?>
						<?= $this->Html->link('<i class="fa fa-eye" aria-hidden="true"></i>', 
							['action' => 'view', $contact->id], ['escape'=>false, 'class'=>'pageContent']) ?>
						<?= $this->Html->link('<i class="fa fa-pencil-square-o" aria-hidden="true"></i>', 
							['action' => 'edit', $contact->id], ['escape'=>false, 'class'=>'pageContent']) ?>
						<?= $this->Form->postLink($enable, ['action' => 'disable', $contact->id], 
							['confirm' => __('change_status').' '.$contact->id, 'escape'=>false]) ?>
						<?= $this->Form->postLink('<i class="fa fa-trash" aria-hidden="true"></i>', 
							['action' => 'delete', $contact->id], ['confirm' => __('delete_record').' '.$contact->id, 'escape'=>false]) ?>
					</td>
				</tr>
				<?php endforeach; ?>
			</tbody>
        </table>
    </div>
    
</div>

<div class="paginator">
    <ul class="pagination">
        <?= $this->Paginator->prev('< ' . __('previous')) ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next(__('next') . ' >') ?>
    </ul>
    <p><?= $this->Paginator->counter() ?></p>
</div>


