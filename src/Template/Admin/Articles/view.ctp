<?php
$fields = ['id', 'slug', 'user_id', 'category_id', 'subcategory_id', 'article_priority', 'article_title', 'article_body', 'article_photo', 'article_postdate', 'article_author', 'article_resource', 'seo_desc', 'seo_keywords', 'seo_tags', 'stat_views', 'stat_shares', 'status'];
?>
<div class="view_page">
    <h3><?=__('show').' '.__($this->request->controller.'_rec').' '.h($rec->id) ?>
		<?=$this->element('actions_btns', ["rec"=>$rec]);?></h3>
    <table class="vertical-table">
    
    <?php foreach($fields as $field):?>
        <tr>
            <th><?= __($field) ?></th>
            <td><?= $rec->$field ?></td>
        </tr>
	<?php endforeach?>
    </table>
</div>
<br />
<div ><?=$this->element('actions_btns', ["rec"=>$rec]);?></div>
<br />