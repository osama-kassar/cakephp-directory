<div id="pricing-item-block" class="white_bg_block ">
	<div class="row ">
		<!-- body -->
		<div class="col-xs-12  ">
			<h1>
				<?=__('add_article')?>
			</h1>
			<div class="from-list-lt row">
				<div class="col-md-12">
					<div class="col-sm-12" >
						<?php echo  $this->Form->create($article, 
							['enctype'=>'multipart/form-data', 
							'type'=>'post', 'class'=>'form-float form-alt', 
							'novalidate' => true]) ?>
						<div class="row">
						
							<div class="form-group col-md-6 col-xs-12">
								<?php echo $this->Form->input('language_id', 
									['type' => 'select', "chk"=>"isSelectEmpty", 
									'options' => $this->Do->get('lang'), 'empty' => __('language_id'), 
									'label'=>__('language_id'), 
									'class'=>'form-control'])?>
							</div>
							
							<div class="form-group col-md-12 col-xs-12">
								<?php echo $this->Form->input('article_title', ["chk"=>"isEmpty", 
									'type'=>'text', 'label'=>__('article_title'), 
									'placeholder'=>__('article_title'), 'class'=>'form-control'])?>
							</div>
							
							<div class="form-group col-md-6 col-xs-12">
								<?php echo $this->Form->input('category_id', ["chk"=>"isSelectEmpty", 
									'onClick'=>'getSubCat(0, "subcategory-id")',
									'onChange'=>'getSubCat(this, "subcategory-id")', 'type' => 'select', 
									'options' => @$categories, 'empty' => __('f_category'), 'label'=>__('category_id'), 
									'class'=>'form-control'])?>
							</div>
							
							<div class="form-group col-md-6 col-xs-12">
								<?php echo $this->Form->input('subcategory_id', ["chk"=>"isSelectEmpty", 
									'type'=>'select', 'options'=>@$subcategories, 
									'label'=>__('subcategory_id'), 'class'=>'form-control'])?>
							</div>
							
							<div class="form-group col-md-12 col-xs-12">
								<?php echo $this->element('ckEditor',[
									'textarea_id'=>'article-body', 'textarea_name'=>'article_body'
									]);?>
								<?php //echo $this->element('froalaEditor',['textarea_name'=>'article_body']);?>
							</div>
							
							<div class="form-group col-md-12 col-xs-12">
								<?php echo $this->Form->input('article_photo', ["chk"=>"isEmpty", 
									'type'=>'text', 'label'=>__('article_photo'), 
									'placeholder'=>__('article_photo'), 'class'=>'form-control'])?>
							</div>
							
							<div class="form-group col-md-12 col-xs-12">
								<?php echo $this->Form->input('article_author', ["chk"=>"isEmpty", 
									'type'=>'text', 'label'=>__('article_author'), 
									'placeholder'=>__('article_author'), 'class'=>'form-control'])?>
							</div>
							
							<div class="form-group col-md-12 col-xs-12">
								<?php echo $this->Form->input('article_resource', ["chk"=>"isEmpty", 
									'type'=>'text', 'label'=>__('article_resource'), 
									'placeholder'=>__('article_resource'), 'class'=>'form-control'])?>
							</div>
							
							<div class="form-group col-md-12 col-xs-12">
								<label><?=__('seo_keywords')?></label>
								<tags-input placeholder="<?=__('add_keywords')?>" 
									ng-model="seo_keywords" use-strings="true"></tags-input>
								<?php echo $this->Form->input('seo_keywords', ["chk"=>"isEmpty", 
									'type'=>'hidden', "value"=>"{{seo_keywords.join()}}", 'label'=>__('seo_keywords'), 
									'placeholder'=>__('seo_keywords'), 'class'=>'form-control'])?>
							</div>
							
							<div class="form-group col-md-12 col-xs-12">
								<label><?=__('seo_tags')?></label>
								<tags-input placeholder="<?=__('add_tags')?>" 
									ng-model="seo_tags" use-strings="true"></tags-input>
								<?php echo $this->Form->input('seo_tags', ["chk"=>"isEmpty",
									'type'=>'hidden', "value"=>"{{seo_tags.join()}}", 'label'=>__('seo_tags'), 
									'placeholder'=>__('seo_tags'), 'class'=>'form-control'])?>
							</div>
							
							<div class="form-group col-md-6 col-xs-12">
								<button class="btn btn-info " type="submit"><?php echo __('Submit')?></button>
							</div>
						</div>
						<br />
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
