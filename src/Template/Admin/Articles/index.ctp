<div>
    <h3>
		<?= __('Articles') ?>
    </h3>
    <div style="overflow-x: scroll;">
        <table class="table">
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('id', __('id')) ?></th>
                    <th><?= $this->Paginator->sort('article_title', __('article_title')) ?></th>
                    <th class="hideMob"><?= $this->Paginator->sort('user_id', __('user_id')) ?></th>
                    <th class="hideMob"><?= $this->Paginator->sort('category_id', __('category_id')) ?></th>
                    <th class="hideMob"><?= $this->Paginator->sort('article_priority', __('article_priority')) ?></th>
                    <!--<th><?= $this->Paginator->sort('article_author', __('article_author')) ?></th>-->
                    <th class="hideMob"><?= $this->Paginator->sort('stat_views', __('stat_views')) ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($articles as $article): ?>
                <tr>
                    <td><?= $this->Number->format($article->id) ?></td>
                    <td><?= h($article->article_title) ?></td>
                    <td class="hideMob"><?= $article->has('user') ? $this->Html->link($article->user->username, ['controller' => 'Users', 'action' => 'view', $article->user->id]) : '' ?></td>
                    <td class="hideMob"><?= $article->has('category') ? $this->Html->link($article->category->category_name, ['controller' => 'Categories', 'action' => 'view', $article->category->id]) : '' ?></td>
                    <td class="hideMob"><?= $this->Number->format($article->article_priority) ?></td>
                    <!--<td><?= h($article->article_author) ?></td>-->
                    <td class="hideMob"><?= $this->Number->format($article->stat_views) ?></td>
                    <td class="actions">
                        <?php $article->status == 0 ? $enable = '<i class="fa fa-check-circle" aria-hidden="true"></i>' : 
                            $enable = '<i class="fa fa-times-circle-o" aria-hidden="true"></i>';?>
                            
                        <?= $this->Html->link('<i class="fa fa-eye" aria-hidden="true"></i>', 
                            ['action' => 'view', $article->id], ['escape'=>false, 'class'=>'pageContent']) ?>
                            
                        <?= $this->Html->link('<i class="fa fa-pencil-square-o" aria-hidden="true"></i>', 
                            ['action' => 'edit', $article->id], ['escape'=>false, 'class'=>'pageContent']) ?>
                            
                        <?= $this->Form->postLink($enable, ['action' => 'disable', $article->id], 
                            ['confirm' => __('change_status').' '.$article->id, 'escape'=>false]) ?>
                            
                        <?= $this->Form->postLink('<i class="fa fa-trash" aria-hidden="true"></i>', 
                            ['action' => 'delete', $article->id], ['confirm' => __('delete_record').' '.$article->id, 'escape'=>false]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    
</div>

<div class="paginator">
    <ul class="pagination">
        <?= $this->Paginator->prev('< ' . __('previous')) ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next(__('next') . ' >') ?>
    </ul>
    <p><?= $this->Paginator->counter() ?></p>
</div>
