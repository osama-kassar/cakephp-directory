

<div class="white_bg_block ">
	<div class="row ">
		<!-- body -->
		<div class="col-xs-12  ">
			<h1>
				<?=__('edit_category')?>
			</h1>
			<div class="from-list-lt row">
				<div class="col-md-12">
					<div class="col-sm-12" >
						<?php echo  $this->Form->create($category, 
							['enctype'=>'multipart/form-data', 
							'type'=>'post', 'class'=>'form-float form-alt', 
							'novalidate' => true]) ?>
						<div class="row">
						
							<div class="form-group col-md-6 col-xs-12">
								<?php echo $this->Form->input('parent_id', [
									'options' =>$parents, "chk"=>"isSelectEmpty", 
									'onchange'=>'filter("parent_id", this[selectedIndex].value)',
									'value'=>@$this->request->query['parent_id'],
									'empty'=>true, 'label'=>__('parent_id'), 'class'=>'form-control']);?>
							</div>
							
							<div class="form-group col-xs-12">
								<?php echo $this->Form->input('children', [
									'options' => @$childern, 'label'=>__('children'), 'empty'=>false, 
									'onchange'=>'filter("parent_id", this[selectedIndex].value)', 
									'multiple'=>true, 'class'=>'form-control', "type"=>"select"]);?>
							</div>
							
							<div class="form-group col-md-12 col-xs-12">
								<?php echo $this->Form->input('category_name', ["chk"=>"isEmpty", 
									'type'=>'text', 'label'=>__('category_name'), 
									'placeholder'=>__('category_name'), 'class'=>'form-control'])?>
							</div>
							
							<div class="form-group col-md-12 col-xs-12">
								<?php echo $this->Form->input('category_params', ["chk"=>"isEmpty", 
									'type'=>'text', 'label'=>__('category_params'), 
									'placeholder'=>__('category_params'), 'class'=>'form-control'])?>
							</div>
							
							<div class="form-group col-md-12 col-xs-12">
								<?php echo $this->Form->input('category_priority', ["chk"=>"isEmpty", 
									'type'=>'text', 'label'=>__('category_priority'), 
									'placeholder'=>__('category_priority'), 'class'=>'form-control'])?>
							</div>
							
							<div class="form-group col-md-6 col-xs-12">
								<button class="btn btn-info " type="submit"><?php echo __('Submit')?></button>
							</div>
						</div>
						<br />
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

