<div>
    <h3>
		<?= __('Categories') ?>
    </h3>
    <div style="overflow-x: scroll;">
        <table class="table">
            <thead>
                <tr>
					<th><?php echo $this->Paginator->sort('id', __('id')) ?></th>
					<th><?php echo $this->Paginator->sort('category_name', __('category_name')) ?></th>
					<th class="hideMob"><?php echo $this->Paginator->sort('parent_id', __('parent_id')) ?></th>
					<th class="hideMob"><?php echo $this->Paginator->sort('category_priority', __('category_priority')) ?></th>
					<th class="hideMob"><?php echo $this->Paginator->sort('status', __('status')) ?></th>
					<th><?php echo __('Actions') ?></th>
                </tr>
            </thead>
            
			<tbody>
				<?php foreach ($categories as $category): ?>
				<tr>
					<td><?php echo $category->id ?></td>
					<td><?php echo $this->Html->link(__($category->category_name), 
						"javascript:void();", 
						['onclick'=>'filter("parent_id", '.$category->id.')'])?></td>
					<td class="hideMob"><?php echo $this->Number->format($category->parent_id)?></td>
					<td class="hideMob"><?php echo $category->category_priority ?></td>
					<td class="hideMob"><?php echo $category->status ?></td>                    
					<td class="actions">
						<?php $category->status == 0 ? $enable = '<i class="fa fa-check-circle" aria-hidden="true"></i>' : 
							$enable = '<i class="fa fa-times-circle-o" aria-hidden="true"></i>';?>
						<?= $this->Html->link('<i class="fa fa-eye" aria-hidden="true"></i>', 
							['action' => 'view', $category->id], ['escape'=>false, 'class'=>'pageContent']) ?>
						<?= $this->Html->link('<i class="fa fa-pencil-square-o" aria-hidden="true"></i>', 
							['action' => 'edit', $category->id], ['escape'=>false, 'class'=>'pageContent']) ?>
						<?= $this->Form->postLink($enable, ['action' => 'disable', $category->id], 
							['confirm' => __('change_status').' '.$category->id, 'escape'=>false]) ?>
						<?= $this->Form->postLink('<i class="fa fa-trash" aria-hidden="true"></i>', 
							['action' => 'delete', $category->id], ['confirm' => __('delete_record').' '.$category->id, 'escape'=>false]) ?>
					</td>
				</tr>
				<?php endforeach; ?>
			</tbody>
        </table>
    </div>
    
</div>

<div class="paginator">
    <ul class="pagination">
        <?= $this->Paginator->prev('< ' . __('previous')) ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next(__('next') . ' >') ?>
    </ul>
    <p><?= $this->Paginator->counter() ?></p>
</div>


