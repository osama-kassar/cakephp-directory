<div>
    <h3>
		<?= __('assignments') ?>
    </h3>
    <div style="overflow-x: scroll;">
        <table class="table">
            <thead>                
                <tr>
                    <th><?= $this->Paginator->sort('id', __('id')) ?></th>
                    <th><?= $this->Paginator->sort('assign_target', __('assign_target')) ?></th>
                    <th class="hideMob"><?= $this->Paginator->sort('assign_title', __('assign_title')) ?></th>
                    <th class="hideMob"><?= $this->Paginator->sort('stat_created', __('stat_created')) ?></th>
                    <th class="hideMob"><?= $this->Paginator->sort('stat_delivered', __('stat_delivered')) ?></th>
                    <th class="hideMob"><?= $this->Paginator->sort('status', __('status')) ?></th>
					<th><?php echo __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($assignments as $assignment): ?>
                <tr>
                    <td><?= $this->Number->format($assignment->id) ?></td>
                    <td><?= h($assignment->assign_target) ?></td>
                    <td><?= h($assignment->assign_title) ?></td>
                    <td><?= h($assignment->stat_created) ?></td>
                    <td><?= h($assignment->stat_delivered) ?></td>
                    <td><?= h($assignment->status) ?></td>
                    <td class="actions">
                        <?= $this->Html->link('<i class="fa fa-eye" aria-hidden="true"></i>', 
                            ['action' => 'view', $assignment->id], ['escape'=>false, 'class'=>'pageContent']) ?>
                            
                        <?= $this->Html->link('<i class="fa fa-pencil-square-o" aria-hidden="true"></i>', 
                            ['action' => 'edit', $assignment->id], ['escape'=>false, 'class'=>'pageContent']) ?>
                            
                        <?= $this->Form->postLink('<i class="fa fa-trash" aria-hidden="true"></i>', 
                            ['action' => 'delete', $assignment->id], ['confirm' => __('delete_record').' '.$assignment->id, 'escape'=>false]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    
</div>

<div class="paginator">
    <ul class="pagination">
        <?= $this->Paginator->prev('< ' . __('previous')) ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next(__('next') . ' >') ?>
    </ul>
    <p><?= $this->Paginator->counter() ?></p>
</div>
