<?php
$fields = ['id', 'user_id', 'assignedby_id', 'assign_target', 'assign_title', 'assign_notes', 'stat_created', 'stat_started', 'stat_delivered', 'status'];
?>
<div class="view_page">
    <h3><?=__('show').' '.__($this->request->controller.'_rec').' '.h($rec->id) ?>
        
    <?php ///echo $this->element('actions_btns')?></h3>
    <table class="vertical-table">
    
    <?php foreach($fields as $field):?>
        <tr>
            <th><?= __($field) ?></th>
            <td><?= $rec->$field ?></td>
        </tr>
	<?php endforeach?>
    </table>
</div>
<br />
<div >
    <?php echo $this->element('actions_btns')?>
</div>
<br />