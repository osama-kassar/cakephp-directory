<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Assignment $assignment
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Assignment'), ['action' => 'edit', $assignment->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Assignment'), ['action' => 'delete', $assignment->id], ['confirm' => __('Are you sure you want to delete # {0}?', $assignment->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Assignments'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Assignment'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="assignments view large-9 medium-8 columns content">
    <h3><?= h($assignment->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $assignment->has('user') ? $this->Html->link($assignment->user->firstname, ['controller' => 'Users', 'action' => 'view', $assignment->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Assignedrec Url') ?></th>
            <td><?= h($assignment->assignedrec_url) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Assign Title') ?></th>
            <td><?= h($assignment->assign_title) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($assignment->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Assignedby Id') ?></th>
            <td><?= $this->Number->format($assignment->assignedby_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Reassigned') ?></th>
            <td><?= $this->Number->format($assignment->reassigned) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Rec State') ?></th>
            <td><?= $this->Number->format($assignment->status) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Stat Created') ?></th>
            <td><?= h($assignment->stat_created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Stat Viewed') ?></th>
            <td><?= h($assignment->stat_viewed) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Stat Delivered') ?></th>
            <td><?= h($assignment->stat_delivered) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Assign Notes') ?></h4>
        <?= $this->Text->autoParagraph(h($assignment->assign_notes)); ?>
    </div>
</div>
