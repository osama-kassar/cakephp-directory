<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Assignment[]|\Cake\Collection\CollectionInterface $assignments
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Assignment'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="assignments index large-9 medium-8 columns content">
    <h3><?= __('Assignments') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('user_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('assignedby_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('assignedrec_url') ?></th>
                <th scope="col"><?= $this->Paginator->sort('assign_title') ?></th>
                <th scope="col"><?= $this->Paginator->sort('stat_created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('stat_viewed') ?></th>
                <th scope="col"><?= $this->Paginator->sort('stat_delivered') ?></th>
                <th scope="col"><?= $this->Paginator->sort('reassigned') ?></th>
                <th scope="col"><?= $this->Paginator->sort('status') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($assignments as $assignment): ?>
            <tr>
                <td><?= $this->Number->format($assignment->id) ?></td>
                <td><?= $assignment->has('user') ? $this->Html->link($assignment->user->firstname, ['controller' => 'Users', 'action' => 'view', $assignment->user->id]) : '' ?></td>
                <td><?= $this->Number->format($assignment->assignedby_id) ?></td>
                <td><?= h($assignment->assignedrec_url) ?></td>
                <td><?= h($assignment->assign_title) ?></td>
                <td><?= h($assignment->stat_created) ?></td>
                <td><?= h($assignment->stat_viewed) ?></td>
                <td><?= h($assignment->stat_delivered) ?></td>
                <td><?= $this->Number->format($assignment->reassigned) ?></td>
                <td><?= $this->Number->format($assignment->status) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $assignment->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $assignment->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $assignment->id], ['confirm' => __('Are you sure you want to delete # {0}?', $assignment->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
