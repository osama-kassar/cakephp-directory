<div ng-init="
              getStatistics(); 
              ">
    <div class="row">

        <div class="col-xs-12">
            <h3><?= __('statistics') ?></h3>
            
            <div class="col-sm-3">
                <h3><?=__('new/all')." ".__('users')?></h3>
               {{stats.usersNew}} / {{stats.usersTotal}}
            </div>
            <div class="col-sm-3">
                <h3><?=__('new/all')." ".__('requests')?></h3>
                {{stats.requestsNew}} / {{stats.requestsTotal}} 
            </div>
            <div class="col-sm-3">
                <h3><?=__('users')." ".__('per_countries')?></h3>
                <span ng-repeat="(k, v) in stats.usersPerCountry">{{k}}: {{v}}</span>
            </div>
            <div class="col-sm-3">
                <h3><?=__('users')." ".__('per_categories')?></h3>
                <span ng-repeat="(k, v) in stats.usersPerCategory">{{k}}: {{v}}</span>
            </div>
            <div class="col-sm-3">
                <h3><?=__('requests')." ".__('per_countries')?></h3>
                <span ng-repeat="(k, v) in stats.requestsPerCountry">{{k}}: {{v}}</span>
            </div>
            <div class="col-sm-3">
                <h3><?=__('requests')." ".__('per_categories')?></h3>
                <span ng-repeat="(k, v) in stats.requestsPerCategory">{{k}}: {{v}}</span>
            </div>
            
        </div>
    </div>
</div>