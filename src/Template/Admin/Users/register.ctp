<div>
<?= $this->Flash->render('auth') ?>
    <?= $this->Form->create($user, ['novalidate']) ?>
    <fieldset>
        <legend><h2><?= __('register') ?></h2></legend>
    	<?= __('enter_password_username') ?>
        <?= $this->Form->input('username',['label'=>__('username')]) ?>
        <?= $this->Form->input('email',['label'=>__('email')]) ?>
        <?= $this->Form->input('confirm_email',['label'=>__('confirm_email')]) ?>
        <?= $this->Form->input('password',['type'=>'password', 'label'=>__('password')]) ?>
        <?= $this->Form->input('confirm_password',['type'=>'password', 'label'=>__('confirm_password')]) ?><br />
    <?= $this->Form->button(__('register')); ?>
    <?= $this->Form->end() ?>
    </fieldset>
</div>
<?= $this->Html->link('<i class="fa fa-sign-in" aria-hidden="true"></i> '.__('login'), ['controller'=>'users', 'action'=>'login'], ['title'=>__('login'), 'escape'=>false, 'class'=>'pageContent']);?>