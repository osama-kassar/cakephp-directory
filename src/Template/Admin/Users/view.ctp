<?php
$fields = ['id', 'city_id', 'country_id', 'firstname', 'lastname', 'password', 'username', 'phone', 'photo', 'role', 'email_activate', 'sms_activate', 'created', 'modified', 'status'];
?>
<div class="view_page">
    <h3><?=__('show').' '.__($this->request->controller.'_rec').' '.h($rec->id) ?>
		<?=$this->element('actions_btns', ["rec"=>$rec]);?></h3>
    <table class="vertical-table">
    
    <?php foreach($fields as $field):?>
        <tr>
            <th><?= __($field) ?></th>
            <td><?= $rec->$field ?></td>
        </tr>
	<?php endforeach?>
    </table>
</div>
<br />
<div ><?=$this->element('actions_btns', ["rec"=>$rec]);?></div>
<br />