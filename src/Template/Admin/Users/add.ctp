<div class="white_bg_block ">
	<div class="row ">
		
		<!-- body -->
		<div class="col-xs-12  ">
			<h1>
				<?php echo __('add_user')?>
			</h1>
			<div class="from-list-lt row">
				<div class="col-md-12">
					<div class="col-sm-12" >
						<?php echo  $this->Form->create($user, 
							['enctype'=>'multipart/form-data',  
							'class'=>'form-float form-alt', 'novalidate' => true]) ?>
						<div class="row">
							
							<div class="form-group col-md-6">
								<?php echo $this->Form->input('firstname', 
									['type'=>'text', 'label'=>__('firstname'), 
									'placeholder'=>__('firstname'), "chk"=>"isEmpty", 
									'class'=>'form-control'])?>
							</div>
							
							<div class="form-group col-md-6">
								<?php echo $this->Form->input('lastname', 
									['type'=>'text', 'label'=>__('lastname'), 
									'placeholder'=>__('lastname'), "chk"=>"isEmpty", 
									'class'=>'form-control'])?>
							</div>
							
							
							<div class="form-group col-xs-12">
								<?php echo $this->Form->input('username', 
									['type'=>'text', 'label'=>__('email'), 
									'placeholder'=>__('email'), "chk"=>"isEmail", 
									'class'=>'form-control'])?>
							</div>
							
							<div class="form-group col-xs-12">
								<?php echo $this->Form->input('password', 
									['type'=>'password', 'label'=>__('password'), 
									'placeholder'=>__('password'), "chk"=>"isEmpty", 
									'class'=>'form-control'])?>
							</div>
							
							<div class="form-group col-md-6 col-xs-12">
								<?php echo $this->Form->input('country_id', [
									'onClick'=>'getSubCat(0, "city-id", this)', "chk"=>"isSelectEmpty",
									'onChange'=>'getSubCat(this, "city-id", this)', 'type' => 'select', 
									'options' => @$countries, 'empty' => __('f_country'), 'label'=>__('f_country'), 
									'class'=>'form-control'])?>
							</div>
							
							<div class="form-group col-md-6 col-xs-12">
								<?php echo $this->Form->input('city_id', [
									'type'=>'select', 'label'=>__('city_id'), "chk"=>"isSelectEmpty", 
									'class'=>'form-control', 'options'=>@$cities,
									'empty' => __('f_city')])?>
							</div>
							
							<div class="form-group col-md-6">
								<?php echo $this->Form->input('phone', 
									['type'=>'text', 'label'=>__('phone'), 
									'placeholder'=>__('phone'), "chk"=>"isEmpty", 
									'class'=>'form-control'])?>
							</div>
							
							<div class="clearfix form-group col-xs-12 ">
								<label><?=__('photo')?></label>
								<?php echo $this->element('uploadForm', ['name'=>'photo', "photosNumber"=>1,
									'oldPhotos'=>$user->photo, "recId"=>$user->id, 
									"msg"=>"upload_user_photos_msg"]);?>
							</div>
							
							<div class="form-group col-xs-12">
								<?php echo $this->Form->input('role', [
									'type'=>'select', 'label'=>__('role'), "chk"=>"isSelectEmpty", 
									'class'=>'form-control', 'options'=>$this->Do->get('AdminRoles'),
									'empty' => __('role')])?>
							</div>
							<div class="form-group col-xs-12">
								<button class="btn btn-info" type="submit"><?php echo __('Submit')?></button>
							</div>
						</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

