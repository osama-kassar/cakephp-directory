<div class="">
    <?= $this->Form->create($user, ['url'=>['controller'=>'Users', 'action'=>'getpassword'], 'novalidate']) ?>
    <fieldset>
        <legend><h2><?= __('getpassword') ?></h2></legend>
        <?php
			echo $this->Form->input('email_request', ['label'=>__('email_request')]);
        ?><br />


    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
    </fieldset>
</div>
<?= $this->Html->link('<i class="fa fa-sign-in" aria-hidden="true"></i> '.__('login'), ['controller'=>'users', 'action'=>'login'], ['title'=>__('login'), 'escape'=>false, 'class'=>'pageContent']);?>
