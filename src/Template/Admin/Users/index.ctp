<div>
    <h3><?= __('users') ?></h3>
    
    <div style="overflow-x: scroll;">
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort(__('id')) ?></th>
                <th><?= $this->Paginator->sort(__('username')) ?></th>
                <th class="hideMob"><?= $this->Paginator->sort(__('email')) ?></th>
                <th class="hideMob"><?= $this->Paginator->sort(__('role')) ?></th>
                <th class="hideMob"><?= $this->Paginator->sort(__('modified')) ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($users as $user): ?>
			<?php $user->status == 0 ? $enable = __('Enable') : $enable = __('Disable');?>
            <tr>
                <td><?= $this->Number->format($user->id) ?></td>
                <td><?= h($user->firstname).' '.h($user->lastname) ?></td>
                <td class="hideMob"><?= h($user->username) ?></td>
                <td class="hideMob"><?= h($user->role) ?></td>
                <td class="hideMob"><?= h($user->modified) ?></td>
                <td class="actions">
                	<?php $user->status == 0 ? $enable = '<i class="fa fa-check-circle" aria-hidden="true"></i>' : 
						$enable = '<i class="fa fa-times-circle-o" aria-hidden="true"></i>';?>
                    <?= $this->Html->link('<i class="fa fa-eye" aria-hidden="true"></i>', 
						['action' => 'view', $user->id], ['escape'=>false]) ?>
                    <?= $this->Html->link('<i class="fa fa-pencil-square-o" aria-hidden="true"></i>', 
						['action' => 'edit', $user->id], ['escape'=>false]) ?>
                    <?= $this->Form->postLink($enable, ['action' => 'disable', $user->id], 
						['confirm' => __('change_status').' '.$user->id, 'escape'=>false]) ?>
                    <?= $this->Form->postLink('<i class="fa fa-trash" aria-hidden="true"></i>', 
						['action' => 'delete', $user->id], ['confirm' => __('delete_record').' '.$user->id, 'escape'=>false]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
