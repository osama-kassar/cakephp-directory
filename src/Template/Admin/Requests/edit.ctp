<div class="white_bg_block ">
	<div class="row ">
		
		<!-- body -->
		<div class="col-xs-12  ">
			<h1>
				<?php echo __('edit_request')?>
			</h1>
			<div class="from-list-lt row">
				<div class="col-md-12">
					<div class="col-sm-12" >
						<?php echo  $this->Form->create($request, 
							['enctype'=>'multipart/form-data',  
							'class'=>'form-float form-alt', 'novalidate' => true]) ?>
						<div class="row">
                            
							<div class="form-group col-md-6 col-xs-12">
								<?php echo $this->Form->input('language_id', 
									['type' => 'select', "chk"=>"isSelectEmpty", 
									'options' => $this->Do->lcl( $this->Do->get('lang') ), 'empty'=>__('language_id'), 
									'label'=>__('language_id'), 
									'class'=>'form-control'])?>
							</div>
                            
							<div class="form-group col-md-6">
								<?php echo $this->Form->input('req_title', 
									['type'=>'text', 'label'=>__('req_title'), 
									'placeholder'=>__('req_title_desc'), "chk"=>"isEmpty", 
									'class'=>'form-control'])?>
							</div>
							<div class="form-group col-md-12">
								<?php echo $this->Form->input('req_desc', 
									['type'=>'textarea', 'label'=>__('req_body'), 
									'placeholder'=>__('req_body_desc'), "chk"=>"isParagraph", 
									'class'=>'form-control'])?>
							</div>
							<div class="form-group col-md-6 col-xs-12">
								<?php echo $this->Form->input('category_id', 
									['onClick'=>'getSubCat(0, "subcategory-id")', "chk"=>"isSelectEmpty",
									'onChange'=>'getSubCat(this, "subcategory-id")', 'type' => 'select', 
									'options' => @$categories, 'empty' => __('f_category'), 'label'=>__('category_id'), 
									'class'=>'form-control'])?>
							</div>
							<div class="form-group col-md-6 col-xs-12">
								<?php echo $this->Form->input('subcategory_id', 
									['type'=>'select', 'label'=>__('subcategory_id'), "chk"=>"isSelectEmpty", 
									'class'=>'form-control', 'options'=>@$subcategories,
									'empty' => __('f_subcategory')])?>
							  </div>
							
							<div class="form-group col-md-6 col-xs-12">
								<?php echo $this->Form->input('country_id', [
									'onClick'=>'getSubCat(0, "city-id", this)', "chk"=>"isSelectEmpty",
									'onChange'=>'getSubCat(this, "city-id", this)', 'type' => 'select', 
									'options' => @$countries, 'empty' => __('f_country'), 'label'=>__('f_country'), 
									'class'=>'form-control'])?>
							</div>
							<div class="form-group col-md-6 col-xs-12">
								<?php echo $this->Form->input('city_id', [
									'type'=>'select', 'label'=>__('city_id'), "chk"=>"isSelectEmpty", 
									'class'=>'form-control', 'options'=>@$cities,
									'empty' => __('f_city')])?>
							</div>
                            
                            
                            
                            <div class="clearfix form-group col-xs-12">
                                <div>
                                    <label> <?=__('req_preferred_contacts');?> </label>
                                </div>
                                <div class="mychckbox_container">
                                    <?= $this->Form->select('req_preferred_contacts', 
                                        $this->Do->lcl( $this->Do->get('prefered_contacts') ),
                                             ['multiple'=>'checkbox',
                                              'label'=>['class'=>'mycheckbox gray'],
                                              'chk'=>'isEmpty']); ?>
                                </div>
                            </div>
                            
                            <div class="clearfix form-group col-xs-12">
                                <div>
                                    <label> <?=__('req_preferred_langs');?> </label>
                                </div>
                                <div class="mychckbox_container">
                                    <?= $this->Form->select('req_preferred_langs', 
                                        $this->Do->lcl( $this->Do->get('prefered_langs') ),
                                             ['multiple'=>'checkbox',
                                              'label'=>['class'=>'mycheckbox gray'],
                                              'chk'=>'isEmpty']); ?>
                                </div>
                            </div>
						
							<div class="form-group col-md-12 col-xs-12">
								<?php echo $this->Form->input('seo_desc', ["chk"=>"isEmpty", 
									'type'=>'text', 'label'=>__('seo_desc'), 
									'placeholder'=>__('seo_desc'), 'class'=>'form-control'])?>
							</div>
							
							<div class="form-group col-md-12 col-xs-12 ">
                                <?=$this->element("tagInput", ["name"=>"seo_keywords"]);?>
                            </div>
							
							<div class="clearfix form-group col-xs-12 ">
								<label><?=__('req_photos')?></label>
								<?php echo $this->element('uploadForm', ['name'=>'req_photos', 
									'oldPhotos'=>$request->req_photos, "recId"=>$request->id, 
									"msg"=>"upload_req_photos_msg"]);?>
							</div>
							<div class="form-group  col-xs-12">
								<button class="btn btn-info" type="submit"><?php echo __('Submit')?></button>
							</div>
						</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
