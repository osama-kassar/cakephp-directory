<div>
    <h3>
		<?= __('Requests') ?>
    </h3>
    <div style="overflow-x: scroll;">
        <table class="table">
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('id', __('id')) ?></th>
                    <th><?= $this->Paginator->sort('req_title', __('req_title')) ?></th>
                    <th class="hideMob"><?= $this->Paginator->sort('subcategory_id', __('subcategory_id')) ?></th>
                    <th class="hideMob"><?= $this->Paginator->sort('stat_views', __('stat_views')) ?></th>
                    <th class="hideMob"><?= $this->Paginator->sort('stat_shares', __('stat_shares')) ?></th>
                    <th class="hideMob"><?= $this->Paginator->sort('status', __('status')) ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($requests as $request): ?>
                <tr>
                    <td><?= $this->Number->format($request->id) ?></td>
                    <td><?= h($request->req_title) ?></td>
                    <td class="hideMob"><?= $request->has('subcategory') ? 
						$this->Html->link($request->subcategory->category_name, 
							['controller' => 'Categories', 'action' => 'view', 
								$request->category->id]) : '' ?></td>
                    <td class="hideMob"><?= $this->Number->format($request->stat_views) ?></td>
                    <td class="hideMob"><?= $this->Number->format($request->stat_shares) ?></td>
                    <td class="hideMob"><?= $this->Number->format($request->status) ?></td>
                    <td class="actions">
                        <?php $request->status == 0 ? 
							$enable = '<i class="fa fa-check-circle" aria-hidden="true"></i>' : 
                            $enable = '<i class="fa fa-times-circle-o" aria-hidden="true"></i>';?>
                            
                        <?= $this->Html->link('<i class="fa fa-eye" aria-hidden="true"></i>', 
                            ['action' => 'view', $request->id], ['escape'=>false, 'class'=>'pageContent']) ?>
                            
                        <?= $this->Html->link('<i class="fa fa-pencil-square-o" aria-hidden="true"></i>', 
                            ['action' => 'edit', $request->id], ['escape'=>false, 'class'=>'pageContent']) ?>
                            
                        <?= $this->Form->postLink($enable, ['action' => 'disable', $request->id], 
                            ['confirm' => __('change_status').' '.$request->id, 'escape'=>false]) ?>
                            
                        <?= $this->Form->postLink('<i class="fa fa-trash" aria-hidden="true"></i>', 
                            ['action' => 'delete', $request->id], 
							['confirm' => __('delete_record').' '.$request->id, 'escape'=>false]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    
</div>

<div class="paginator">
    <ul class="pagination">
        <?= $this->Paginator->prev('< ' . __('previous')) ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next(__('next') . ' >') ?>
    </ul>
    <p><?= $this->Paginator->counter() ?></p>
</div>

