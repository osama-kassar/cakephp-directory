
<div id="vfx-product-inner-item">
	<div class="container">
		<div class="row">
			
			
			
			
			
			
			
			
			
			<!-- LIST -->
			<div class="col-md-9 col-sm-8 col-xs-12 nopadding">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="sorts-by-results">
						<div class="col-md-6 col-sm-6 col-xs-6">
							<span class="result-item-view">
								<?=__('query_result')?> <span class="yellow"><?=count($requests)?></span>
							</span>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-6">
								<?php 
									$urlvars = $this->Do->appendURL('', 'array');
									if(!empty($urlvars[0])){
										foreach($urlvars as $itm){
											$itm_arr = explode('=',$itm);
											echo '
											<span class="tag_remove pull-left">'.
												__($itm_arr[0]).'<a href="'.$this->Do->appendURL($itm, 'remove').'">
												 <i class="fa fa-times"></i></a> 
											</span>';
										}
									}
								?>
						</div>
						<!--<div class="col-md-6 col-sm-6 col-xs-6">
              <div class="disp-f-right">
                <div class="disp-style"><a href="listing_grid.html"><i class="fa fa-th"></i></a></div>
                <div class="disp-style active"><a href="listing_list.html"><i class="fa fa-th-list"></i></a></div>
              </div>
            </div>-->
					</div>
					<?php foreach ($requests as $request): ?>
					<div class="recent-listing-box-container-item list-view-item" style="direction:rtl;">
						<div class="col-md-4 nopadding feature-item-listing-item listing-item">
							<div class="recent-listing-box-image">
								<h1>
									<?=$request->category->category_name?>
								</h1>
								<?php $img = explode('|', $request->req_photos);?>
								<?=$this->Html->image('/img/requests_photos/thumb/'.$img[0]);?>
							</div>
							<div class="hover-overlay">
								<div class="hover-overlay-inner">
									<ul class="listing-links">
										<li>
											<?=$this->Html->link(
												'<i class="fa fa-pencil"></i> '.__('rec_edit'),
												['action'=>'edit', $request->id],['escape'=>false])?>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-md-8 nopadding">
							<div class="recent-listing-box-item">
								<div class="listing-boxes-text">
									<h3>
										<?=$this->Html->link(
											$request->req_title, 
											['action'=>'view', $request->slug])
										?>
									</h3>
									<p>
										<?=$request->req_desc?>
									</p>
								</div>
								<div class="recent-feature-item-rating">
									<h2><i class="fa fa-map-marker"></i> <?=$request->country->category_name?></h2>
									<span>
										<?=$request->category->category_name?>
									</span>
								</div>
							</div>
						</div>
					</div>
					<?php endforeach;?>
					<div class="vfx-person-block">
						<ul class="vfx-pagination">
							<?= $this->Paginator->prev('<i class="fa fa-angle-double-right" aria-hidden="true"></i>', ['escape'=>false]) ?>
							<?= $this->Paginator->numbers() ?>
							<?= $this->Paginator->next('<i class="fa fa-angle-double-left" aria-hidden="true"></i>', ['escape'=>false]) ?>
						</ul>
					</div>
				</div>
			</div>
			
			<?=$this->Element('user_sidebar')?>
		</div>
	</div>
</div>
