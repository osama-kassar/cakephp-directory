
<?php echo $this->Html->css('style_tm', [null, 'fullBase' => true]);?>


<div class="msg_tm"
style="
  @import url(http://fonts.googleapis.com/earlyaccess/droidarabickufi.css);
  font-family: 'Droid Arabic Kufi', serif;
  direction: rtl;
  text-align:center;
  max-width:1000px;
  margin:auto;
  padding:20px;
  border: 1px solid #7a9aaf;"
>
    <header style="
                   background:#7a9aaf; 
                   margin: 0; 
                   padding:20px;
                   min-height:70px;
                   border-bottom:1px solid #ccc; ">
        <div style="
                    width:50%; 
                    text-align:left; 
                    float:left;">
            <img src='<?=$_SERVER['HTTP_HOST'].$content['app_folder']?>/images/logo.png' 
                 style="max-width:100%; padding:10px 0;">
        </div>
        <div style="
                    width: 50%;
                    text-align: right; 
                    float: right;">
            <h1><?=__('welcome').' '.$content['firstname']?></h1>
        </div>
    </header>
    
    <div style="padding: 20px; font-size: 24px;">
        <br />
        <div><?=__('getpassword_message')?></div>
        <br />
        <div><?=__('temp_password').': <b>'.$content['new_password'].'</b>'?></div>
        <br />
    </div>
    
    <footer style="
                   text-align:left;
                   border-top:1px solid #ccc; 
                   background:#7a9aaf; 
                   padding:20px;">
        
        <div><?=__('email_signature')?></div>
        <br>
        <div style="text-align: left;">
            TURYKEYDE © 2019 <br>
            ALTINKEY DIJITAL PAZARLAMA VE TİCARET LIMITES SIRKETI <br>
            Turkey - Istanbul - Beylikduzu 34524 
        </div>
        <div>
            <?=$this->Html->link(__('terms-conditions'),[
                    '_full' => true,
                    "controller"=>"Pages", "action"=>"display",  "terms-conditions"])?> 
            - 
            <?=$this->Html->link(__('privacy-policy'),[
                    '_full' => true,
                    "controller"=>"Pages", "action"=>"display",  "privacy-policy"])?>
        </div>
    </footer>
</div>

