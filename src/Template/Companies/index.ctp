<div id="vfx-product-inner-item" ng-init="fsearch.page=pages_name.companies">
	<div class="container">
		<div class="row list_top_space">
		
		
		
		
		
			
			
			
			
			
			
			
			
			
			
			
			
			<!-- LIST -->
			<div class="col-md-9 col-sm-8 col-xs-12">
				<div class="sorts-by-results">
					<div class="col-md-3 col-sm-6 col-xs-6">
						<span class="result-item-view">
							<?=__('query_result')?> <span class="yellow"><?=$total?></span>
						</span>
					</div>
					<div class="col-md-9 col-sm-6 col-xs-6">
							<?php 
								$urlvars = $this->Do->appendURL('', 'array');
								if(!empty($urlvars[0])){
									foreach($urlvars as $itm){
										$itm_arr = explode('=',$itm);
										if(!in_array($itm_arr[0], $this->Do->get('filters'))){ continue; }
										if(!empty($itm_arr[1])){
											echo '
											<span class="tag_remove pull-left">'.
												__($itm_arr[0]).': '.$this->Do->getCatName($itm_arr)
												.'<a href="'.$this->Do->appendURL($itm, 'remove').'">
												 <i class="fa fa-times"></i></a> 
											</span>';
										}
									}
								}
							?>
					</div>
				</div>
				<div class="row">
				
					<?php 
					foreach($companies as $company):?>
					<div class="col-md-3 col-sm-6 col-xs-12">
						<div class="feature-item-container-box listing-item">
							<div class="feature-title-item">
								<h1>
									<?=$company->category->category_name?>
								</h1>
								<?=
								$this->Html->link(
									$this->Html->image('/img/companies_photos/thumb/'.
										$company->company_logo, ['alt'=>$company->company_logo]),
									['action'=>'view', $company->slug],
									["escape"=>false])?>
							</div>
							<div class="feature-box-text">
								<h3>
									<?=$this->Html->link(
											$company->company_name, 
											['action'=>'view', $company->slug])
										?>
								</h3>
								<a href="tel:<?=$company->company_phone?>">
									<i class="fa fa-phone"></i> <?=$company->company_phone?>
								</a>
							</div>
							
							<div class="feature-item-location">
								<h2><i class="fa fa-map-marker"></i> <?=__($company->city->category_name)?></h2>
								<span>
									<?=$this->Html->link(
										__('show_details'), 
										['action'=>'view', $company->slug])
										?>
								</span>
							</div>
						</div>
					</div>
					<?php endforeach?>
					<div class="vfx-person-block">
						<ul class="vfx-pagination">
							<?= $this->Paginator->prev('<i class="fa fa-angle-double-right" aria-hidden="true"></i>', ['escape'=>false]) ?>
							<?= $this->Paginator->numbers() ?>
							<?= $this->Paginator->next('<i class="fa fa-angle-double-left" aria-hidden="true"></i>', ['escape'=>false]) ?>
						</ul>
					</div>
				</div>
			</div>
		
		
		
			<!-- SIDE BAR -->
			<div class="col-md-3 col-sm-4 col-xs-12">
				<div class="news-search-lt">
					<form action="<?=$app_folder?>/<?=$currlang?>/<?=$pages_name[0]?>?keyword=<?=$this->request->getData('keyword')?>">
						<?=$this->Form->input("keyword", ["type"=>"text", 
							"placeholder"=>__('search'), 
							"label"=>false, "value"=>@$_GET['keyword'], 
							"class"=>"form-control",
							"ng-change"=>"loadSearch()",
							"ng-model"=>"fsearch.keyword"])?>
						
						<div class="col-sm-12 autocomplete_div" ng-if="searchResult.length>0">
							<div ng-repeat="itm in searchResult" class="autocomplete_item">
								<a href ng-click="goTo('/<?=$currlang?>/<?=$pages_name[0]?>/'+itm.slug);">
									{{itm[ fields[1][1] ]}}
								</a>
							</div>
						</div>
					</form>
					<span class="input-search">
					<i class="fa fa-search"></i>
					</span>
				</div>
				
				<div>
					<div class="form-group">
						<?php 
							echo $this->Html->link('
							  <button type="button" class="btn-quote" style="width:100%">
							  <i class="fa fa-plus-circle"></i>
							  '.__('add_your_company').'</button>',  '/mycompany', ['escape'=>false])?>
					</div>
				</div>
				
				
				<!-- categories r -->
				<div class="left-slide-slt-block" id="categories_list">
					<h3><?=__('categories')?></h3>
				</div>
				<div class="list-group">
					<?php
					foreach($categories as $category){  
						$lbl = ' <i class="'.($category->category_params == '' ? 
								'fa fa-briefcase' : $category->category_params).'"></i> '.$category->category_name;
						echo '<a href="'.$this->Do->appendURL('category='.$category->id)
						.'" class="list-group-item '.(@$this->request->query['category'] == $category->id ? 'active' : '').'">  '.$lbl.'</a>';
					?>
					<?php } ?>
					<?php if(count($categories) > 6):?>
					<button class="show_more" ng-click="showMore('categories_list')">
						<i class="fa fa-arrows-v" aria-hidden="true"></i>
						<?=__('show_more')?>
					</button>
					<?php endif ?>
				</div>
				
				<!-- countries filter -->
				<div class="left-slide-slt-block">
					<h3><?=__('countries')?></h3>
				</div>
				<div class="list-group" id="countries_list">
					<?php
					foreach($this->Do->lcl($countries) as $k=>$v){  
						$lbl = ' <i class="fa fa-map-marker"></i> '.$v;
						echo ' <a href="'.$this->Do->appendURL('country='.$k )
						.'" class="list-group-item '.(@$this->request->query['country'] == $k ? 'active' : '').'">  '.$lbl.'</a>';
					}
					?>
					<?php if(count($countries) > 6):?>
					<button class="show_more" ng-click="showMore('countries_list')">
						<i class="fa fa-arrows-v" aria-hidden="true"></i>
						<?=__('show_more')?>
					</button>
					<?php endif ?>
				</div>
			</div>
			
			
			
			
			
			
			
		</div>
	</div>
</div>
