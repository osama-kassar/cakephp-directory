<style>
.from-list-lt .from-input-ic {
	top: 28px;
}
</style>
<div id="breadcrum-inner-block">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 text-center">
				<div class="breadcrum-inner-header">
					<h1>
						<?=__('dashboard')?>
					</h1>
					<?=$this->Html->link(__('home'), ['/'])?>
					<i class="fa fa-circle"></i>
					<?=$this->Html->link(__('mycompany'), ['/mycompany'])?>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="pricing-item-block" class="white_bg_block ">
	<div class="container ">
		<div class="row ">
			
			<?=$this->element('user_sidebar')?>
			
			<!-- body -->
			<div class="col-md-9 col-sm-8 col-xs-12  ">
				<div class="col-md-12 all-categorie-list-title bt_heading_3 text-center">
					<h1>
						<?=__('mycompany')?>
					</h1>
					<div class="blind line_1"></div>
					<div class="flipInX-1 blind icon"><span class="icon"><i class="fa fa-stop"></i>&nbsp;&nbsp;<i class="fa fa-stop"></i></span></div>
					<div class="blind line_2"></div>
				</div>
				<div class="from-list-lt row">
					<div class="col-md-12">
						<div class="col-sm-12" >
							<p><?php echo __('edit_company_msg'); ?></p>
						</div>
						<div class="col-sm-12" >
							<?php echo  $this->Form->create($company, 
								['enctype'=>'multipart/form-data', 
								'type'=>'post', 'class'=>'form-float form-alt', 
								'novalidate' => true]) ?>
							<div class="row">
							
								<div class="form-group col-md-6 col-xs-12">
									<span class="from-input-ic"><i class="fa fa-bars"></i></span>
									<?php echo $this->Form->input('language_id', 
										['type' => 'select', "chk"=>"isSelectEmpty", 
										'options' => $this->Do->lcl($this->Do->get('lang')), 'empty' => __('language_id'), 
										'label'=>__('language_id'), 
										'class'=>'form-control'])?>
								</div>
								
								<div class="form-group col-md-12 col-xs-12">
									<span class="from-input-ic"><i class="fa fa-header"></i></span>
									<?php echo $this->Form->input('company_name', ["chk"=>"isEmpty", 
										'type'=>'text', 'label'=>__('company_name'), 
										'placeholder'=>__('company_name'), 'class'=>'form-control'])?>
								</div>
								
								<div class="form-group col-md-6 col-xs-12">
									<span class="from-input-ic"><i class="fa fa-bars"></i></span>
									<?php echo $this->Form->input('category_id', ["chk"=>"isSelectEmpty", 
										'onClick'=>'getSubCat(0, "subcategory-id")',
										'onChange'=>'getSubCat(this, "subcategory-id")', 'type' => 'select', 
										'options' => @$categories, 'empty' => __('f_category'), 'label'=>__('category_id'), 
										'class'=>'form-control'])?>
								</div>
								
								<div class="form-group col-md-6 col-xs-12">
									<span class="from-input-ic"><i class="fa fa-bars"></i></span>
									<?php echo $this->Form->input('subcategory_id', ["chk"=>"isSelectEmpty", 
										'type'=>'select', 'options'=>@$subcategories, 
										'label'=>__('subcategory_id'), 'class'=>'form-control'])?>
								</div>
								
								<div class="form-group col-md-12 col-xs-12">
									<span class="from-input-ic"><i class="fa fa-envelope-o"></i></span>
									<?php echo $this->Form->input('company_desc', ["chk"=>"isParagraph" , 
										'type'=>'textarea', 'label'=>__('company_desc'), 
										'placeholder'=>__('company_desc'), 'class'=>'form-control'])?>
								</div>
								
								<div class="form-group col-md-6 col-xs-12">
									<span class="from-input-ic"><i class="fa fa-phone"></i></span>
									<?php echo $this->Form->input('company_phone', ["chk"=>"isPhone", 
										'type'=>'text', 'label'=>__('company_phone'), 
										'placeholder'=>__('company_phone'), 'class'=>'form-control'])?>
								</div>
								
								<div class="form-group col-md-6 col-xs-12">
									<span class="from-input-ic"><i class="fa fa-whatsapp"></i></span>
									<?php echo $this->Form->input('company_links_0', ["chk"=>"isPhone", 
										'type'=>'text', 'label'=>__('whatsapp'), 
										'placeholder'=>__('whatsapp_ph'), 'class'=>'form-control'])?>
								</div>
								
								<div class="form-group col-md-6 col-xs-12">
									<span class="from-input-ic"><i class="fa fa-at"></i></span>
									<?php echo $this->Form->input('company_email', ["chk"=>"isEmail", 
										'type'=>'text', 'label'=>__('company_email'), 
										'placeholder'=>__('company_email'), 'class'=>'form-control'])?>
								</div>
								
                                <div class="row">
                                    <div class="col-xs-12 ">
                                        <div class="form-group col-md-6 col-xs-12"> 
                                            <span class="from-input-ic"><i class="fa fa-globe"></i></span>
                                            <?php echo $this->Form->input('country_id', ["chk"=>"isSelectEmpty", 
                                                'onClick'=>'getSubCat(0, "city-id")',
                                                'onChange'=>'getSubCat(this, "city-id")', 'type' => 'select', 
                                                'options' => @$countries, 
                                                'label'=>__('country_id'), 'class'=>'form-control', 'disabled'=>true])?>
                                        </div>

                                        <div class="form-group col-md-6 col-xs-12">
                                            <span class="from-input-ic"><i class="fa fa-globe"></i></span>
                                            <?php echo $this->Form->input('city_id', ["chk"=>"isSelectEmpty", 
                                            'options' => @$cities, 'type'=>'select', 
                                            'label'=>__('city_id'), 'empty' => __('f_city'), 
                                            'class'=>'form-control'])?>
                                        </div>
                                    </div>
                                </div>
                                
								<div class="form-group col-md-12 col-xs-12">
									<span class="from-input-ic"><i class="fa fa-map-o"></i></span>
									<?php echo $this->Form->input('company_location', ["chk"=>"isEmpty", 
										'type'=>'text', 'label'=>__('company_location'), 
										'placeholder'=>__('company_location'), 'class'=>'form-control', 
										"onfocus"=>"document.getElementById('gmap').classList.add('defold')"])?>
									
									<div class="foldable" id="gmap">
									<?=$this->element('googlemap', 
										['mapplace'=>$company->company_location, 
										'w'=>'100%', 'h'=>'350px', 
										"markerTitle"=>$company->company_name, 
										'draggableMarker'=>true]);?>
									</div>
								</div>
								
								<div class="form-group col-md-12 col-xs-12">
									<div>
										<label> <?=__('company_worktime');?> </label>
									</div>
                                    <div class="mychckbox_container">
                                        <?= $this->Form->select('company_worktime', 
                                            $this->Do->lcl( $this->Do->get("days") ),
                                                 ['multiple'=>'checkbox',
                                                  'label'=>['class'=>'mycheckbox gray'],
                                                  'chk'=>'isEmpty']); ?>
                                        
                                        <?php echo $this->Form->input('company_worktime_from', [ 
										'type'=>'text', 'readonly'=>true, 'label'=>__('from'), 
										'placeholder'=>__('company_worktime_from'), 'class'=>'form-control ltr'])?>
                                        
                                        <script type="text/javascript">
                                                $('#company-worktime-from').timepicker({
                                                    format: 'LT',
                                                    defaultTime: '9:30 AM'
                                                });
                                        </script>
                                        
                                        <?php echo $this->Form->input('company_worktime_to', [
										'type'=>'text', 'readonly'=>true, 'label'=>__('to'), 
										'placeholder'=>__('company_worktime_to'), 'class'=>'form-control ltr'])?>
                                        
                                        <script type="text/javascript">
                                                $('#company-worktime-to').timepicker({
                                                    format: 'LT',
                                                    defaultTime: '6:00 PM'
                                                });
                                        </script>
									</div>
								</div>
								
								<div class="form-group col-md-12 col-xs-12">
									<span class="from-input-ic"><i class="fa fa-envelope-o"></i></span>
									<?php echo $this->Form->input('company_address', ["chk"=>"isEmpty", 
										'type'=>'textarea', 'label'=>__('company_address'), 
										'placeholder'=>__('company_address_desc'), 'class'=>'form-control'])?>
								</div>
									
								<div class="clearfix form-group col-xs-12">
									<div>
										<label> <?=__('company_preferred_langs');?> </label>
									</div>
									<div class="mychckbox_container ">
										<?= $this->Form->select('company_preferred_langs', 
                                            $this->Do->lcl( $this->Do->get('prefered_langs') ),
                                                 ['multiple'=>'checkbox',
                                                  'label'=>['class'=>'mycheckbox gray'],
                                                  'chk'=>'isEmpty']); ?>
									</div>
								</div>
								
								<div class="clearfix form-group col-xs-12">
                                    <h3><?=__("socialmedia_links")?></h3>
									<?php 
									$social_media = $this->Do->get('socialmedia');
                                    $vals = explode(":link:", $company->company_links);
                                        
									for($i=1; $i<count($social_media); $i++):?>
									<div class=" form-group col-md-6 col-xs-12">
										<span class="from-input-ic"><i class="fa fa-<?=$social_media[$i]?>"></i></span>
										<?php echo $this->Form->input('company_links_'.$i,  
											['type'=>'text', 'value'=>@$vals[$i], 
											'label'=>__($social_media[$i]), 
											'placeholder'=>__('http://'.$social_media[$i].'.com/you-number'), 
											'class'=>'form-control ltr'])?>
									</div>
									<?php endfor?>
								</div>
								
								<div class="form-group col-md-12 col-xs-12">
									<label><?=__('company_logo')?></label>
									<?php echo $this->element('uploadForm', 
										['name'=>'logo', 'photosNumber'=>1, 
										"msg"=>"upload_company_logo_msg"]);?>
								</div>
								
								<div class="form-group col-md-12 col-xs-12">
									<label><?=__('company_photos')?></label>
									<?php echo $this->element('uploadForm', 
										["msg"=>"upload_company_photos_msg", 
										'name'=>'photo']);?>
								</div>
							
								<div class="form-group col-xs-12">
									<span class="from-input-ic"><i class="fa fa-info-o"></i></span>
									<?php echo $this->Form->input('seo_desc', ["chk"=>"isEmpty", 
										'type'=>'text', 'label'=>__('seo_desc'), 
										'placeholder'=>__('seo_desc'), 'class'=>'form-control'])?>
								</div>
                                
								<div class="form-group col-md-12 col-xs-12 ">
                                    <?=$this->element("tagInput", ["name"=>"seo_keywords"]);?>
                                </div>
								
								<div class="form-group col-md-12 col-xs-12">
									<span class="from-input-ic"><i class="fa fa-play"></i></span>
									<?php echo $this->Form->input('company_videos', 
										['type'=>'text', 'label'=>__('company_videos'), 
										'placeholder'=>__('company_videos_ph'), 'class'=>'form-control ltr'])?>
								</div>
								
								<div class="form-group col-md-6 col-xs-12">
									<button class="btn " type="submit"><?php echo __('Submit')?></button>
								</div>
							</div>
							<br />
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

