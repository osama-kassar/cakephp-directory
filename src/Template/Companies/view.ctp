<?php
	$this->assign('title',$company->company_name);
	$this->assign('keywords',$company->seo_keywords);
	$this->assign('description',$company->seo_desc);
?>

<!-- HEADER -->
<div class="details-lt-block">
	<div class="container header_slt_block">
		<div class="slt_item_head">
			<div class="user_logo_pic">
				<?=$this->Html->image('/img/companies_photos/thumb/'.$company->company_logo)?>
			</div>
			<div class="slt_item_contant">
				<h1>
					<?=$company->company_name?>
				</h1>
				<!--<div class="rating-box">
					<div class="rating">
						<span>
						<i class="fa fa-star"></i>
						<i class="fa fa-star"></i>
						<i class="fa fa-star"></i>
						<i class="fa fa-star"></i>
						<i class="fa fa-star-o"></i>
						</span>
					</div>
				</div>-->
				<div class="head-bookmark-bock">
					<div class="detail-banner-btn">
						<a href="#">
							<i class="fa fa-bars"></i>
							<?=$company->category->category_name?>
						</a>
					</div>
					<div class="detail-banner-btn">
						<a href="#">
							<i class="fa fa-bars"></i>
							<?=$company->subcategory->category_name?>
						</a>
					</div>
					<div class="location">
						<i class="fa fa-map-marker"></i>
						<?=__($company->city->category_name)?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- BODY -->
<div id="vfx-product-inner-item">
	<div class="container">
		<div class="row">
		
			
			<!-- SIDE BAR -->
			<div class="col-md-3 col-sm-4 col-xs-12">
				<h2>
					<?=$company->company_name?>
				</h2>
				<div class="left-slide-slt-block">
					<button class="btn-quote" style="width:100%;">
					<i class="fa fa-info-circle"></i>
					<?=__('order_info')?>
					</button>
				</div>
				
				<!-- company address -->
				<div class="sidebar_box">
					<h3>
						<?=__('company_address')?>
					</h3>
					<i class="fa fa-map-marker"></i>
					<?=$company->company_address?>
				</div>
				
				<!-- company phone -->
				<div class="sidebar_box">
					<h3>
						<?=__('company_phone')?>
					</h3>
					<button class="btn-quote" style="width:100%;"
			onclick="tel:<?=$company->company_phone?>">
					<i class="fa fa-phone"></i>
					<?=$company->company_phone?>
					</button>
				</div>
				
				<!-- company whatsapp -->
				<?php 
		$links = explode(':link:',$company->company_links);
		if(!empty( $links[0] )):?>
				<div class="sidebar_box">
					<h3>
						<?=__('company_whatsapp')?>
					</h3>
					<button class="btn-quote" style="width:100%;" 
			onclick="goTo('https://api.whatsapp.com/send?phone=<?=$links[0]?>&text=<?=__('whatsapp_msg')?>')">
					<i class="fa fa-whatsapp"></i>
					<?=$links[0]?>
					</button>
				</div>
				<?php endif?>
				
				<!-- contact company -->
				<div class="sidebar_box">
					<h3>
						<?=__('send_message')?>
					</h3>
					<button data-toggle="modal" 
						data-target="#sendmessage" class="btn-quote" style="width:100%;" >
						<i class="fa fa-paper-plane"></i> 
						<?=__('contact_us')?> 
					</button>
				</div>
				
				<!-- company social media -->
				<div class="sidebar_box">
					<h3>
						<?=__('company_socialmedia')?>
					</h3>
					<ul class="social-icons">
						<?php
			$sm = $this->Do->get('socialmedia');
			for($i=1; $i<(count($links)); $i++){
				if(!empty( $links[$i] )){
					echo '<li><a href="'.$links[$i].'"><i class="fa fa-'.$sm[$i].'"></i></a></li>';
				}
			}
		?>
					</ul>
				</div>
				
				<!-- company work time -->
				<div class="dlt-spons-item sidebar_box">
					<h3>
						<?=__('company_worktime')?>
					</h3>
					<i class="fa fa-clock-o"></i>
                    
					<?php 
                        $all = explode(", ", $company->company_worktime);
                        $days = $this->Do->lcl( $this->Do->get('days') );
                        $indexes = explode("-", $all[0]);
                        for($i=0; $i<count( $indexes ); $i++){
                            echo '<a>'.$days[ $indexes[$i] ].'</a>';
                        }
                        echo '<div><a>'.__('from').' '.$all[1].' - '.__('to').' '.$all[2].'</a></div>';
                    ?>
				</div>
				
				<!-- company freferred languages -->
				<div class="dlt-spons-item sidebar_box">
					<h3>
						<?=__('company_languages')?>
					</h3>
					<?php 
                        $langs = explode(",", $company->company_preferred_langs);
                        $l = $this->Do->get('lang');
                        foreach( $langs as $lang ){
                            echo '<a> '.__($l[$lang]).' </a>';
                        }
                    ?>
				</div>
				
				<!-- map loation -->
				<div class="sidebar_box">
					<h3>
						<?=__('company_map')?>
					</h3>
					<div><i class="fa fa-map-marker"></i>
						<?=$company->company_location?>
					</div>
					<div>
						<?php echo $this->element("googlemap", ["mapplace"=>$company->company_location, 
				"w"=>"100%", "h"=>"200px", 
				"markerTitle"=>$company->company_name, 
				"id"=>"map".$company->id]);?>
					</div>
				</div>
			</div>
		
		
		
		
			<div class="col-md-9 col-sm-8 col-xs-12">
				<div class="slider">
					<div class="detail-gallery">
						<div class="detail-gallery-preview">
						<?php
						$imgs = explode("|",$company->company_photos);
						echo $this->Html->link(
							$this->Html->image("/img/companies_photos/".$imgs[0]),
							"/img/companies_photos/".$imgs[0], ["escape"=>false]
						);?>
						</div>
					
				<?php 
				echo '
				<div class="owl-carousel owl-theme owl-loaded owl-drag">
					<div class="owl-stage-outer">
						<ul class="detail-gallery-index">';
				for($i=0; $i<count($imgs); $i++){
					echo '<li class="detail-gallery-list-item active">';	
					echo $this->Html->link(
							$this->Html->image("/img/companies_photos/thumb/".$imgs[$i]), 
							"javascript:void();", 
							["data-target"=>$app_folder."/img/companies_photos/".$imgs[$i], 
							"escape"=>false, "class"=>"group1"]
					);
					echo '</li>';
				}
				echo '</ul>
					</div>
				</div>';
							?>
						</div>
					</div>
					<div class="dlt-title-item">
						<h2>
							<?=$company->company_name?>
						</h2>
						<div>
							<?=__('stat_views').' '.$company->stat_views?>
							<span>
							<?=$company->stat_created?>
							</span>
						</div>
						<article>
							<?=$company->company_desc?>
						</article>
						<p>
							<?=$this->element('share',["obj"=>$company, "mdl"=>"Companies"])?>
						</p>
					</div>
					<div class="dlt-spons-item">
						<?php 
				$tags = explode(",",$company->seo_keywords);
				$vids = explode(",",$company->company_videos);
				if(!empty($tags[0])){
					foreach($tags as $tag){
						echo '<a href="javascript:void()"><i class="fa fa-tag"></i> '.$tag.'</a>';
					}
				}
						?>
					</div>
					<?php 
                
                        if(! empty($vids[0]) ){
                            $vid_url = '';
                            if(strpos($vids[0], 'youtube')>-1){
                                $vid_url = 'https://www.youtube.com/embed/'.explode("v=", $vids[0])[1];
                            }
                            if(strpos($vids[0], 'vimeo')>-1){
                                $vid_url = 'https://player.vimeo.com/video/'.explode("/", $vids[0])[3];
                            }
                            if($vid_url == ''){
                                echo __('video_url_wrong');
                            }
                    ?>
					<div class="detail-content">
						<h2>
							<?=__('company_videos')?>
						</h2>
						<div class="detail-video">
							<iframe width="850" height="350" src="<?=$vid_url?>" frameborder="0" allowfullscreen></iframe>
						</div>
					</div>
					<?php }?>
					
					
				</div>

				
			</div>
		</div>
	</div>
</div>