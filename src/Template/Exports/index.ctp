<?php /*?><nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Export'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Companies'), ['controller' => 'Companies', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Company'), ['controller' => 'Companies', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Categories'), ['controller' => 'Categories', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Category'), ['controller' => 'Categories', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="exports index large-9 medium-8 columns content">
    <h3><?= __('Exports') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('company_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('category_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('subcategory_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('exp_name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('exp_price') ?></th>
                <th scope="col"><?= $this->Paginator->sort('exp_method') ?></th>
                <th scope="col"><?= $this->Paginator->sort('exp_deliver') ?></th>
                <th scope="col"><?= $this->Paginator->sort('exp_phone') ?></th>
                <th scope="col"><?= $this->Paginator->sort('country_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('exp_desc') ?></th>
                <th scope="col"><?= $this->Paginator->sort('exp_photos') ?></th>
                <th scope="col"><?= $this->Paginator->sort('exp_date') ?></th>
                <th scope="col"><?= $this->Paginator->sort('stat_views') ?></th>
                <th scope="col"><?= $this->Paginator->sort('stat_saved') ?></th>
                <th scope="col"><?= $this->Paginator->sort('stat_loved') ?></th>
                <th scope="col"><?= $this->Paginator->sort('stat_shared') ?></th>
                <th scope="col"><?= $this->Paginator->sort('seo_keywords') ?></th>
                <th scope="col"><?= $this->Paginator->sort('seo_desc') ?></th>
                <th scope="col"><?= $this->Paginator->sort('status') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($exports as $export): ?>
            <tr>
                <td><?= $this->Number->format($export->id) ?></td>
                <td><?= $export->has('company') ? $this->Html->link($export->company->company_name, ['controller' => 'Companies', 'action' => 'view', $export->company->id]) : '' ?></td>
                <td><?= $export->has('category') ? $this->Html->link($export->category->category_name, ['controller' => 'Categories', 'action' => 'view', $export->category->id]) : '' ?></td>
                <td><?= $this->Number->format($export->subcategory_id) ?></td>
                <td><?= h($export->exp_name) ?></td>
                <td><?= $this->Number->format($export->exp_price) ?></td>
                <td><?= h($export->exp_method) ?></td>
                <td><?= h($export->exp_deliver) ?></td>
                <td><?= $this->Number->format($export->exp_phone) ?></td>
                <td><?= $export->has('country') ? $this->Html->link($export->country->category_name, ['controller' => 'Categories', 'action' => 'view', $export->country->id]) : '' ?></td>
                <td><?= h($export->exp_desc) ?></td>
                <td><?= h($export->exp_photos) ?></td>
                <td><?= h($export->exp_date) ?></td>
                <td><?= $this->Number->format($export->stat_views) ?></td>
                <td><?= $this->Number->format($export->stat_saved) ?></td>
                <td><?= $this->Number->format($export->stat_loved) ?></td>
                <td><?= $this->Number->format($export->stat_shared) ?></td>
                <td><?= h($export->seo_keywords) ?></td>
                <td><?= h($export->seo_desc) ?></td>
                <td><?= $this->Number->format($export->status) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $export->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $export->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $export->id], ['confirm' => __('Are you sure you want to delete # {0}?', $export->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
<?php */?>








<div id="vfx-product-inner-item">
  <div class="container">
    <div class="row">
      <div class="col-md-3 col-sm-4 col-xs-12">
        <div class="news-search-lt">
          <input class="form-control" placeholder="Search" type="text">
          <span class="input-search"> <i class="fa fa-search"></i> </span> </div>
        <div class="left-slide-slt-block">
          <h3>Categories</h3>
        </div>
        <div class="list-group"> <a href="#" class="list-group-item active"><i class="fa fa-hand-o-right"></i> Business <span class="list-lt">15</span></a> <a href="#" class="list-group-item"><i class="fa fa-hand-o-right"></i> Health & Fitness <span class="list-lt">09</span></a> <a href="#" class="list-group-item"><i class="fa fa-hand-o-right"></i> Real Estate <span class="list-lt">18</span></a> <a href="#" class="list-group-item"><i class="fa fa-hand-o-right"></i> Entertainment <span class="list-lt">24</span></a> <a href="#" class="list-group-item"><i class="fa fa-hand-o-right"></i> Beauty & Spas <span class="list-lt">06</span></a> <a href="#" class="list-group-item"><i class="fa fa-hand-o-right"></i> Automotive <span class="list-lt">04</span></a> <a href="#" class="list-group-item"><i class="fa fa-hand-o-right"></i> Hotels & Travel <span class="list-lt">14</span></a> <a href="#" class="list-group-item"><i class="fa fa-hand-o-right"></i> Sports & Adventure <span class="list-lt">07</span></a> <a href="#" class="list-group-item"><i class="fa fa-hand-o-right"></i> Technology <span class="list-lt">12</span></a> <a href="#" class="list-group-item"><i class="fa fa-hand-o-right"></i> Arts & Entertainment <span class="list-lt">26</span></a> <a href="#" class="list-group-item"><i class="fa fa-hand-o-right"></i> Education & Learning <span class="list-lt">24</span></a> <a href="#" class="list-group-item"><i class="fa fa-hand-o-right"></i> Cloth Shop <span class="list-lt">16</span></a> </div>
        <div class="left-slide-slt-block">
          <h3>Popular Tags</h3>
        </div>
        <div class="archive-tag">
          <ul>
            <li><a href="#" class="active">Amazing</a></li>
            <li><a href="#">Envato</a></li>
            <li><a href="#">Themes</a></li>
            <li><a href="#">Clean</a></li>
            <li><a href="#">Responsivenes</a></li>
            <li><a href="#">SEO</a></li>
            <li><a href="#">Mobile</a></li>
            <li><a href="#">IOS</a></li>
            <li><a href="#">Flat</a></li>
            <li><a href="#">Design</a></li>
          </ul>
        </div>
        <div class="left-slide-slt-block">
          <h3>Location List</h3>
        </div>
        <div class="left-location-item">
          <ul>
            <li><a href="#"><i class="fa fa-angle-double-right"></i> Manchester</a><span class="list-lt">07</span></li>
            <li><a href="#"><i class="fa fa-angle-double-right"></i> Lankashire</a><span class="list-lt">04</span></li>
            <li><a href="#"><i class="fa fa-angle-double-right"></i> New Mexico</a><span class="list-lt">03</span></li>
            <li><a href="#"><i class="fa fa-angle-double-right"></i> Nevada</a><span class="list-lt">06</span></li>
            <li><a href="#"><i class="fa fa-angle-double-right"></i> Kansas</a><span class="list-lt">08</span></li>
            <li><a href="#"><i class="fa fa-angle-double-right"></i> West Virginina</a><span class="list-lt">05</span></li>
          </ul>
        </div>
        <div class="left-slide-slt-block">
          <h3>Archives</h3>
        </div>
        <div class="left-archive-categor">
          <ul>
            <li><a href="#"><i class="fa fa-angle-double-right"></i> January 2016</a><span class="list-lt">09</span></li>
            <li><a href="#"><i class="fa fa-angle-double-right"></i> February 2016</a><span class="list-lt">52</span></li>
            <li><a href="#"><i class="fa fa-angle-double-right"></i> March 2016</a><span class="list-lt">36</span></li>
            <li><a href="#"><i class="fa fa-angle-double-right"></i> April 2016</a><span class="list-lt">78</span></li>
            <li><a href="#"><i class="fa fa-angle-double-right"></i> May 2016</a><span class="list-lt">66</span></li>
            <li><a href="#"><i class="fa fa-angle-double-right"></i> June 2016</a><span class="list-lt">15</span></li>
          </ul>
        </div>
      </div>
      <div class="col-md-9 col-sm-8 col-xs-12 nopadding">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="sorts-by-results">
            <div class="col-md-6 col-sm-6 col-xs-6"> <span class="result-item-view">Your Search Returned <span class="yellow"><?=count($exports)?></span> Results</span> </div>
            <!--<div class="col-md-6 col-sm-6 col-xs-6">
              <div class="disp-f-right">
                <div class="disp-style"><a href="listing_grid.html"><i class="fa fa-th"></i></a></div>
                <div class="disp-style active"><a href="listing_list.html"><i class="fa fa-th-list"></i></a></div>
              </div>
            </div>-->
          </div>
          <?php foreach ($exports as $export){ ?>
          <div class="recent-listing-box-container-item list-view-item" style="direction:rtl;">
            <div class="col-md-4 nopadding feature-item-listing-item listing-item">
              <div class="recent-listing-box-image">
                <h1><?=$export->category->category_name?></h1>
                <img src="images/product/img1.png" alt="img1"> </div>
              <div class="hover-overlay">
                <div class="hover-overlay-inner">
                  <ul class="listing-links">
                    <li><a href="#"><i class="fa fa-heart green-1"></i></a></li>
                    <li><a href="#"><i class="fa fa-map-marker blue-1"></i></a></li>
                    <li><a href="#"><i class="fa fa-share yallow-1"></i></a></li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="col-md-8 nopadding">
              <div class="recent-listing-box-item">
                <div class="listing-boxes-text">
                  <h3><?=$export->exp_name?></h3>
                  <p><?=$export->exp_desc?></p>
                </div>
                <div class="recent-feature-item-rating">
                  <h2><i class="fa fa-map-marker"></i><?=$export->country_id?></h2>
                  <span> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> </span> </div>
              </div>
            </div>
          </div>
            <?php }?>
            
        </div>
      </div>
    </div>
  </div>
</div>
