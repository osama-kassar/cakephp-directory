<style>
.from-list-lt .from-input-ic {
	top: 28px;
}
</style>
<div class="container">
	<div id="breadcrum-inner-block">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 text-center">
					<div class="breadcrum-inner-header">
						<h1> <?php echo __('add_export')?> </h1>
					</div>
				</div>
			</div>
		</div>
	</div>
	<p> <?php echo __('add_export_msg')?> </p>
	<div class="from-list-lt">
		<div class="col-sm-8 col-sm-offset-2"> <?php echo  $this->Form->create($export, [ 'class'=>'form-float form-alt' , 'novalidate' => true]) ?>
			<div class="row">
				<div class="form-group col-md-6 col-xs-12"> <span class="from-input-ic"><i class="fa fa-user"></i></span> <?php echo $this->Form->input('exp_name', ['type'=>'text', 'label'=>__('exp_name'), 'placeholder'=>__('exp_name'), 'class'=>'form-control'])?> </div>
				<div id="price">
					<div class="form-group col-md-2 col-xs-2"> <span class="from-input-ic"><i class="fa fa-money"></i></span> <?php echo $this->Form->input('exp_currency', 
					['type'=>'select', 'label'=>__('exp_currency'), 'class'=>'form-control', 'options'=>$exp_currency])?> </div>
					<div  class="form-group col-md-4 col-xs-4"> <span class="from-input-ic"><i class="fa fa-usd"></i></span> <?php echo $this->Form->input('exp_price', ['type'=>'text', 'label'=>__('exp_price'), 'placeholder'=>__('exp_price'), 'class'=>'form-control'])?> </div>
				</div>
				<div class="form-group col-md-6 hideElm"> </div>
				<div class="form-group col-md-6 "> <?php echo $this->Form->input('exp_price_0', ['type'=>'checkbox', 'label'=>['text' => '<span>'.__('exp_price_0').'</span>',  'class' => 'mycheckbox gray', 'escape'=>false], "onchange"=>"disElm($(this).is(':checked'), 'price')"])?> </div>
				<div class="form-group col-md-6 col-xs-12"> <span class="from-input-ic"><i class="fa fa-bars"></i></span> <?php echo $this->Form->input('category_id', 
				['empty' => __('f_category'),'type'=>'select', 
				'label'=>__('category_id'), 'class'=>'form-control'])?> </div>
				<div class="form-group col-md-6 col-xs-12"> <span class="from-input-ic"><i class="fa fa-globe"></i></span> <?php echo $this->Form->input('country_id', 
				['type'=>'select', 'label'=>__('country_id'),
				'empty' => __('f_country'), 'class'=>'form-control'])?> </div>
				<div class="form-group col-md-6 col-xs-12"> <span class="from-input-ic"><i class="fa fa-phone"></i></span> <?php echo $this->Form->input('exp_phone', ['type'=>'text', 'label'=>__('exp_phone'), 'placeholder'=>__('exp_phone'), 'class'=>'form-control'])?> </div>
				<div class="form-group col-md-12"> <span class="from-input-ic"><i class="fa fa-envelope-o"></i></span> <?php echo $this->Form->input('exp_desc', ['type'=>'textarea', 'label'=>__('exp_desc'), 'placeholder'=>__('exp_desc'), 'class'=>'form-control'])?> </div>
				<div class=" form-group col-md-6">
					<div>
						<label>
							<?=__('exp_method');?>
						</label>
					</div>
					<div class="mychckbox_container">
						<?php  
					foreach($exp_method as $k=>$val){
						 echo $this->Form->input('exp_method', [
							'type'=>'checkbox', 
							'label'=>['text' => '<span>'.__($val).'</span>',  
							'class' => 'mycheckbox gray', 'escape'=>false], 
							'name'=>'exp_method[]', 'id'=>'exp_method_'.$k, 'value'=>$k,
							'hiddenField' => false]);
					}
				?>
					</div>
				</div>
				<div class=" form-group col-md-6">
					<div>
						<label>
							<?=__('exp_deliver');?>
						</label>
					</div>
					<div class="mychckbox_container">
						<?php  
					foreach($exp_deliver as $k=>$val){
						 echo $this->Form->input('exp_deliver', [
							'type'=>'checkbox', 
							'label'=>['text' => '<span>'.__($val).'</span>',  
							'class' => 'mycheckbox gray', 'escape'=>false], 
							'name'=>'exp_deliver[]', 'id'=>'exp_deliver_'.$k, 'value'=>$k,
							'hiddenField' => false]);
					}
				?>
					</div>
				</div>
				<?php /*?>
		<div class="form-group col-md-6 col-xs-12"> <span class="from-input-ic"><i class="fa fa-tags"></i></span>
            <?php echo $this->Form->input('city_id', 
				['type'=>'select', 'label'=>__('city_id'), 'empty' => __('f_city'), 'class'=>'form-control'])?>
          </div>
		
		<div class="form-group col-md-6 col-xs-12"> <span class="from-input-ic"><i class="fa fa-tags"></i></span>
            <?php echo $this->Form->input('exp_links', ['type'=>'textarea', 'label'=>__('exp_links'), 'placeholder'=>__('exp_links'), 'class'=>'form-control', 'readonly', 'data-toggle'=>"modal", 'data-target'=>"#addlink"])?>
          </div>
		<?php */?>
				<div class="clearfix form-group col-xs-12 ">
					<div>
						<label>
							<?=__('exp_photos');?>
						</label>
					</div>
					<?php echo $this->element('uploadForm', ['name'=>'photo_', 'photosNumber'=>12]);?> </div>
				<div class="form-group col-md-6 col-xs-12"> <span class="from-input-ic"><i class="fa fa-location-arrow"></i></span> <?php echo $this->Form->input('exp_address', ['type'=>'textarea', 'label'=>__('exp_address'), 'placeholder'=>__('exp_address_desc'), 'class'=>'form-control'])?> </div>
				<div class="form-group col-md-6 col-xs-12"> <span class="from-input-ic"><i class="fa fa-paragraph"></i></span> <?php echo $this->Form->input('seo_desc', ['type'=>'textarea', 'label'=>__('seo_desc'), 'placeholder'=>__('seo_desc_desc'), 'class'=>'form-control'])?> </div>
				<div class="form-group col-md-12"> <span class="from-input-ic"><i class="fa fa-key"></i></span> <?php echo $this->Form->input('seo_keywords', ['type'=>'text', 'label'=>__('seo_keywords'), 'placeholder'=>__('seo_keywords'), 'class'=>'form-control'])?> </div>
				<div class="form-group col-md-12">
					<button class="btn pull-right" type="submit"><?php echo __('Submit')?></button>
				</div>
			</div>
			<br />
			</form>
		</div>
	</div>
</div>
<!-- registration form --> 
<?php echo $this->element('addlink')?> 
<script>
var disElm = function(v, tar){
	return $('#'+tar).find("*").prop('disabled', v);
}


</script>