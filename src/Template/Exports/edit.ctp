<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $export->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $export->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Exports'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Companies'), ['controller' => 'Companies', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Company'), ['controller' => 'Companies', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Categories'), ['controller' => 'Categories', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Category'), ['controller' => 'Categories', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="exports form large-9 medium-8 columns content">
    <?= $this->Form->create($export) ?>
    <fieldset>
        <legend><?= __('Edit Export') ?></legend>
        <?php
            echo $this->Form->control('company_id', ['options' => $companies]);
            echo $this->Form->control('category_id', ['options' => $categories]);
            echo $this->Form->control('subcategory_id');
            echo $this->Form->control('exp_name');
            echo $this->Form->control('exp_price');
            echo $this->Form->control('exp_method');
            echo $this->Form->control('exp_deliver');
            echo $this->Form->control('exp_phone');
            echo $this->Form->control('country_id', ['options' => $countries]);
            echo $this->Form->control('exp_desc');
            echo $this->Form->control('exp_photos');
            echo $this->Form->control('exp_date');
            echo $this->Form->control('stat_views');
            echo $this->Form->control('stat_saved');
            echo $this->Form->control('stat_loved');
            echo $this->Form->control('stat_shared');
            echo $this->Form->control('seo_keywords');
            echo $this->Form->control('seo_desc');
            echo $this->Form->control('status');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
