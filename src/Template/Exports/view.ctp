<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Export'), ['action' => 'edit', $export->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Export'), ['action' => 'delete', $export->id], ['confirm' => __('Are you sure you want to delete # {0}?', $export->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Exports'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Export'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Companies'), ['controller' => 'Companies', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Company'), ['controller' => 'Companies', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Categories'), ['controller' => 'Categories', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Category'), ['controller' => 'Categories', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="exports view large-9 medium-8 columns content">
    <h3><?= h($export->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Company') ?></th>
            <td><?= $export->has('company') ? $this->Html->link($export->company->company_name, ['controller' => 'Companies', 'action' => 'view', $export->company->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Category') ?></th>
            <td><?= $export->has('category') ? $this->Html->link($export->category->category_name, ['controller' => 'Categories', 'action' => 'view', $export->category->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Exp Name') ?></th>
            <td><?= h($export->exp_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Exp Method') ?></th>
            <td><?= h($export->exp_method) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Exp Deliver') ?></th>
            <td><?= h($export->exp_deliver) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Country') ?></th>
            <td><?= $export->has('country') ? $this->Html->link($export->country->category_name, ['controller' => 'Categories', 'action' => 'view', $export->country->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Exp Desc') ?></th>
            <td><?= h($export->exp_desc) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Exp Photos') ?></th>
            <td><?= h($export->exp_photos) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Seo Keywords') ?></th>
            <td><?= h($export->seo_keywords) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Seo Desc') ?></th>
            <td><?= h($export->seo_desc) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($export->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Subcategory Id') ?></th>
            <td><?= $this->Number->format($export->subcategory_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Exp Price') ?></th>
            <td><?= $this->Number->format($export->exp_price) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Exp Phone') ?></th>
            <td><?= $this->Number->format($export->exp_phone) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Stat Views') ?></th>
            <td><?= $this->Number->format($export->stat_views) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Stat Saved') ?></th>
            <td><?= $this->Number->format($export->stat_saved) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Stat Loved') ?></th>
            <td><?= $this->Number->format($export->stat_loved) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Stat Shared') ?></th>
            <td><?= $this->Number->format($export->stat_shared) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Status') ?></th>
            <td><?= $this->Number->format($export->status) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Exp Date') ?></th>
            <td><?= h($export->exp_date) ?></td>
        </tr>
    </table>
</div>
