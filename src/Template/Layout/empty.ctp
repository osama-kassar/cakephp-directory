<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?php echo $this->fetch('title') ?>
    </title>
    
	<style>
        :root {
          --lightblue	: #a3bac8;
          --medblue		: #234251;
          --darkblue	: #01273a;
          --gray		: #6d6d6d;
          --yellow		: #f9ca40;
          --cyan		: #aec0d2;
          --white		: #e7edf3;
          --trikwaz		: #386167;
          --black		: #363636;
        }
    </style>
    <?php echo $this->Html->meta('icon') ?>
    <?= $this->Html->css('backend') ?>
</head>
<body>
    <div id="flash"><?= $this->Flash->render() ?></div>
    <div class="container clearfix">
        <?= $this->fetch('content') ?>
    </div>
    <?php echo $this->Html->script('jquery-3.1.0.min') ?>
	<?php echo $this->Html->script('myfunc');?> 
</body>
</html>
