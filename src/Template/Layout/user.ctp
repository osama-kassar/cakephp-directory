<!DOCTYPE html>
<html>
    <head>
        <?php echo $this->Html->charset() ?>
        <!--<meta name="viewport" content="width=device-width, initial-scale=1.0">-->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
    <?php echo $this->Html->css('/plugins/animate.css-master/animate.min') ?>
        <?php echo $this->Html->css('/css/font-awesome/css/font-awesome.min') ?>
    <?php echo $this->Html->css('/plugins/bootstrap-3.3.6-dist/css/bootstrap.min') ?>
    <?php echo $this->Html->css('style.frontend'); ?>
    <?php if($this->request->lang !== 'ar'){echo $this->Html->css('style.frontend.en');}?>
        <?php echo $this->Html->script('myfunc') ?>
    </head>

<body>
    <div class="userLayout">
    	<!--<div>
            <nav class="topbar">
                <?php //echo $this->element('atopmenu');?>
            </nav>
        </div>-->
        
		<div>
			<?php echo $this->Flash->render() ?>
            <section>
                <?php echo $this->fetch('content') ?>
            </section>
        </div>
	</div>
    <br>

</body>
</html>
