<!DOCTYPE html>
<html lang="ar">
<head>
<?= $this->Html->charset() ?>
<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title>Administration</title>
	
    <!-- Style Sheets -->
    <?php 
        echo $this->Html->css('bootstrap.min');
        echo $this->Html->css('bootstrap-rtl.min');
        echo $this->Html->css('animate');
        echo $this->Html->css('font-awesome.min');
        echo $this->Html->css('bootstrap-tagsinput.min');
        echo $this->Html->css('bootstrap-timepicker.min');
        echo $this->Html->css('backend');
    ?>
	<style>
        :root {
          --lightblue	: #dae6f3;
          --medblue		: #f3f9fe;
          --darkblue	: #01273a;
          --gray		: #6d6d6d;
          --yellow		: #f9ca40;
          --cyan		: #aec0d2;
          --white		: #e7edf3;
          --trikwaz		: #386167;
          --black		: #363636;
		  --red			: #aa1e1e;
        }
    </style>
	
	
	<!-- Angular stuff -->
	<?php 
        echo $this->Html->script('jquery-3.1.0.min') ;
        echo $this->Html->script('angular.min');
        echo $this->Html->script('bootstrap-tagsinput.min');
        echo $this->Html->script('bootstrap-timepicker.min');
    ?>
	
<script>
var ptrn=[];
	ptrn['isEmail'] 		= /^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,7}$/;
	ptrn['isNumber'] 		= /^[0-9]$/;
	ptrn['isInteger'] 		= /^[\s\d]$/;
	ptrn['isFloat'] 		= /^[0-9]?\d+(\.\d+)?$/;
	ptrn['isVersion'] 		= /^(?:(\d+)\.)?(?:(\d+)\.)?(\*|\d+)$/
	ptrn['isPassword'] 		= /^[A-Za-z0-9@#$%^&*()!_-]{4,32}$/;
	ptrn['isParagraph'] 	= /^[^()]{40,255}$/;
	ptrn['isEmpty'] 		= /^[^()]{3,255}$/; ///^((?!undefined).){3,255}$/;
	ptrn['isSelectEmpty'] 	= /^[^()]{1,255}$/;
	ptrn['isZipcode'] 		= /^\+[0-9]{1,4}$/;
	ptrn['isPhone'] 		= /^[\s\d]{9,12}$/;

var errorMsg=[];
	errorMsg['isEmail'] 		= '<?=__('is-email-msg')?>';
	errorMsg['isNumber']	   = '<?=__('is-number-msg')?>';
	errorMsg['isInteger'] 	  = '<?=__('is-integer-msg')?>';
	errorMsg['isFloat'] 		= '<?=__('is-flaot-msg')?>';
	errorMsg['isVersion']	  = '<?=__('is-version-msg')?>';
	errorMsg['isPassword'] 	 = '<?=__('is-password-msg')?>';//Only Alphabet, Numbers and symboles @ # $ % ^ & * ( ) ! _ - allowed;
	errorMsg['isParagraph'] 	= '<?=__('is-paragraph-msg')?>';//Paragraph should be between 40 and 255 character;
	errorMsg['isEmpty'] 		= '<?=__('is-empty-msg')?>';
	errorMsg['isSelectEmpty']  = '<?=__('is-selected-empty-msg')?>';
	errorMsg['isPhone'] 		= '<?=__('is-phone-msg')?>';

function _setError(elm ,msg="", clr=false){
	var tar=elm.parentElement;
	if(elm.type == 'checkbox'){tar=elm.parentElement.parentElement.parentElement}
	/*if(clr){
		tar.setAttribute("style", "background: rgba(0, 255, 0, 0.1) !important;");
	}else{
		tar.setAttribute("style", "background: rgba(255, 0, 0, 0.1) !important;");;
	}*/
	if($('.error-message',tar).html() == undefined){
		$(tar).append('<div class="error-message"></div>');
	}
	$('.error-message',tar).text(msg)
	
}



var app = angular.module('app', []);
app.controller('ctrl', function($scope, $http, $location) {
    $scope.error_messages = [];
	$scope.app_folder = '<?php echo $app_folder?>';
	$scope.currlang = '<?php echo $currlang?>';
	$scope.userdt  = {};
	$scope.currUrl  = window.location.href;
	$scope.dmn = window.location.origin;
	$scope.currTab = '1';
    $scope.oldLogin = new Date();
    
	var path = '<?php echo $authUrl?>';
	var urlBase = $scope.app_folder+"/admin";
    
	$scope.getStatistics = function(target){
        $http.get(urlBase+"/users/getStatistics?ajax=1")
		.then(function(res) {
            $scope.stats = {"usersTotal":0, "usersNew":0, "requestsTotal":0, "requestsNew":0, "usersPerCountry":0, "usersPerCategory":0, "requestsPerCountry":0, "requestsPerCategory":0, "usersInactive":0, "requestsInactive":0 }
            var inc1 = 0;  
            var inc2 = 0;
            angular.forEach(res.data["users"], function(itm){
                inc1++;
                $scope.stats.usersTotal = inc1;
                if( new Date( toEnNumbers( itm.created ) ) > new Date( $scope.oldLogin ) ){ $scope.stats.usersNew += 1 }
                if( itm.status == 0 ){ $scope.stats.usersInactive += 1 }
                $scope.stats.usersPerCountry[itm.country_name] = inc1;
                $scope.stats.usersPerCategory[itm.role_name] = inc1;
            })
            
            
            angular.forEach(res.data["requests"], function(itm){
                inc2++;
                $scope.stats.requestsTotal = inc2;
                if( new Date( toEnNumbers( itm.stat_created ) ).getTime() > new Date( $scope.oldLogin ).getTime() ){ $scope.stats.requestsNew += 1 }
                if( itm.status == 0 ){ $scope.stats.requestsInactive += 1 }
                $scope.stats.requestsPerCountry[itm.country] = inc2 ;
                $scope.stats.requestsPerCategory[itm.category] = inc2;
                console.log( itm , itm.stat_created );
            })
        })
    }
    
    var toEnNumbers = function(str) {
        var ar = "٠١٢٣٤٥٦٧٨٩";
        var en = "0123456789";
        var res = ""
        for(var i in str){
            var ind = ar.indexOf( str.charAt(i) );
            if( ind > -1 ){
                res += en[ind]
            }else{
                res += str.charAt(i)
            }
        }
        return res;
    }
})
</script>
</head>



<body  ng-app="app" ng-controller="ctrl as ctrl" >
<div id="imgHolder" onClick="this.setAttribute('style', 'opacity:0;z-index:-20;')"></div>
<div class="container-fluid mycontainer">
	<div class="row">
		<div class="col-md-3 col-xs-12"> <?php echo $this->element('amenu');?> </div>
		<div class="col-md-9"> 
			<div><?php echo $this->element('hmenu');?></div>
			<?php echo $this->Flash->render() ?>
			<section> <?php echo $this->fetch('content') ?> </section>
		</div>
  	</div>
</div>


<!-- Scripts --> 
<?php echo $this->Html->script('myfunc');?> 

</body>
</html>
