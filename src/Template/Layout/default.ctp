<!DOCTYPE html>
<html lang="ar">
<head>

    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $this->fetch('title') ?>
    </title>
	
    <?php
		$metaKeywords = !empty($keywords) ? $keywords : __('main_keywords');
		$metaDescription = !empty($description) ? $description : __('main_description');
	?>
    <?php echo $this->Html->meta('keywords', $metaKeywords);  ?>
    <?php echo $this->Html->meta('description', $metaDescription);  ?>
	
    <?= $this->Html->meta('icon') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>

    <!-- Style Sheets -->
	<style>
        :root {
          --lightblue	: #7a9aaf;
          --medblue		: #273c4d;
          --darkblue	: #0d1a2a;
          --gray		: #d2d3ce;
          --yellow		: #ff8800;
          --cyan		: #aec0d2;
          --white		: #e7edf3;
          --trikwaz		: #386167;
          --black		: #363636;
          --green		: #7aaf9a;
        }
    </style>


<?php 
    //CSS
	if($this->request->params['action'] = 'view'){
		echo $this->Html->css('animate.min');
		echo $this->Html->css('superlist');
		echo $this->Html->css('owl.carousel.min');
		echo $this->Html->css('colorbox/example1/colorbox');
	}
	echo $this->Html->css('bootstrap.min');
	echo $this->Html->css('stylesheet');
	echo $this->Html->css('style_ar');
	if($currlang !== 'ar'){echo $this->Html->css('style_en');}
    echo $this->Html->css('font-awesome.min');
    echo $this->Html->css('bootstrap-tagsinput.min');
    echo $this->Html->css('bootstrap-timepicker.min');
    

    // JS
    echo $this->Html->script('jquery-3.1.0.min');
    echo $this->Html->script('angular.min');
    echo $this->Html->script('angular-animate.min');
    echo $this->Html->script('bootstrap-tagsinput.min');
    echo $this->Html->script('bootstrap-timepicker.min');
?>
<!-- ANGULARJS APP -->
<script>
var ptrn=[];
	ptrn['isEmail'] 		= /^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,7}$/;
	ptrn['isNumber'] 		= /^[0-9]$/;
	ptrn['isInteger'] 		= /^[\s\d]$/;
	ptrn['isFloat'] 		= /^[0-9]?\d+(\.\d+)?$/;
	ptrn['isVersion'] 		= /^(?:(\d+)\.)?(?:(\d+)\.)?(\*|\d+)$/
	ptrn['isPassword'] 		= /^[A-Za-z0-9@#$%^&*()!_-]{4,32}$/;
	ptrn['isParagraph'] 	= /^[^()]{40,255}$/;
	ptrn['isEmpty'] 		= /^[^()]{3,255}$/; ///^((?!undefined).){3,255}$/;
	ptrn['isSelectEmpty'] 	= /^[^()]{1,255}$/;
	ptrn['isZipcode'] 		= /^\+[0-9]{1,4}$/;
	ptrn['isPhone'] 		= /^[\s\d]{9,15}$/;

var errorMsg=[];
	errorMsg['isEmail'] 		= '<?=__('is-email-msg')?>';
	errorMsg['isNumber']	   = '<?=__('is-number-msg')?>';
	errorMsg['isInteger'] 	  = '<?=__('is-integer-msg')?>';
	errorMsg['isFloat'] 		= '<?=__('is-flaot-msg')?>';
	errorMsg['isVersion']	  = '<?=__('is-version-msg')?>';
	errorMsg['isPassword'] 	 = '<?=__('is-password-msg')?>';//Only Alphabet, Numbers and symboles @ # $ % ^ & * ( ) ! _ - allowed;
	errorMsg['isParagraph'] 	= '<?=__('is-paragraph-msg')?>';//Paragraph should be between 40 and 255 character;
	errorMsg['isEmpty'] 		= '<?=__('is-empty-msg')?>';
	errorMsg['isSelectEmpty']  = '<?=__('is-selected-empty-msg')?>';
	errorMsg['isPhone'] 		= '<?=__('is-phone-msg')?>';

    
    
function _setError(elm ,msg="", clr=false){
	var tar=$(elm).parent();
    if(elm.type=="checkbox"){tar=$(elm).parent().parent().parent()}
	//$(tar).remove('div.error-message');
	if($('.error-message',tar).html() == undefined){
		$(tar).append('<div class="error-message"></div>');
	}
    $('.error-message',tar).text(msg)	
}
    
    
var turkeyde = angular.module('turkeyde', []);
turkeyde.controller('ctrl', function($scope, $http, $location) {
	$scope.error_messages = [];
	$scope.app_folder = '<?php echo $app_folder?>';
	$scope.currlang = '<?php echo $currlang?>';
	$scope.userdt  = {};
	$scope.currUrl  = window.location.href;
	$scope.dmn = window.location.origin;
	$scope.currTab = '1';
	$scope.fsearch = {"page":"companies", "country":"", "keyword":""};
	$scope.pages_name = {"companies":"<?=$pages_name[0]?>", "requests":"<?=$pages_name[1]?>", "articles":"articles"};
	$scope.fsearch={"page" : $scope.pages_name.companies};
	$scope.fields = {
		"1" :["slug","company_name", "company_logo"], 
		"2" :["slug","req_title", "req_photos"], 
		"3" :["slug", "article_title", "article_photo"]
	};
	var path = '<?php echo $authUrl?>';
	var urlBase = $scope.app_folder+"/"+$scope.currlang;
	
	$scope.loadSearch = function() {
		$scope.searchResult=[];
		if($scope.fsearch.keyword.length>1){
			var url = urlBase+"/"+$scope.fsearch.page
			$http.get(url+"?keyword="+$scope.fsearch.keyword+"&ajax=1", { cache: false}).then(function(response) {
				//for(k in response.data){
					$scope.searchResult = response.data;
				//}
			});
		}
	};
	
	var _setCvr = function(tar ,param){
		var elm = document.getElementById( tar );
		if(param==1){
			elm.style.opacity=1;
			elm.style.zIndex=200;
		}else{
			elm.style.opacity=0;
			elm.style.zIndex=-1;
		}
	}
	var _getErrors = function (obj, form_name){
		const result = '';
		for(const prop in obj) {
			const value = obj[prop];
			var arr = $.map(value, function(val, index) {
				return [val];
			});
            var tar_name = prop.replace("_", "-")
            _setError($( '#'+form_name+' #'+tar_name )[0] ,arr[0])
		}
	}
	
	$scope.showMore = function(elm){
		$('#'+elm).toggleClass("do_show_more");
	}
	$scope.doLogin = function(){
		_setCvr('login_cvr', 1)
		$http({
			method  : "POST",
			url	 : urlBase+"/login?ajax=1",
			data	: $scope.userdt 
		})
		.then(function(res) {
            console.log(res.data)
			_setCvr('login_cvr', 0);
			if(res.data.status == '1'){
				$('#flash').html("<div class='message success' onclick=$(this).addClass('fadeOut');><span class='fadeIn'><?=__('login-success')?></span></div>");
				$('#login').modal('hide');
                if(res.data.role.indexOf('user.') > -1){
                   window.location.href = window.location.href.replace('login=1', '');
                }else{
                   window.location.href = $scope.app_folder+"/admin";
                }
			}else if(res.data.status == '-2'){
//				$('#flash').html("<div class='message error' onclick=$(this).addClass('fadeOut');><span class='fadeIn'><?=__('sms-not-activated')?></span></div>");
//				$('#login').modal('hide');
//				$('#getsms').modal('show');
			}else if(res.data.status == '0'){
				$('#flash').html("<div class='message error' onclick=$(this).addClass('fadeOut');><span class='fadeIn'><?=__('email-not-activated')?></span></div>");
				$('#login').modal('hide');
				$('#getemail').modal('show');
			}else{
				$('#flash').html("<div class='message error' onclick=$(this).addClass('fadeOut');><span class='fadeIn'><?=__('login-fail')?></span></div>");
			}
		}, function(res){
            $('#flash').html("<div class='message error' onclick=$(this).addClass('fadeOut');><span class='fadeIn'><?=__('send-fail')?></span></div>");
			_setCvr('login_cvr', 0)
        });
	}
	
	$scope.doResend = function(activeTar='sms'){
		_setCvr('getsms_cvr',1);
		var msg = "132465";
		$http({
			method  : "POST",
			url	 : "https://bulksms.vsms.net/eapi/submission/send_sms/2/2.0?username=Turkeyde&password=Mary123Mary123&message=" + msg + "&msisdn=905346346466,905392467519"
		}).than(function(data) {
			_setCvr('getsms_cvr', 0)
		}, function(res){
            $('#flash').html("<div class='message error' onclick=$(this).addClass('fadeOut');><span class='fadeIn'><?=__('send-fail')?></span></div>");
			_setCvr('getsms_cvr', 0)
        })
	}
	
	$scope.doActivate = function(activeTar='sms'){
		_setCvr('activate_cvr',1);
		$http({
			method  : "POST",
			url	 : urlBase+"/activate?ajax=1&activeTar="+activeTar,
			data	: $scope.userdt 
		})
		.then(function(res) {
			_setCvr('activate_cvr', 0)
			if(res.data !== '1'){
			}
		}, function(res){
            $('#flash').html("<div class='message error' onclick=$(this).addClass('fadeOut');><span class='fadeIn'><?=__('send-fail')?></span></div>");
			_setCvr('activate_cvr', 0)
        })
	}
	
	$scope.registerdt={iagree: 'no'}
	$scope.doRegister = function(){
		$scope.registerdt.city_id = 391
		_setCvr('register_cvr',1);
		$http({
			method  : "POST",
			url	 : urlBase+"/register?ajax=1",
			data	: $scope.registerdt 
		})
		.then(function(res) {
			_setCvr('register_cvr', 0)
			if(res.data !== '1'){
				_getErrors(res.data, 'reg_form')
				$('#flash').html("<div class='message error' onclick=$(this).addClass('fadeOut');><span class='fadeIn'><?=__('register-fail')?></span></div>");
			}else{
				$('#flash').html("<div class='message success' onclick=$(this).addClass('fadeOut');><span class='fadeIn'><?=__('register-success')?></span></div>");
				//$('#login', window.parent.document).modal('hide');
				//$('#login', top.document).modal('hide');
				$('#register').modal('hide');
				//$('#login').modal('show');
			}
		}, function(res){
            $('#flash').html("<div class='message error' onclick=$(this).addClass('fadeOut');><span class='fadeIn'><?=__('send-fail')?></span></div>");
			_setCvr('register_cvr', 0)
        });
	}
	
	$scope.msg={};
	$scope.doSend = function(tar){
		_setCvr('send_cvr',1);
		$http({
			method  : "POST",
			url	 : urlBase+"/contacts/sendmessage?ajax=1&tarSlug="+tar,
			data	: $scope.msg 
		})
		.then(function(res) {
			_setCvr('send_cvr',0);
			if(res.data !== '1'){
				_getErrors(res.data, 'send_form')
				$('#flash').html("<div class='message error' onclick=$(this).addClass('fadeOut');><span class='fadeIn'><?=__('send-fail')?></span></div>");
			}else{
				$scope.msg = {}
				$('#flash').html("<div class='message success' onclick=$(this).addClass('fadeOut');><span class='fadeIn'><?=__('send-success')?></span></div>");
				$('#sendmessage').modal('hide');
			}
		}, function(res){
            $('#flash').html("<div class='message error' onclick=$(this).addClass('fadeOut');><span class='fadeIn'><?=__('send-fail')?></span></div>");
			_setCvr('send_cvr', 0)
        })
	}
	
	$scope.doGetPassword = function(){
		_setCvr('getpassword_cvr',1);
		$http({
			method  : "POST",
			url	 : urlBase+"/getpassword?ajax=1",
			data	: $scope.userdt 
		})
		.then(function(res) {
			_setCvr('getpassword_cvr',0);
			if(res.data !== '1'){
				_getErrors(res.data, 'getpassword_form')
				$('#flash').html("<div class='message error' onclick=$(this).addClass('fadeOut');><span class='fadeIn'><?=__('send-fail')?></span></div>");
			}else{
				$scope.msg = {}
				$('#flash').html("<div class='message success' onclick=$(this).addClass('fadeOut');><span class='fadeIn'><?=__('send-success')?></span></div>");
				$('#getpassword').modal('hide');
			}
		}, function(res){
            $('#flash').html("<div class='message error' onclick=$(this).addClass('fadeOut');><span class='fadeIn'><?=__('send-fail')?></span></div>");
			_setCvr('getpassword_cvr', 0)
        })
	}
	
	$scope.getInfo = function(){
		$http.get($scope.app_folder+"/articles?ajax=1")
		.then(function(dt){
			alert(dt)
		})
	}
	
	var _setShare = function(tar){
		var pageUrl = document.URL;
		var title = document.title;
		var p = document.getElementsByTagName("article")[0].innerHTML;
		var url = '';
		if(tar == 'facebook'){
			url = decodeURI('https://www.facebook.com/sharer/sharer.php?u='+pageUrl);
		}
		if(tar == 'twitter'){
			url = decodeURI('https://twitter.com/home?status='+pageUrl);
		}
		if(tar == 'google-plus'){
			url = decodeURI('https://plus.google.com/share?url='+pageUrl);
		}
		if(tar == 'linkedin'){
			url = decodeURI('https://www.linkedin.com/shareArticle?mini=true&url='+pageUrl+'&title='+title+'&summary='+p+'&source='+pageUrl);
		}
		if(tar == 'pinterest'){
			url = decodeURI('https://pinterest.com/pin/create/button/?url='+pageUrl+'&media='+p+'&description='+p);
		}
		if(tar == 'email'){
			url = 'mailto:email';
		}
		return url;
	}
	
    $scope.openWin = function(tar){
		var urlTar = _setShare(tar);
		var conf = "directories=no, menubar=no, status=no, location=no, titlebar=no, toolbar=no, scrollbars=no, width=700, height=400, resizeable=no, top=150, left=250";
		var popup=window.open(urlTar, '_blank', conf); //'http://localhost/turkeyde/pages/sharer'
		//var dom = popup.document.body;
    }
	
	$scope.callAction = function(url){
		$http.get($scope.app_folder+url+'&ajax=1').then(function(res){
			console.log(res.data)
		})
	}
    
	$scope.goTo = function(path, ext=null){
		if(ext){return window.open($scope.dmn+$scope.app_folder+path)}
		return window.location.href = $scope.dmn+$scope.app_folder+path
	}
	$scope.showMenu = function (){
		if($('.mobMenu').css('right')=='0px'){
			$('.mobMenu').css('right', '-800px');
			$('.menu_btn').html('<i class="fa fa-bars"></i>');
		}else{
			$('.mobMenu').css('right', '0px');
			$('.menu_btn').html('<i class="fa fa-times"></i>');
		}
	}
});
turkeyde.directive('setFixed', ['$window', function ($window) {
	return {
		restrict: 'A',
		link: function(scope, element, attrs) {
			if(window.innerWidth < 768){
				//element[0].classList.add('smallHeader');
				return ;	
			}
			angular.element(document).bind('scroll', function() {
                if ($window.pageYOffset > 1) {
                    element[0].classList.add('smallHeader')
                } else {
                    element[0].classList.remove('smallHeader')
                }
			});
		}
	};
}]);
    
turkeyde.directive("showPassword", ['$timeout', function ($timeout) {
    return {
        link: function (scope, elm, attrs, ngModel) {
            elm.bind('click', function(e) {
                var field = $(elm).parent().find("input")
                if(field.attr("type") == "password"){
                    $(elm).parent().find("input").attr("type", "text");
                    $(elm).removeClass("fa-eye-slash").addClass("fa-eye");
                }else{
                    $(elm).parent().find("input").attr("type", "password");
                    $(elm).removeClass("fa-eye").addClass("fa-eye-slash");
                }
            })
        }
    }
}]);
turkeyde.directive('chk', function () {
    return {
        scope: {chk: '@'},
        link: function (scope, element, attrs, ctrl) {
			element.bind('blur', onBlur);
			function onBlur(ctrl){
				if(attrs.type == 'checkbox' || attrs.type == 'radio'){
					var newid = attrs.id.substr(0, attrs.id.length-1);
					var elms = document.querySelectorAll('[id^='+newid+']');
					for(var i in elms){
						if(elms[i].checked == true){
							return _setError(element[0] , '', true);
						}else{
							_setError(element[0] ,errorMsg[scope.chk]);
						}
					}
				}else{
					if ( ptrn[scope.chk].test(element[0].value) ) {
						_setError(element[0] , '', true);
					} else {
						_setError(element[0] ,errorMsg[scope.chk]);
					}
				}
				scope.$apply();
			}
        }
    };
});
turkeyde.filter('urlcrypt', function() {
    return window.decodeURIComponent;
});

</script>
</head>
<body  ng-app='turkeyde' ng-controller="ctrl as ctrl" >
<div id="imgHolder" onClick="this.setAttribute('style', 'opacity:0;z-index:-20;')"></div>

<!--<div id="vfx_loader_block">
  <div class="vfx-loader-item"> <?=$this->Html->image("/images/loading.gif")?> </div>
</div>-->

<?php echo $this->element('header')?>

<!-- login form -->
<?php echo $this->element('login')?>

<!-- registration form -->
<?php echo $this->element('register')?>

<!-- get PASSWORD form -->
<?php echo $this->element('getpassword')?>

<!-- get SMS form -->
<?php //echo $this->element('getsms')?>

<!-- get EMAIL form -->
<?php //echo $this->element('getemail')?>

<!-- content -->
<?php echo $this->fetch('content') ?>

<!-- overlay flash message -->
<div id="flash"> 
	<?php echo $this->Flash->render() ?> 
</div>
    
<!-- footer -->
<?php echo $this->element('footer')?>


<!-- Scripts --> 
<?php 
	echo $this->Html->script('myfunc');
	//echo $this->Html->script('jquery_counterup');
	echo $this->Html->script('jquery-1.11.1.min');
	echo $this->Html->script('bootstrap.min');
	echo $this->Html->script('jquery_custom');
	if($this->request->action == 'view'){
	    echo $this->Html->script('superlist');
		//echo $this->element('sendmessage');
		echo $this->Html->script('colorbox/jquery.colorbox-min');
		echo $this->Html->script('bootstrap-select/bootstrap-select.min');
		echo $this->Html->script('owl.carousel.min');
	}
?>
<?php //echo $this->Html->script('//maps.googleapis.com/maps/api/js?sensor=false');?>
<?php
	if(@$_GET['login']==1){
		echo "<script>setTimeout(function(){ $('#login').modal('show') }, 1000);</script>";
	}
?>
</body>
</html>
