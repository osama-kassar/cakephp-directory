<div id="vfx-product-inner-item">
  <div class="container">
    <div class="row">
      <div class="col-md-3 col-sm-4 col-xs-12">
        <div class="news-search-lt">
          <input class="form-control" placeholder="Search" type="text">
          <span class="input-search"> <i class="fa fa-search"></i> </span> </div>
        <div class="left-slide-slt-block">
          <h3>Categories</h3>
        </div>
        <div class="list-group"> <a href="#" class="list-group-item active"><i class="fa fa-hand-o-right"></i> Business <span class="list-lt">15</span></a> <a href="#" class="list-group-item"><i class="fa fa-hand-o-right"></i> Health & Fitness <span class="list-lt">09</span></a> <a href="#" class="list-group-item"><i class="fa fa-hand-o-right"></i> Real Estate <span class="list-lt">18</span></a> <a href="#" class="list-group-item"><i class="fa fa-hand-o-right"></i> Entertainment <span class="list-lt">24</span></a> <a href="#" class="list-group-item"><i class="fa fa-hand-o-right"></i> Beauty & Spas <span class="list-lt">06</span></a> <a href="#" class="list-group-item"><i class="fa fa-hand-o-right"></i> Automotive <span class="list-lt">04</span></a> <a href="#" class="list-group-item"><i class="fa fa-hand-o-right"></i> Hotels & Travel <span class="list-lt">14</span></a> <a href="#" class="list-group-item"><i class="fa fa-hand-o-right"></i> Sports & Adventure <span class="list-lt">07</span></a> <a href="#" class="list-group-item"><i class="fa fa-hand-o-right"></i> Technology <span class="list-lt">12</span></a> <a href="#" class="list-group-item"><i class="fa fa-hand-o-right"></i> Arts & Entertainment <span class="list-lt">26</span></a> <a href="#" class="list-group-item"><i class="fa fa-hand-o-right"></i> Education & Learning <span class="list-lt">24</span></a> <a href="#" class="list-group-item"><i class="fa fa-hand-o-right"></i> Cloth Shop <span class="list-lt">16</span></a> </div>
        <div class="left-slide-slt-block">
          <h3>Popular Tags</h3>
        </div>
        <div class="archive-tag">
          <ul>
            <li><a href="#" class="active">Amazing</a></li>
            <li><a href="#">Envato</a></li>
            <li><a href="#">Themes</a></li>
            <li><a href="#">Clean</a></li>
            <li><a href="#">Responsivenes</a></li>
            <li><a href="#">SEO</a></li>
            <li><a href="#">Mobile</a></li>
            <li><a href="#">IOS</a></li>
            <li><a href="#">Flat</a></li>
            <li><a href="#">Design</a></li>
          </ul>
        </div>
        <div class="left-slide-slt-block">
          <h3>Location List</h3>
        </div>
        <div class="left-location-item">
          <ul>
            <li><a href="#"><i class="fa fa-angle-double-right"></i> Manchester</a><span class="list-lt">07</span></li>
            <li><a href="#"><i class="fa fa-angle-double-right"></i> Lankashire</a><span class="list-lt">04</span></li>
            <li><a href="#"><i class="fa fa-angle-double-right"></i> New Mexico</a><span class="list-lt">03</span></li>
            <li><a href="#"><i class="fa fa-angle-double-right"></i> Nevada</a><span class="list-lt">06</span></li>
            <li><a href="#"><i class="fa fa-angle-double-right"></i> Kansas</a><span class="list-lt">08</span></li>
            <li><a href="#"><i class="fa fa-angle-double-right"></i> West Virginina</a><span class="list-lt">05</span></li>
          </ul>
        </div>
        <div class="left-slide-slt-block">
          <h3>Archives</h3>
        </div>
        <div class="left-archive-categor">
          <ul>
            <li><a href="#"><i class="fa fa-angle-double-right"></i> January 2016</a><span class="list-lt">09</span></li>
            <li><a href="#"><i class="fa fa-angle-double-right"></i> February 2016</a><span class="list-lt">52</span></li>
            <li><a href="#"><i class="fa fa-angle-double-right"></i> March 2016</a><span class="list-lt">36</span></li>
            <li><a href="#"><i class="fa fa-angle-double-right"></i> April 2016</a><span class="list-lt">78</span></li>
            <li><a href="#"><i class="fa fa-angle-double-right"></i> May 2016</a><span class="list-lt">66</span></li>
            <li><a href="#"><i class="fa fa-angle-double-right"></i> June 2016</a><span class="list-lt">15</span></li>
          </ul>
        </div>
      </div>
      <div class="col-md-9 col-sm-8 col-xs-12 nopadding">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="sorts-by-results">
            <div class="col-md-6 col-sm-6 col-xs-6"> <span class="result-item-view">Your Search Returned <span class="yellow"><?=count($services)?></span> Results</span> </div>
            <!--<div class="col-md-6 col-sm-6 col-xs-6">
              <div class="disp-f-right">
                <div class="disp-style"><a href="listing_grid.html"><i class="fa fa-th"></i></a></div>
                <div class="disp-style active"><a href="listing_list.html"><i class="fa fa-th-list"></i></a></div>
              </div>
            </div>-->
          </div>
          <?php foreach ($services as $service){ ?>
          <div class="recent-listing-box-container-item list-view-item" style="direction:rtl;">
            <div class="col-md-4 nopadding feature-item-listing-item listing-item">
              <div class="recent-listing-box-image">
                <h1><?=$service->category->category_name?></h1>
                <img src="images/product/img1.png" alt="img1"> </div>
              <div class="hover-overlay">
                <div class="hover-overlay-inner">
                  <ul class="listing-links">
                    <li><a href="#"><i class="fa fa-heart green-1"></i></a></li>
                    <li><a href="#"><i class="fa fa-map-marker blue-1"></i></a></li>
                    <li><a href="#"><i class="fa fa-share yallow-1"></i></a></li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="col-md-8 nopadding">
              <div class="recent-listing-box-item">
                <div class="listing-boxes-text">
                  <h3><?=$service->service_name?></h3>
                  <p><?=$service->service_desc?></p>
                </div>
                <div class="recent-feature-item-rating">
                  <h2><i class="fa fa-map-marker"></i><?=$service->country_id?></h2>
                  <span> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> </span> </div>
              </div>
            </div>
          </div>
            <?php }?>
            
        </div>
      </div>
    </div>
  </div>
</div>
