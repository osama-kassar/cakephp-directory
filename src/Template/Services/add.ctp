<style>
.from-list-lt .from-input-ic {
	top: 28px;
}
</style>
<div class="container">
	<div id="breadcrum-inner-block">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 text-center">
					<div class="breadcrum-inner-header">
						<h1> <?php echo __('add_service')?> </h1>
					</div>
				</div>
			</div>
		</div>
	</div>
	<p> <?php echo __('add_service_msg')?> </p>
	<div class="from-list-lt">
		<div class="col-sm-8 col-sm-offset-2"> <?php echo  $this->Form->create($service, [ 'class'=>'form-float form-alt' , 'novalidate' => true]) ?>
			<div class="row">
				<div class="form-group col-md-6 col-xs-12"> <span class="from-input-ic"><i class="fa fa-user"></i></span> <?php echo $this->Form->input('service_name', ['type'=>'text', 'label'=>__('service_name'), 'placeholder'=>__('service_name'), 'class'=>'form-control'])?> </div>
				<div id="price">
					<div class="form-group col-md-2 col-xs-2"> <span class="from-input-ic"><i class="fa fa-money"></i></span> <?php echo $this->Form->input('service_currency', 
					['type'=>'select', 'label'=>__('service_currency'), 'class'=>'form-control', 'options'=>$service_currency])?> </div>
					<div  class="form-group col-md-4 col-xs-4"> <span class="from-input-ic"><i class="fa fa-usd"></i></span> <?php echo $this->Form->input('service_price', ['type'=>'text', 'label'=>__('service_price'), 'placeholder'=>__('service_price'), 'class'=>'form-control'])?> </div>
				</div>
				<div class="form-group col-md-6 hideElm"> </div>
				<div class="form-group col-md-6 "> <?php echo $this->Form->input('service_price_0', ['type'=>'checkbox', 'label'=>['text' => '<span>'.__('service_price_0').'</span>',  'class' => 'mycheckbox gray', 'escape'=>false], "onchange"=>"disElm($(this).is(':checked'), 'price')"])?> </div>
				<div class="form-group col-md-6 col-xs-12"> <span class="from-input-ic"><i class="fa fa-bars"></i></span> <?php echo $this->Form->input('category_id', 
				['empty' => __('f_category'),'type'=>'select', 
				'label'=>__('category_id'), 'class'=>'form-control'])?> </div>
				<div class="form-group col-md-6 col-xs-12"> <span class="from-input-ic"><i class="fa fa-globe"></i></span> <?php echo $this->Form->input('country_id', 
				['type'=>'select', 'label'=>__('country_id'),
				'empty' => __('f_country'), 'class'=>'form-control'])?> </div>
				<div class="form-group col-md-6 col-xs-12"> <span class="from-input-ic"><i class="fa fa-phone"></i></span> <?php echo $this->Form->input('service_phone', ['type'=>'text', 'label'=>__('service_phone'), 'placeholder'=>__('service_phone'), 'class'=>'form-control'])?> </div>
				<div class="form-group col-md-12"> <span class="from-input-ic"><i class="fa fa-envelope-o"></i></span> <?php echo $this->Form->input('service_desc', ['type'=>'textarea', 'label'=>__('service_desc'), 'placeholder'=>__('service_desc'), 'class'=>'form-control'])?> </div>
				<?php /*?>
				<div class=" form-group col-md-6">
					<div>
						<label>
							<?=__('service_method');?>
						</label>
					</div>
					<div class="mychckbox_container">
						<?php  
					foreach($service_method as $k=>$val){
						 echo $this->Form->input('service_method', [
							'type'=>'checkbox', 
							'label'=>['text' => '<span>'.__($val).'</span>',  
							'class' => 'mycheckbox gray', 'escape'=>false], 
							'name'=>'service_method[]', 'id'=>'service_method_'.$k, 'value'=>$k,
							'hiddenField' => false]);
					}
				?>
					</div>
				</div>
				<div class=" form-group col-md-6">
					<div>
						<label>
							<?=__('service_deliver');?>
						</label>
					</div>
					<div class="mychckbox_container">
						<?php  
					foreach($service_deliver as $k=>$val){
						 echo $this->Form->input('service_deliver', [
							'type'=>'checkbox', 
							'label'=>['text' => '<span>'.__($val).'</span>',  
							'class' => 'mycheckbox gray', 'escape'=>false], 
							'name'=>'service_deliver[]', 'id'=>'service_deliver_'.$k, 'value'=>$k,
							'hiddenField' => false]);
					}
				?>
					</div>
				</div>
		<div class="form-group col-md-6 col-xs-12"> <span class="from-input-ic"><i class="fa fa-tags"></i></span>
            <?php echo $this->Form->input('city_id', 
				['type'=>'select', 'label'=>__('city_id'), 'empty' => __('f_city'), 'class'=>'form-control'])?>
          </div>
		
		<div class="form-group col-md-6 col-xs-12"> <span class="from-input-ic"><i class="fa fa-tags"></i></span>
            <?php echo $this->Form->input('service_links', ['type'=>'textarea', 'label'=>__('service_links'), 'placeholder'=>__('service_links'), 'class'=>'form-control', 'readonly', 'data-toggle'=>"modal", 'data-target'=>"#addlink"])?>
          </div>
		<?php */?>
				<div class="clearfix form-group col-xs-12 ">
					<div>
						<label>
							<?=__('service_photos');?>
						</label>
					</div>
					<?php echo $this->element('uploadForm', ['name'=>'photo_', 'photosNumber'=>12]);?> </div>
				<div class="form-group col-md-6 col-xs-12"> <span class="from-input-ic"><i class="fa fa-location-arrow"></i></span> <?php echo $this->Form->input('service_address', ['type'=>'textarea', 'label'=>__('service_address'), 'placeholder'=>__('service_address_desc'), 'class'=>'form-control'])?> </div>
				<div class="form-group col-md-6 col-xs-12"> <span class="from-input-ic"><i class="fa fa-paragraph"></i></span> <?php echo $this->Form->input('seo_desc', ['type'=>'textarea', 'label'=>__('seo_desc'), 'placeholder'=>__('seo_desc_desc'), 'class'=>'form-control'])?> </div>
				<div class="form-group col-md-12"> <span class="from-input-ic"><i class="fa fa-key"></i></span> <?php echo $this->Form->input('seo_keywords', ['type'=>'text', 'label'=>__('seo_keywords'), 'placeholder'=>__('seo_keywords'), 'class'=>'form-control'])?> </div>
				<div class="form-group col-md-12">
					<button class="btn pull-right" type="submit"><?php echo __('Submit')?></button>
				</div>
			</div>
			<br />
			</form>
		</div>
	</div>
</div>
<!-- registration form --> 
<?php echo $this->element('addlink')?> 
<script>
var disElm = function(v, tar){
	return $('#'+tar).find("*").prop('disabled', v);
}


</script>