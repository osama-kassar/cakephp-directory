<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
	<div class="listing-modal-1 modal-dialog"  style="width:100%;max-width:600px;">
		<div class="modal-content" style=" position:relative; overflow:hidden;">
			<div class="cvr" id="login_cvr"><i class="fa fa-refresh fa-spin fa-3x fa-fw"></i></div>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="close_btn">&times;</button>
				<!--<h4 class="modal-title" id="myModalLabel">
					<?=__('login')?> / <?=__('create-new-account')?>
				</h4>-->
			<div class="modal_half"></div>
			</div>
			<div class="modal-body row">
				
				
				
				<div class="listing-login-form col-sm-6 col-xs-12">
					<h4><?=__('login')?></h4>
					<form method="post" novalidate="novalidate" ng-submit="doLogin();">
						<div class="listing-form-field"> <i class="fa fa-user blue-1"></i>
							<input class="form-field bgwhite" type="text" 
                	placeholder="<?=__('username')?>" id="username" name="username"
                    ng-model="userdt.username"/>
						</div>
						<div class="listing-form-field"> <i class="fa fa-lock blue-1"></i>
							<input class="form-field bgwhite" type="password" 
                	placeholder="<?=__('password')?>" id="password" name="password" 
                    ng-model="userdt.password"/>
						</div>
						<div>
							<?php
								echo $this->Form->input('remember_me', [
									'type'=>'checkbox', 
									'label'=>['text' => '<span>'.__('remember-me').'</span>',  
									'class' => 'mycheckbox gray', 'escape'=>false], 
									'name'=>'remember_me', 'id'=>'remember_me',
									'hiddenField' => false]);
							?>
						</div>
						<div class="listing-form-field">
							<input class="form-field submit" type="submit" value="<?=__('login')?>" />
						</div>
					</form>
					<div class="bottom-links"> 
						<!--<span>
							<?=__('no-account?')?>
							<a href="javascript:void()" 
								data-dismiss="modal"  
								data-toggle="modal" 
								data-target="#register">
								<?=__('create-new-account')?>
							</a>
						</span><br />-->
						<span> 
							<a href="javascript:void()" 
								data-dismiss="modal"  
								data-toggle="modal" 
								data-target="#getpassword">
								<?=__('forget-password')?>
							</a> 
						</span> 
					</div>
				</div>
				
				
				
				<div class="listing-login-form col-sm-6 col-xs-12">
					<h4 class="hideMob"><?=__('registration')?></h4>
					<p class="hideMob"><?=__('before_register_msg')?></p>
					<hr class="hideWeb"/>
					<div class="listing-form-field">
						<input data-dismiss="modal"  
								data-toggle="modal" 
								data-target="#register"
								id="btn_register"
							class="form-field submit" type="submit" value="<?=__('create-new-account')?>" />
					</div>
				</div>
				
				
				
				
				
			</div>
		</div>
	</div>
</div>
