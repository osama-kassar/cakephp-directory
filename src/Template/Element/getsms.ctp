
<div class="modal fade" id="getsms" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
	<div class="listing-modal-1 modal-dialog" >
		<div class="modal-content">
			<div class="cvr" id="getsms_cvr"><i class="fa fa-refresh fa-spin fa-3x fa-fw"></i></div>
			<div class="modal-header">
				<button type="button" class="close" 
					data-dismiss="modal" aria-hidden="true" 
					id="close_btn">&times;</button>
				<h4 class="modal-title" id="myModalLabel">
					<?=__('activate_sms')?>
				</h4>
			</div>
			<div class="modal-body">
				<div class="listing-login-form">
					<form method="post" novalidate="novalidate" ng-submit="doActivate('sms');">
						<div class="listing-form-field">
							<i class="fa fa-lock blue-1"></i>
							<input class="form-field bgwhite" type="text" 
                				placeholder="<?=__('sms_activate_code')?>" id="sms_activate" name="sms_activate"
                    			ng-model="userdt.sms_activate"/>
						</div>
						
						<div class="listing-form-field">
							<input class="form-field submit" type="submit" value="<?=__('submit')?>" />
						</div>
					</form>
					<div class="bottom-links">
							<a href="javascript:void()" ng-click="doResend();">
								<div><i class="fa fa-share-square"></i> 
								<?=__('send-me-code-again')?>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
