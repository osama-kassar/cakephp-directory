	<div class="row col-xs-12 borderBulk">
		<h3 class="col-sm-12"><?=__('contact_info')?></h3>
		<div class="col-sm-4 hideMob"><?=__('username')?></div>
		<div class="col-sm-8"><?=$uinfo['firstname'].' '.$uinfo['lastname']?></div>
		<div class="col-sm-4 hideMob"><?=__('email')?></div>
		<div class="col-sm-8"><?=$uinfo['username']?>&nbsp;</div>
		<div class="col-sm-4 hideMob"><?=__('phone')?></div>
		<div class="col-sm-8"><?=$uinfo['phone']?>&nbsp;</div>
		<div class="col-sm-4 hideMob"><?=__('country')?></div>
		<div class="col-sm-8"><?=$this->Do->lcl($uinfo['country']['category_name'])?>&nbsp;</div>
		<div class="col-sm-4 hideMob"><?=__('city')?></div>
		<div class="col-sm-8"><?=$this->Do->lcl($uinfo['city']['category_name'])?>&nbsp;</div>
		<?php if( $this->request->session()->read('Auth.User.id') == $uinfo['id'] ):?>
		<div class="form-group col-xs-12"><br />
			<button class="btn pull-right" type="button" ng-click="goTo('/myaccount', true)">
				<i class="fa fa-pencil"></i> 
				<?php echo __('change_contact_info')?></button>
		</div>
		<?php endif?>
	</div>
