
<div>

    <div><?=__('sendusmsg_desc')?></div>
    
    <?php
        echo $this->Form->create(@$contact, ['url' => ['controller' => 'contacts', 'action' => 'followus'], 'novalidate']);
        echo $this->Form->input('contact_name', ['label'=>false, 'placeholder'=>__('fullname'), 'style'=>'width:250px;']);
        echo $this->Form->input('contact_email', ['label'=>false, 'placeholder'=>__('email'), 'style'=>'width:250px;']);
        //echo $this->Do->captcha('1');
        echo $this->Form->button(__('Submit'));
        echo $this->Form->end();
    ?>
</div>