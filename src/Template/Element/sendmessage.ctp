
<div class="modal fade" id="sendmessage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
	<div class="listing-modal-1 modal-dialog " >
		<div class="modal-content">
			<div class="cvr" id="send_cvr"><i class="fa fa-refresh fa-spin fa-3x fa-fw"></i></div>
			<div class="modal-header">
				<button type="button" class="close" 
					data-dismiss="modal" aria-hidden="true" 
					id="close_btn">&times;</button>
				<h4 class="modal-title" id="myModalLabel">
					<?=__('send_message')?>
				</h4>
			</div>
			<div class="modal-body">
				<div class="listing-login-form">
					<form method="post" novalidate="novalidate" ng-submit="doSend('<?=$this->request->params['slug']?>');" id="send_form">
						<div class="listing-form-field">
							<i class="fa fa-user blue-1"></i>
							<input class="form-field bgwhite" type="text" chk="isEmpty"
								id="contact_name" name="contact_name"
                				placeholder="<?=__('contact_name')?>" 
                    			ng-model="msg.contact_name"/>
						</div>
						<div class="listing-form-field">
							<i class="fa fa-phone blue-1"></i>
							<input class="form-field bgwhite" type="tel" chk="isPhone"
								id="contact_phone" name="contact_phone"
                				placeholder="<?=__('contact_phone')?>" 
                    			ng-model="msg.contact_phone"/>
						</div>
						<div class="listing-form-field">
							<i class="fa fa-at blue-1"></i>
							<input class="form-field bgwhite" type="text" chk="isEmail"
								id="contact_email" name="contact_email"
                				placeholder="<?=__('contact_email')?>" 
                    			ng-model="msg.contact_email"/>
						</div>
						<div class="listing-form-field">
							<i class="fa fa-caret-down blue-1"></i>
							<?=$this->Form->input('contact_reason', ["options"=>$this->Do->get('contact_reason'), 
								"ng-model"=>"msg.contact_reason", "class"=>"form-select bgwhite", "label"=>false,
								"placeholder"=>__('contact_reason'), "empty"=>__('contact_reason'),
								 "chk"=>"isSelectEmpty", "id"=>"contact_message", "name"=>"contact_message"])?>
						</div>
						<div class="listing-form-field">
							<textarea class="form-textarea bgwhite" 
								id="contact_message" name="contact_message"
                				placeholder="<?=__('contact_message')?>" id="contact_message" name="contact_message"
                    			ng-model="msg.contact_message" chk="isParagraph"/></textarea>
						</div>
						<div class="listing-form-field">
							<input class="form-field submit" type="submit" value="<?=__('send')?>" />
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
