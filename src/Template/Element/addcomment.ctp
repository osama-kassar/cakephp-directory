
<div style="padding:8px;">

<h2><?=__('addcomment')?></h2>
<div><?=__('adcomment_msg')?></div>
<div id="answers"></div>
<?php
	echo $this->Form->create(null, ['url'=>['controller'=>'Comments', 'action'=>'add', $this->request->params['pass'][0]], 'novalidate', 'id'=>'fform']);
	for($i=1; $i<13; $i++){
		echo '<span>'.$this->Html->link(
				$this->Html->image('/img/emotions/emo'.$i.'.png', ['style'=>'width:30px;', 'id'=>'emo'.$i, 'class'=>'emo']), 
				'javascript:setEmo("emo'.$i.'")',
				['escape'=>false]).
			'</span>';
	}
	echo $this->Form->input('comment_name', ['label'=>__('fullname'), 'style'=>'max-width:400px;']);
	echo $this->Form->input('comment_email', ['label'=>__('email'), 'style'=>'max-width:400px;']);
	echo $this->Form->input('comment_body', ['type'=>'textarea', 'label'=>__('comment')]);
	echo $this->Form->input('comment_emo', ['type'=>'hidden', 'value'=>'emo11.png']);
	echo $this->Form->input('comment_id', ['type'=>'hidden', 'value'=>'0']);
	echo $this->Do->captcha('1', 'numeric');
	echo $this->Form->button(__('Submit'));
	echo $this->Form->end();
?>
</div>
<script>
$(document).ready(function(){
	$('#fform').submit(function(e){
		e.preventDefault();
			$("#fform button").attr('disabled','disabled');
			$.ajax({
			type:'POST',
			url:$('#fform').attr('action')+'?ajax=1',
			data:$('#fform').serialize(),
			success:function(response){
				$("#fform button").removeAttr('disabled');
				var dt = JSON.parse(response);
				if(Object.keys(dt).length > 0){
					showErrors(dt);
				}else{
					alert('<?=__('comment_to_review')?>');
					$('.error-message').remove();
					$("#fform").trigger('reset');
				}
			},
		});     

	})
});

$("#emo11").addClass('selectedEmo');
function setEmo(tar){
	$("#comment-emo").val(tar+'.png');
	$(".emo").removeClass('selectedEmo');
	$("#"+tar).addClass('selectedEmo');
}

</script>