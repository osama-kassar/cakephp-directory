<?php
$user=$this->request->session()->read('Auth.User');
?>
<div class="hmenu ">

	<div class="pull-left col-sm-6" style="text-align:left;">
		<div class="hideMob"><?=__('welcome').', '.$user['username']?></div>
		
        <span class="side_menu">
            <?=$this->Html->link(' <i class="fa fa-bell"></i> '.__('notifications'),
                ["prefix"=>"admin", "controller"=>"Users", "action"=>"edit", $user['id']],
                ["escape"=>false])?>
            <span class="badge"><?=count($notifications->toArray())?></span>
            <span class="itms">
                <ul>
                    <?php foreach($notifications as $itm){?>
                    <li><?=$this->Html->link($itm->assign_target, 
                            ["controller"=>"assignments", "action"=>"view", $itm->id])?></li>
                    <?php }?>
                </ul>
            </span>
            
            
            
            
        </span>
            
        <?=$this->Html->link(' <i class="fa fa-power-off"></i> '.__('logout'),
            "/logout",
            ["escape"=>false])?>

        <?=$this->Html->link(' <i class="fa fa-user"></i> '.__('edit'),
            ["prefix"=>"admin", "controller"=>"Users", "action"=>"edit", $user['id']],
            ["escape"=>false])?>
	</div>
	
	<div class="pull-right col-sm-6">
		<?=$this->Html->link(' <div class="btn btn-primary">
			<i class="fa fa-plus"></i> <span class="hideMob">'.__('Add').' '.__($this->request->controller.'_rec').'</span></div>',
			["prefix"=>"admin", "controller"=>$this->request->controller, "action"=>"add"],
			["escape"=>false])?> 
		<?=$this->Html->link(' <i class="fa fa-home"></i> ',
			'/',
			["escape"=>false])?> 
	</div>
</div>