<!-- PRICING AREA -->

<div class="col-sm-12 text-center">
	<div class="col-md-12 pricing-heading-title bt_heading_3">
		<h1><span>
			<?=__('plans')?>
			</span>
			<?=__('pricing')?>
		</h1>
		<div class="blind line_1"></div>
		<div class="flipInX-1 blind icon"><span class="icon">
			<i class="fa fa-stop"></i>&nbsp;&nbsp;<i class="fa fa-stop"></i></span></div>
		<div class="blind line_2"></div>
	</div>
	<div class="row">
		<div class="col-md-4 col-sm-4 col-xs-12">
			<div class="price-table-feature-block">
				<h1>Basic</h1>
				<hr>
				<p>Free <span>$24</span> Per Month</p>
				<div class="vfx-pl-seperator">
					<span><i class="fa fa-caret-down"></i></span>
				</div>
				<div class="vfx-price-list-item">
					<h2>Limited Number</h2>
					<p>Our company offers best pricing options for field agents and companies.</p>
				</div>
				<div class="vfx-price-list-item">
					<h2>One Agent for All</h2>
					<p>Our company offers best pricing options for field agents and companies.</p>
				</div>
				<div class="vfx-price-list-item">
					<h2>Mail Communication</h2>
					<p>Our company offers best pricing options for field agents and companies.</p>
				</div>
				<div class="vfx-price-btn">
					<button class="purchase-btn"><i class="fa fa-unlock-alt"></i> Purchase Now</button>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-4 col-xs-12">
			<div class="price-table-feature-block active">
				<h1>Premium</h1>
				<hr>
				<p>Free <span>$49</span> Per Month</p>
				<div class="vfx-pl-seperator">
					<span><i class="fa fa-caret-down"></i></span>
				</div>
				<div class="vfx-price-list-item">
					<h2>Unlimited Number</h2>
					<p>Our company offers best pricing options for field agents and companies.</p>
				</div>
				<div class="vfx-price-list-item">
					<h2>One Agent for All</h2>
					<p>Our company offers best pricing options for field agents and companies.</p>
				</div>
				<div class="vfx-price-list-item">
					<h2>Mail Communication</h2>
					<p>Our company offers best pricing options for field agents and companies.</p>
				</div>
				<div class="vfx-price-btn">
					<button class="purchase-btn"><i class="fa fa-unlock-alt"></i> Purchase Now</button>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-4 col-xs-12">
			<div class="price-table-feature-block">
				<h1>Plus</h1>
				<hr>
				<p>Free <span>$99</span> Per Month</p>
				<div class="vfx-pl-seperator">
					<span><i class="fa fa-caret-down"></i></span>
				</div>
				<div class="vfx-price-list-item">
					<h2>Unlimited Number</h2>
					<p>Our company offers best pricing options for field agents and companies.</p>
				</div>
				<div class="vfx-price-list-item">
					<h2>Unlimited Number</h2>
					<p>Our company offers best pricing options for field agents and companies.</p>
				</div>
				<div class="vfx-price-list-item">
					<h2>Personal Training</h2>
					<p>Our company offers best pricing options for field agents and companies.</p>
				</div>
				<div class="vfx-price-btn">
					<button class="purchase-btn"><i class="fa fa-unlock-alt"></i> Purchase Now</button>
				</div>
			</div>
		</div>
	</div>
	<div style="height:90px;"></div>
</div>

<!-- PRICING AREA  
<div id="pricing-item-block">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 text-center">
        <div class="col-md-12 pricing-heading-title bt_heading_3">
          <h1><span><?=__('plans')?></span> <?=__('pricing')?></h1>
          <div class="blind line_1"></div>
          <div class="flipInX-1 blind icon"><span class="icon"><i class="fa fa-stop"></i>&nbsp;&nbsp;<i class="fa fa-stop"></i></span></div>
          <div class="blind line_2"></div>
        </div>
		
		
		
		
		
		
		
        <div class="row">
          <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="price-table-feature-block">
              <h1>Basic</h1>
              <hr>
              <p>Free <span>$24</span> Per Month</p>
              <div class="vfx-pl-seperator"> <span><i class="fa fa-caret-down"></i></span> </div>
              <div class="vfx-price-list-item">
                <h2>Limited Number</h2>
                <p>Our company offers best pricing options for field agents and companies.</p>
              </div>
              <div class="vfx-price-list-item">
                <h2>One Agent for All</h2>
                <p>Our company offers best pricing options for field agents and companies.</p>
              </div>
              <div class="vfx-price-list-item">
                <h2>Mail Communication</h2>
                <p>Our company offers best pricing options for field agents and companies.</p>
              </div>
              <div class="vfx-price-btn">
                <button class="purchase-btn"><i class="fa fa-unlock-alt"></i> Purchase Now</button>
              </div>
            </div>
          </div>
          <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="price-table-feature-block active">
              <h1>Premium</h1>
              <hr>
              <p>Free <span>$49</span> Per Month</p>
              <div class="vfx-pl-seperator"> <span><i class="fa fa-caret-down"></i></span> </div>
              <div class="vfx-price-list-item">
                <h2>Unlimited Number</h2>
                <p>Our company offers best pricing options for field agents and companies.</p>
              </div>
              <div class="vfx-price-list-item">
                <h2>One Agent for All</h2>
                <p>Our company offers best pricing options for field agents and companies.</p>
              </div>
              <div class="vfx-price-list-item">
                <h2>Mail Communication</h2>
                <p>Our company offers best pricing options for field agents and companies.</p>
              </div>
              <div class="vfx-price-btn">
                <button class="purchase-btn"><i class="fa fa-unlock-alt"></i> Purchase Now</button>
              </div>
            </div>
          </div>
          <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="price-table-feature-block">
              <h1>Plus</h1>
              <hr>
              <p>Free <span>$99</span> Per Month</p>
              <div class="vfx-pl-seperator"> <span><i class="fa fa-caret-down"></i></span> </div>
              <div class="vfx-price-list-item">
                <h2>Unlimited Number</h2>
                <p>Our company offers best pricing options for field agents and companies.</p>
              </div>
              <div class="vfx-price-list-item">
                <h2>Unlimited Number</h2>
                <p>Our company offers best pricing options for field agents and companies.</p>
              </div>
              <div class="vfx-price-list-item">
                <h2>Personal Training</h2>
                <p>Our company offers best pricing options for field agents and companies.</p>
              </div>
              <div class="vfx-price-btn">
                <button class="purchase-btn"><i class="fa fa-unlock-alt"></i> Purchase Now</button>
              </div>
            </div>
          </div>
		  
		  
		  
        </div>
      </div>
    </div>
  </div>
</div>-->