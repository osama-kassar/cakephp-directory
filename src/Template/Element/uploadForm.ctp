<style>
.img_thumb{
	display:inline-block;
	padding: 4px;
}
.img_thumb img{
	height: 120px;
}
.photobulk{
	border:1px solid var(--lightblue);
	background:#F5F5F5;
	padding: 10px;
	border-radius: 4px;
}
.hint{
	color:#999;
	font-style:italic;
}
.photobulk{}
.cbStyle{
	position:relative !important;
	margin-left:auto !important;
}
.img_thumb{
	position:relative;	
}
.img_thumb a i{
	position:absolute;
	right:4px;
	top:4px;
	background:rgba(255,255,255,0.8);
	padding:2px 4px;
}
.file{
	max-width:100%;
}
</style>
<div id="upload-wrapper">
	<div class="photobulk">
        <?php
        empty($photosNumber) ? $photosNumber = 10 : $photosNumber;
        empty($name) ? $name = 'photo_' : $name;
        empty($recId) ? $recId = 0 : $recId;
        empty($msg) ? $msg = 'files_upload_rules' : $msg;
        empty($mdl) ? $mdl=$this->request->params['controller'] : $mdl;
        
        // show the old photos
        if(!empty($oldPhotos)){
            $photos_arr = explode('|', $oldPhotos);
            $dir = strtolower($this->request['controller']);
			foreach($photos_arr as $k => $v){
				echo '<div class="img_thumb" id="'.$photos_arr[$k].'">';
				echo $this->Html->link(
					$this->Html->image('/img/'.$dir.'_photos/thumb/'.$photos_arr[$k]), 
						'javascript:void();', 
						['escape'=>false, 'onclick'=>'showImg(this);', 
						'data-id'=>$app_folder.'/img/'.$dir.'_photos/'.$photos_arr[$k] ]
						);
				echo $this->Html->link('<i class="fa fa-times"></i>', 
					['controller'=>false, 'action'=>'delphoto', 'mdl'=>$mdl, 'id'=>$recId, 'photo'=>$photos_arr[$k], 'field'=>$name],
					['escape'=>false]);
				echo '</div>';
			}
		}
        ?>
    
        <div class="hint"><?=__($msg)?></div>
        <?php for($i=0; $i<$photosNumber; $i++){
                $display = ($i<1) ? 'block' : 'none';?>
                <input id="<?=$name.'_'.$i?>" 
                    name="<?=$name.'_'.$i?>" type="file" class="file" 
                    onchange="addFile('<?=$name.'_'.($i+1)?>');" 
                    style="display: <?=$display?>;"/>
        <?php }?>
        
    </div>
</div>