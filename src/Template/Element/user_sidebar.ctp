<?php 
    $uinfo = $this->request->session()->read('Auth.User');
    $img = !empty($uinfo['photo']) ? $uinfo['photo'] : 'noimg.jpg';
?>

<div class="col-md-3 col-sm-4 col-xs-12 hideMob">
	<div id="leftcol_item">
		<div class="user_dashboard_pic">
			<?=$this->Html->image('/img/users_photos/thumb/'.$img );?>
			<span class="user-photo-action"><?=__('edit_photo')?></span>
		</div>
	</div>
	<div class="dashboard_nav_item">
		<ul>
			<li class="{{currUrl.indexOf('dashboard')>-1 ? 'active' : ''}}">
				<?=$this->Html->link('<i class="fa fa-tachometer"></i> '.__('dashboard'), 
					['controller'=>'Users', 'action'=>'dashboard'], ['escape'=>false])?>
			</li>
			<li class="{{currUrl.indexOf('packages')>-1 ? 'active' : ''}}">
				<?=$this->Html->link('<i class="fa fa-pencil"></i> '.__('upgrade'), 
					['controller'=>'Users', 'action'=>'packages'], ['escape'=>false])?>
			</li>
			<li class="{{currUrl.indexOf('compan')>-1 ? 'active' : ''}}">
				<?php
					if(!empty($uinfo['company'])){
						echo $this->Html->link('<i class="fa fa-pencil"></i> '.__('edit_company'), 
							"/mycompany", ['escape'=>false]);
					}else{
						echo $this->Html->link('<i class="fa fa-plus"></i> '.__('add_company'), 
							['controller'=>'Companies', 'action'=>'add'], 
								['escape'=>false]);
					}
					?>
			</li>
			<li class="{{currUrl.indexOf('requests/add')>-1 ? 'active' : ''}}">
				<?=$this->Html->link('<i class="fa fa-plus"></i> '.__('add_request'), 
					['controller'=>'Requests', 'action'=>'add'], ['escape'=>false])?>
			</li>
			<li class="{{currUrl.indexOf('requests/mylist')>-1 ? 'active' : ''}}">
				<?=$this->Html->link('<i class="fa fa-bars"></i> '.__('mylist'), 
					['controller'=>'Requests', 'action'=>'mylist'], ['escape'=>false])?>
			</li>
			<li class="{{currUrl.indexOf('myaccount')>-1 ? 'active' : ''}}">
				<?=$this->Html->link('<i class="fa fa-pencil"></i> '.__('edit_profile'), 
					"/myaccount", ['escape'=>false])?>
			</li>
			<li class="{{currUrl.indexOf('logout')>-1 ? 'active' : ''}}">
				<?=$this->Html->link('<i class="fa fa-sign-out"></i> '.__('logout'), 
					['controller'=>'Users', 'action'=>'logout'], ['escape'=>false])?>
			</li>
		</ul>
	</div>
</div>