
<div class="actions_btns">
    <?php
        $rec->status == 0 ? 
        $enable = '<i class="fa fa-check-circle" aria-hidden="true"></i>' : 
        $enable = '<i class="fa fa-times-circle-o" aria-hidden="true"></i>';

        echo $this->Html->link('<i class="fa fa-pencil-square-o" aria-hidden="true"></i>', 
            ['action' => 'edit', $rec->id], ['escape'=>false]); 

        echo $this->Form->postLink($enable, ['action' => 'disable', $rec->id], 
            ['confirm' => __('change_status').' '.$rec->id, 'escape'=>false]); 

        echo $this->Form->postLink('<i class="fa fa-trash" aria-hidden="true"></i>', 
            ['action' => 'delete', $rec->id], 
            ['confirm' => __('delete_record').' '.$rec->id, 'escape'=>false]);
    ?>
</div>
<br>
<br>
<?php 
    if($this->request->controller == 'Assignments'){
?>
<div class="actions_btns2">
    <fieldset>
    <legend><?=__('assignment_task')?></legend>
<?php 
    
	   echo $this->Form->create(null, ['url'=>['controller'=>'Assignments', 'action'=>'edit', $this->request->params['pass'][0], $this->request->controller], 'novalidate']);
        
        echo $this->Form->control("assign.userid", ["label"=>__("users"), "type"=>"select", "options"=>$users]);
        
        echo $this->Form->control("assign.lang", ["label"=>__("langs"), "type"=>"select", "options"=>$langs]);
        
        //echo $this->Form->control("assign.expiredate", ["empty"=>"langs", "type"=>"text", "options"=>$langs, "label"=>__("days")]);
        
        echo $this->Form->control("assign.cloneTarget", ["type"=>"checkbox", "options"=>$users, "label"=>__("cloneTarget")]);
        
        echo $this->Form->control("assign.cloneAssign", ["type"=>"checkbox", "options"=>$users, "label"=>__("cloneAssign")]);
        
        echo $this->Form->button('<i class="fa fa-thumb-tack" aria-hidden="true"></i> '.__('assign'));
        
        echo $this->Form->end();
        ?>
        </fieldset>
</div>
<?php }?>