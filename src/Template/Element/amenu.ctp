<?php

		$url = $app_folder.'/admin/';
	$options=[
		$url.'dashboard'=>' <i class="fa fa-tachometer"></i> '.__('dashboard_m'),
		$url.'articles'=>' <i class="fa fa-rss-square"></i> '.__('articles_m'),
		$url.'assignments'=>' <i class="fa fa-recycle"></i> '.__('assignments_m'),
		$url.'categories'=>' <i class="fa fa-bars"></i> '.__('categories_m'),
		$url.'requests'=>' <i class="fa fa-exchange"></i> '.__('requests_m'),
		$url.'companies'=>' <i class="fa fa-suitcase"></i> '.__('companies_m'),
		//$url.'comments'=>' <i class="fa fa-comments"></i> '.__('comments_m'),
		$url.'contacts'=>' <i class="fa fa-envelope"></i> '.__('contacts_m'),
		$url.'users'=>' <i class="fa fa-users"></i> '.__('users_m'),
	];

?>
<script>
function showMenu(){
	if($('.amenu_holder').css('margin-right')=='-330px'){
		$('.amenu_holder').css('margin-right', '-15px');
		$('.menubtn a').html('<i class="fa fa-times"></i>');
	}else{
		$('.amenu_holder').css('margin-right', '-330px');
		$('.menubtn a').html('<i class="fa fa-bars"></i>');
	}
}
document.onclick = function(e) {
  if(!document.getElementById('amenu_holder').contains(e.target) && document.body.clientWidth < 992) {
		$('.amenu_holder').css('margin-right', '-330px');
		$('.menubtn a').html('<i class="fa fa-bars"></i>');
  }
}
</script>

<div class=" menubtn">
	<a href="javascript:showMenu();"><i class="fa fa-bars"></i></a>
</div>

	<nav class="row amenu_holder" id="amenu_holder">
		<header>
			<h1><?=__('CONTROL_PANEL')?></h1>
		</header>
		<?php 
			foreach($options as $k=>$v){
				$cls=''; 
				if( strpos($_SERVER['REQUEST_URI'], $k) > -1){
					$cls = 'current';
				}
		?> 
		<a href="<?=$k?>" class="<?=$cls?>"><?=$v?></a>
		<?php }?>
	</nav>