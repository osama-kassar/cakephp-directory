<?php
    $user = $this->request->session()->read('Auth.User');
    $img = !empty($user['photo']) ? $user['photo'] : 'noimg.jpg';
?>

<div id="logo-header" class="bigHeader"  set-fixed>
	<div class="container" >
		<div class="row" >
			<nav >
                
                
                
                
                <!-- MOBILE Bars button -->
				<div class=" hideWeb">
					<button type="button" class="navbar-toggle menu_btn" ng-click="showMenu()">
						<i class="fa fa-bars"></i>
					</button>
				</div>
				
                
                
                
                
                
				<!-- logo all devices -->
				<div class="col-sm-2 col-xs-9 logo_clm">
					<div id="logo">
						<?=$this->Html->image('/images/logo.png', ['url'=>'/'])?>
					</div>
				</div>
				
                
                
                
                
				<!-- main menu -->
				<div class="mobMenu" id="mobMenu" >
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
					<!-- right menu -->
					<div class="col-sm-6 col-xs-12  mmenu_clm">
						<a href="#" class="dropdown-toggle" 
								data-toggle="dropdown" role="button" 
								aria-haspopup="true" aria-expanded="false">
						</a>
						<div id="nav_menu_list">
							<ul>
							
								<!-- profile, login/logout buttons  MOBILE-->
								<span class="btn_item hideWeb" >
                                    
									<?php if(strpos($user['role'], 'user.')){?>
									<li>
										<?=$this->Html->link('<i class="fa fa-power-off"></i>'.
											__('logout'), '/logout', ['escape'=>false])?>
									</li>
									<li>
										<?=$this->Html->link('<i class="fa fa-home"></i>', '/', ['escape'=>false])?>
									</li>
									<li>
										<?=$this->Html->link(
											$this->Html->image("/img/users_photos/thumb/".$img),
											["controller"=>"Users", "action"=>"dashboard"],
											["escape"=>false, "class"=>"rounded_img"]
										)?>
									</li>
									<?php }elseif(strpos($user['role'], 'admin') !== false ){?>
                                    <li>
                                        <?=$this->Html->link('<i class="fa fa-cog"></i>', '/admin', ['escape'=>false])?>
                                    </li>
                                    <?php }else{ ?>
									<li >
										<?=$this->Html->link('<i class="fa fa-sign-in"></i> '.__('login'), '/login', 
											["data-toggle"=>"modal", "data-target"=>"#login", 'escape'=>false])?>
									</li>
									<?php }?>
								</span>
								
								
								
								
								<!-- WEB -->
								<span class="link_item">
									<li class="hideMob">
										<?=$this->Html->link('<i class="fa fa-home"></i>', '/', ['escape'=>false])?>
									</li>
									<li class="{{(currUrl|urlcrypt).indexOf(pages_name.companies)>-1 ? 'active2' : ''}}">
										<?=$this->Html->link(__('companies'), 
													['controller'=>'Companies', 'action'=>'index'])?>
									</li>
									<li class="{{(currUrl|urlcrypt).indexOf(pages_name.requests)>-1 ? 'active2' : ''}}">
										<?=$this->Html->link(__('requests'), 
													['controller'=>'Requests', 'action'=>'index'])?>
									</li>
									<li class="{{currUrl.indexOf('articles')>-1 ? 'active2' : ''}}">
										<?=$this->Html->link(__('articles'), 
													['controller'=>'Articles', 'action'=>'index'])?>
									</li>
								</span>
								
								<!-- user mobile menu -->
								<?php if(!empty($user)):?>
								<span class="hideWeb link_item">
									<h4><br />
										<b class="{{currUrl.indexOf('dashboard')>-1 ? 'active2' : ''}}">
										<?=$this->Html->link('<i class="fa fa-tachometer"></i> '.__('dashboard'), 
													['controller'=>'Users', 'action'=>'dashboard'], ['escape'=>false])?>
										</b></h4>
									<li class="{{currUrl.indexOf('packages')>-1 ? 'active2' : ''}}">
										<?=$this->Html->link('<i class="fa fa-pencil"></i> '.__('upgrade'), 
													['controller'=>'Users', 'action'=>'packages'], ['escape'=>false])?>
									</li>
									<li class="{{currUrl.indexOf('compan')>-1 ? 'active2' : ''}}">
										<?php
													if(!empty($user['company'])){
														echo $this->Html->link('<i class="fa fa-pencil"></i> '.__('edit_company'), 
															"/mycompany", ['escape'=>false]);
													}else{
														echo $this->Html->link('<i class="fa fa-plus"></i> '.__('add_company'), 
															['controller'=>'Companies', 'action'=>'add'], 
															['escape'=>false]);
													}
													?>
									</li>
									<!--
											<li class="{{currUrl.indexOf('exports')>-1 ? 'active' : ''}}">
												<?=$this->Html->link('<i class="fa fa-plus"></i> '.__('add_export'), 
													['controller'=>'Exports', 'action'=>'add'], ['escape'=>false])?>
											</li>
											<li class="{{currUrl.indexOf('services')>-1 ? 'active' : ''}}">
												<?=$this->Html->link('<i class="fa fa-plus"></i> '.__('add_service'), 
													['controller'=>'Services', 'action'=>'add'], ['escape'=>false])?>
											</li>
											-->
									<li class="{{currUrl.indexOf('requests')>-1 ? 'active2' : ''}}">
										<?=$this->Html->link('<i class="fa fa-plus"></i> '.__('add_request'), 
													['controller'=>'Requests', 'action'=>'add'], ['escape'=>false])?>
									</li>
									<li class="{{currUrl.indexOf('myaccount')>-1 ? 'active2' : ''}}">
										<?=$this->Html->link('<i class="fa fa-pencil"></i> '.__('edit_profile'), 
													"/myaccount", ['escape'=>false])?>
									</li>
									<li class="{{currUrl.indexOf('logout')>-1 ? 'active2' : ''}}">
										<?=$this->Html->link('<i class="fa fa-sign-out"></i> '.__('logout'), 
													['controller'=>'Users', 'action'=>'logout'], ['escape'=>false])?>
									</li>
								</span>
								<?php endif?>
							</ul>
						</div>
					</div>
					
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
					<!-- left menu -->
					<div class="col-sm-3 col-xs-12 sidemenu_clm" >
						<div id="nav_menu_list">
							<ul>
                                
                                <!-- LANGUAGES menu -->
								<li>
								    <?=$this->Html->link('<i class="fa fa-globe"></i> '.$currlang,
									'/'.$currlang,["escape"=>false])?>
                                    <div class="lang_menu">
                                    <?php
                                    foreach($this->Do->get('lang') as $itm){
                                        if($itm !== $currlang){
                                            echo $this->Html->link(
                                                $this->Html->image('/img/'.$itm.'.gif').' '.$itm,
                                                    '/'.$itm,["escape"=>false]);
                                        }
                                    }
                                    ?>
                                    </div>
								</li>
								<span class="btn_item " >
                                    <?php if(strpos($user['role'], 'user.') !== false){?>
                                    <li>
                                        <?=$this->Html->link('<i class="fa fa-power-off"></i> '.
                                            __('logout'), '/logout', ['escape'=>false])?>
                                    </li>
                                    <li>
                                        <?=$this->Html->link(
                                            $this->Html->image("/img/users_photos/thumb/".$img),
                                            ["controller"=>"Users", "action"=>"dashboard"],
                                            ["escape"=>false, "class"=>"rounded_img"]
                                        )?>
                                    </li>
                                    <?php }?>
                                    
                                    <?php if(strpos($user['role'], 'admin.') !== false){?>
                                    <li>
                                        <?=$this->Html->link('<i class="fa fa-cog"></i>', '/admin', ['escape'=>false])?>
                                    </li>
                                    <?php }?>
                                    
                                    <?php if(!isset($user)){ ?>
                                    <li >
                                        <?=$this->Html->link('<i class="fa fa-sign-in"></i> '.__('login'), '/login', 
                                            ["data-toggle"=>"modal", "data-target"=>"#login", 'escape'=>false, "class"=>"text_itm"])?>
                                    </li>
                                    <?php }?>
								</span>
							</ul>
						</div>
					</div>
				</div>
			</nav>
		</div>
	</div>
</div>

<script>
document.onclick = function(e) {
	if($('.mobMenu').css('right') !== '-800px'){
		if(!document.getElementById('mobMenu').contains(e.target) && document.body.clientWidth < 992) {
			$('.mobMenu').css('right', '-800px');
			$('.menu_btn').html('<i class="fa fa-bars"></i>');
		}
	}
}
</script>