<style>
.noAnim * { 
	-webkit-transition: none; transition: none;
}
.noAnim span{ 
	display:inline-block; width:auto !important; margin:4px 0;
}
.hider{
	display:none;
}
</style>

<?php echo $this->Html->script("//maps.google.com/maps/api/js?key=AIzaSyAeN8_Cs-kzyPIyAaynG7ROC6kq4msqJ9E&callback=initMap&sensor=false async defer"); ?>

<?php
	$mapplace = empty($mapplace) ? ['41.105','29.01'] : explode(',', $mapplace);
	$defMarkerIcon = 'http://'.$_SERVER['HTTP_HOST'].$app_folder.'/webroot/img/pnt.png';
	$defMarkerShadow = 'https://google-maps-icons.googlecode.com/files/shadow.png';
	
	$map_options = [
		"id"         => empty($id) ? "map_canvas" : $id,
		"width"      => empty($w) ? '450px' : $w,
		"height"     => empty($h) ? '300px' : $h,
		"type"	   => empty($mode) ? 'ROADMAP' : $mode,// Type of map - `ROADMAP`, `SATELLITE`, `HYBRID` or `TERRAIN`
		"zoom"       => empty($zoom) ? 12 : $zoom,
		"maxZoom" 	=> empty($maxZoom) ? 30 : $maxZoom,
		"minZoom"	=> empty($minZoom) ? 0 : $minZoom,
		"markerTitle" => empty($markerTitle) ? __('site_title') : $markerTitle,
		'windowText' => empty($windowText) ? __('sitename') : $windowText,
		'draggableMarker' => empty($draggableMarker) ? false : $draggableMarker,
		"localize"   => empty($localize) ? false : $localize,
		'infoWindow' => empty($infoWindow) ? false : $infoWindow,
		"latitude"   => @$mapplace[0],
		"longitude"  => @$mapplace[1],
		"marker"     => empty($marker) ? true : $marker,
		'markerIcon' => empty($markerIcon) ? $defMarkerIcon : $markerIcon,
		'markerShadow' => empty($markerShadow) ? $defMarkerShadow : $markerShadow,
	];
	
?>
<script>
function getVal(val,tar='company-location'){
	document.getElementById(tar).value = val;
}
</script>
<div class="noAnim  ">
	<?php echo $this->GoogleMap->map($map_options); ?>
	<span >
	<?=$this->Form->input('latitude_center', 
		['class'=>'hider', 'label'=>false, 'value'=>@$mapplace[0], 
			'id'=>'latitude_center', "onchange"=>"getVal(company-location, $mapplace[0],$mapplace[1])"])?>
	</span> 
	<span >
	<?=$this->Form->input('longitude_center', 
		['class'=>'hider', 'label'=>false, 'value'=>@$mapplace[1], 
			'id'=>'longitude_center', "onchange"=>"getVal(company-location, $mapplace[0],$mapplace[1])"])?>
	</span>
	<?php if(@$draggableMarker):?>
	<span class="">
		<button class="btn btn-info" onclick="localize(); return false;">
			<i class="fa fa-thumb-tack"></i> <?=__('get_current_location')?>
		</button>
	</span>
	<?php endif?>
</div>

