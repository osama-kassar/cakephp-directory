

<div class="modal fade" id="addlink" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
  <div class="listing-modal-1 modal-dialog" >
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="close_btn">&times;</button>
        <h4 class="modal-title" id="myModalLabel"><?=__('login')?></h4>
      </div>
      <div class="modal-body">
        <div class="listing-login-form">
           <form method="post" novalidate="novalidate">
            <div class="listing-form-field"> <i class="fa fa-user blue-1"></i>
				<input class="form-field bgwhite" type="text" 
                	placeholder="<?=__('ex')?>" id="username" name="username"
                    ng-model="userdt.username"/>
            </div>
            <div class="listing-form-field"> <i class="fa fa-lock blue-1"></i>
				<input class="form-field bgwhite" type="password" 
                	placeholder="<?=__('password')?>" id="password" name="password" 
                    ng-model="userdt.password"/>
            </div>
            <div class="listing-form-field clearfix margin-top-20 margin-bottom-20">
              <span style="float:right;">
              <input type="checkbox" id="checkbox-1-1" class="regular-checkbox" />
              <label for="checkbox-1-1"></label>
              <label for="checkbox-1-1" class="checkbox-lable"><?=__('remember-me')?></label>
              </span>
              <span style="float:left;"><a href="#"><?=__('forget-password')?></a></span>
			</div>
            <div class="listing-form-field">
              <input class="form-field submit" type="submit" value="<?=__('login')?>" />
            </div>
          </form>
          <div class="bottom-links">
            <p><?=__('no-account?')?><a href="#"><?=__('create-new-account')?></a></p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
