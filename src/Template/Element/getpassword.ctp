
<div class="modal fade" id="getpassword" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
	<div class="listing-modal-1 modal-dialog" >
		<div class="modal-content">
			<div class="cvr" id="getpassword_cvr"><i class="fa fa-refresh fa-spin fa-3x fa-fw"></i></div>
			<div class="modal-header">
				<button type="button" class="close" 
					data-dismiss="modal" aria-hidden="true" 
					id="close_btn">&times;</button>
				<h4 class="modal-title" id="myModalLabel">
					<?=__('getpassword')?>
				</h4>
			</div>
			<div class="modal-body">
				<div class="listing-login-form">
					<form method="post" novalidate="novalidate" ng-submit="doGetPassword();" id="getpassword_form">
						<div class="listing-form-field">
							<i class="fa fa-envelope blue-1"></i>
							<input class="form-field bgwhite" type="text" 
                				placeholder="<?=__('email')?>" id="email" name="email"
                    			ng-model="userdt.email"/>
						</div>
						<div class="listing-form-field">
							<input class="form-field submit" type="submit" value="<?=__('submit')?>" />
						</div>
					</form>
					<div class="bottom-links">
						<div>
							<?=__('no-account?')?>
							<a href="javascript:void()" 
									data-dismiss="modal"  
									data-toggle="modal" 
									data-target="#register">
								<?=__('create-new-account')?>
							</a>
						</div>
						<div>
							<a href="javascript:void()" 
									data-dismiss="modal"  
									data-toggle="modal" 
									data-target="#login">
								<?=__('dologin')?>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
