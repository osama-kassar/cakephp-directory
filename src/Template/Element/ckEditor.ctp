<div>

<?php echo $this->Html->script('/plugins/ckeditor/ckeditor') ?>
<?php //echo $this->Html->script('/plugins/ckeditor/ckeditor4.10.1') ?>
<?php echo $this->Form->input($textarea_name,['type'=>'textarea', 'label'=>__($textarea_name)]);?>
<?php $currlang='ar';$currlang == 'ar' ? $dir = 'rtl' : $dir = 'ltr';?>


<script>
	CKEDITOR.replace( '<?=$textarea_id?>',
    {
		filebrowserBrowseUrl: '<?=$app_folder?>/pages/photos',
		filebrowserImageBrowseUrl: '<?=$app_folder?>/pages/photos',
		filebrowserUploadUrl: '<?=$app_folder?>/admin/articles/uploadphoto?ajax=1',
		filebrowserImageUploadUrl: '<?=$app_folder?>/admin/articles/uploadphoto?ajax=1',
		contentsLangDirection: '<?=$dir?>',
		//BasePath = '/ckfinder/' ,
		//baseHref : app_folder+'/img/media/thumb/',
		//image2_prefillDimensions = app_folder+'/img/media/thumb/',
        toolbar :
        [
            [ 'Maximize' ],
            [ 'Source' ],
            [ 'BidiLtr', 'BidiRtl' ],
            //[ 'Bold','Italic','Underline','Strike' ],
			//[ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ],
			//[ 'Find','Replace' ],
			[ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ],
			[ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock', 'ShowBlocks'],
			//[ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ],
			[ 'Link','Unlink','Anchor' ],
			[ 'Image','Flash','Youtube','Table','HorizontalRule','Smiley','SpecialChar' ],
			[ 'Format'/*,'Styles','Font'*/ ],
			[ 'TextColor','BGColor','FontSize' ],
			//[ 'Maximize','ShowBlocks' ]
        ],
        height: "450px",
        width: "100%",
    });
	CKEDITOR.on( 'dialogDefinition', function( ev ){
		var dialogName = ev.data.name;
		var dialogDefinition = ev.data.definition;
		if ( dialogName == 'image' ) {
			var infoTab = dialogDefinition.getContents( 'info' );
			var urlField = infoTab.get( 'txtUrl' );
			urlField['default'] = '<?=$app_folder?>/webroot/img/media/thumb/';
		}
	});

</script>
</div>