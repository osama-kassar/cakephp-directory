<style>
.img_thumb{
	display:inline-block;
	padding: 4px;
}
.img_thumb img{
	height: 120px;
}
.photobulk{
	border: 2px solid #ccc;
	background:#F5F5F5;
	padding: 10px;
	margin:10px;
}
.hint{
	color:#999;
	font-style:italic;
}
.photobulk{}
#uploadBTN{
	height:26px;
}

.formStyle span{
	display:inline-block;
}
.formStyle i{
	font-size:30px;
	line-height:30px;
	margin:-12px  0 0 0;
	padding:0;
	height:30px;
	width:30px;
}
</style>
<div class="formStyle">
	<?php echo $this->Form->create(null, [
                'novalidate', 
                'id'=>'imageUploadForm', 
                'onsubmit'=>'javascript:return uplaod;', 
                'enctype'=>'multipart/form-data']);?>
    <?php echo $this->Form->input('phname', ['label'=>__('phname')]);?>
            
    <span><?php echo $this->Form->button(__('SubmitPhoto'));?></span>
            
    <span><?php echo $this->Form->input('photo_1', [
                'type'=>'file', 'label'=>false, 
                'id'=>'photo_1', 'name'=>'photo_1']);?></span>
    <?php echo $this->Form->end();?>
    <i class="fa fa-refresh fa-spin fa-3x fa-fw" style="opacity:0;" id="loader"></i>

</div>

<hr />
<iframe src="<?=$app_folder?>/pages/photos" width="98%" height="500" id="photos" frameborder="0"></iframe>
<?php echo $this->Html->script('jquery-3.1.0.min') ?>
<script>

var dmn = 'http://'+document.location.hostname+app_folder;
var post_url = dmn+'/admin/articles/uploadphoto/';


var uplaod = 
	$("#imageUploadForm").on('submit',(function(e) {
		$('#imageUploadForm').submit(false);
		e.preventDefault();
		$("#loader").css("opacity", "1");
		$.ajax({
			url: post_url+$("#phname").val(),
			type: "POST",
			data:  new FormData(this),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				$("#loader").css("opacity", "0");
				document.getElementById('photos').contentWindow.location.reload(true);
			},
			error: function(){
				alert('error: you need permessions');
			}
		});
	}));


function addPhoto(img){
	var content2 = '<span style="width:32%; display:inline-block;"><a href="'+app_folder+'/img/media/'+img+'" target="_blank"><img src="'+app_folder+'/img/media/thumb/'+img+'" height="80"></a><br>'+img+'</span>';
	$('#upload-wrapper').append(content2);
}
</script>
