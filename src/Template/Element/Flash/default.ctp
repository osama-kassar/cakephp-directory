<?php
$class = 'message';
if (!empty($params['class'])) {
    $class .= ' ' . $params['class'];
}
?>

<div class="message default" onclick="this.classList.add('animated', 'fadeOutUp');"><span class="animated jello"><?= h($message) ?></span></div>



