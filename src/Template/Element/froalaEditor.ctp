<div>
    <!-- Include Editor style. -->
	<?php echo $this->Html->css('/plugins/froala/css/froala_editor.min') ?>
	<?php echo $this->Html->css('/plugins/froala/css/froala_style.min') ?>
     
    <!-- Include JS file. -->
	<?php echo $this->Html->script('/plugins/froala/js/froala_editor.min') ?>
	
	
	
	<?php echo $this->Form->input($textarea_name,['type'=>'textarea', 'label'=>__($textarea_name)]);?>
	
	<script>
		var editor_id = '<?=$textarea_name?>';
        $('textarea#article-body').froalaEditor({
          // Set image buttons, including the name
          // of the buttons defined in customImageButtons.
          imageEditButtons: ['imageDisplay', 'imageAlign', 'imageInfo', 'imageRemove']
        })
	</script>
	
</div>