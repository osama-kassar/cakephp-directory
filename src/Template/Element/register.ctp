<div class="modal fade" id="register" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="listing-modal-1 modal-dialog" style="width:100%;max-width:500px;">
		<div class="modal-content">
			<div class="cvr" id="register_cvr"><i class="fa fa-refresh fa-spin fa-3x fa-fw"></i></div>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel2">
					<?=__('registration')?>
				</h4>
			</div>
			<div class="modal-body">
				<div class="listing-register-form">
					<form method="post" novalidate="novalidate"
						ng-submit="doRegister();" id="reg_form">
						<div class="col-sm-6 col-xs-12 listing-form-field ">
							<i class="fa fa-user blue-1"></i>
							<input class="form-field bgwhite" 
								type="text" name="firstname" id="firstname" 
								placeholder="<?=__('first-name')?>"  
								ng-model="registerdt.firstname" chk="isEmpty"/>
						</div>
						<div class="col-sm-6 col-xs-12 listing-form-field ">
							<i class="fa fa-user blue-1"></i>
							<input class="form-field bgwhite" 
								type="text" name="lastname" 
								id="lastname" placeholder="<?=__('last-name')?>"  
								ng-model="registerdt.lastname" chk="isEmpty"/>
						</div>
						<div class="col-sm-12 col-xs-12 listing-form-field ">
							<i class="fa fa-envelope blue-1"></i>
							<input class="form-field bgwhite" 
								type="text" name="username" id="username" 
								placeholder="<?=__('email')?>" 
								ng-model="registerdt.username" chk="isEmail"/>
						</div>
						<div class="col-sm-12 col-xs-12 listing-form-field ">
							<i class="fa fa-mobile blue-1"></i>
							<input class="form-field bgwhite" type="tel"  chk="isPhone"
								name="phone" id="phone" placeholder="<?=__('mobile')?>" 
								ng-model="registerdt.phone"/>
						</div>
						<div class="col-sm-12 col-xs-12 listing-form-field ">
							<i class="fa fa-eye-slash blue-1" show-password></i>
							<input class="form-field bgwhite" type="password" 
								name="password" id="password"  chk="isPassword"
								placeholder="<?=__('password')?>"  
								ng-model="registerdt.password"/>
						</div>

						<div class="col-sm-6 col-xs-12 listing-form-field ">
							<i class="fa fa-map-marker  blue-1"></i>
							<?php echo $this->Form->input("country_id", [
								"onChange"=>"getSubCat(this, 'city-id-reg')", "type" => "select", 
								"options" => $this->Do->lcl(@$counteries), "empty" => __("f_country"), "label"=>false, 
								"class"=>"form-select bgwhite", "chk"=>"isSelectEmpty",
								"ng-model"=>"registerdt.country_id"])?>
						</div>
						<div class="col-sm-6  col-xs-12 listing-form-field">
							<i class="fa fa-street-view blue-1"></i>
							<?php echo $this->Form->input("city_id",  [
								"id"=>"city-id-reg", 
								"type"=>"select", "chk"=>"isSelectEmpty",  
								"empty" => __("f_city"), "label"=>false, 
								"options"=>@$cities,
								"class"=>"form-select bgwhite"])?>
						</div>

						<div class="clearfix"></div>
						<div class="gray" id="iagree_div">
							<?php
								echo $this->Form->input('iagree', [
									'type'=>'checkbox',
									'label'=>['text' => '<span>'.__('iagree').
									' <a href="javascript:void();">'
										.__('terms-conditions').'
									</a> '.__('and').
									' <a href="javascript:void();">'
										.__('privacy-policy').'
									</a></span>',  
									'class' => 'mycheckbox gray', 'escape'=>false],
									'ng-model'=>'registerdt.iagree',
									'ng-false-value'=>"'no'",
           							'ng-true-value'=>"'yes'",
									 "chk"=>"isEmpty"
									]);
							?>
							
						</div>
						<div class="gray" id="maillist_div">
							<?php
								echo $this->Form->input('maillist', [
									'type'=>'checkbox',
									'label'=>['text' => '<span>'.__('iagree').
									' <a href="javascript:void();">'
										.__('addme_to_maillist').'
									</a></span>',  
									'class' => 'mycheckbox gray', 'escape'=>false],
									'ng-model'=>'registerdt.maillist',
									'ng-false-value'=>"'no'",
           							'ng-true-value'=>"'yes'"
									]);
							?>
							
						</div>
						<div class="listing-form-field">
							<input class="submit" type="submit" value="<?=__('create-account')?>" id="btn"/>
						</div>
					</form>
					<div class="bottom-links">
						<div>
							<?=__('already-have-account')?>
							<a href="javascript:void()" 
									data-dismiss="modal"  
									data-toggle="modal" 
									data-target="#login">
								<?=__('dologin')?>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>