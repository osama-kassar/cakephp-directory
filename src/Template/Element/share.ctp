<div class="shareBar">
<h4><?=__('shareit')?></h4>
<a href="javascript:void(0);" 
	ng-click="openWin('facebook'); callAction('/sharecounter?id=<?=$obj->id?>&mdl=<?=$mdl?>'); ">
	<i class="fa fa-facebook"></i>
</a>
<a href="javascript:void(0);" 
	ng-click="openWin('twitter'); callAction('/sharecounter?id=<?=$obj->id?>&mdl=<?=$mdl?>'); ">
	<i class="fa fa-twitter"></i>
</a>
<a href="javascript:void(0);" 
	ng-click="openWin('google-plus'); callAction('/sharecounter?id=<?=$obj->id?>&mdl=<?=$mdl?>'); ">
	<i class="fa fa-google-plus"></i>
</a>
<a href="javascript:void(0);" 
	ng-click="openWin('linkedin'); callAction('/sharecounter?id=<?=$obj->id?>&mdl=<?=$mdl?>'); ">
	<i class="fa fa-linkedin"></i>
</a>
<a href="javascript:void(0);" 
	ng-click="openWin('pinterest'); callAction('/sharecounter?id=<?=$obj->id?>&mdl=<?=$mdl?>'); ">
	<i class="fa fa-pinterest"></i>
</a>
<a href="javascript:void(0);" 
	ng-click="openWin('email'); callAction('/sharecounter?id=<?=$obj->id?>&mdl=<?=$mdl?>'); ">
	<i class="fa fa-envelope"></i>
</a>
<a><i class="fa"><?=__('shared')?>: <?=$obj->stat_shares==null?0:$obj->stat_shares?></i></a>
</div>