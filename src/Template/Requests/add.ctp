<style>
.from-list-lt .from-input-ic {
	top: 28px;
}
</style>
<div id="breadcrum-inner-block">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 text-center">
				<div class="breadcrum-inner-header">
					<h1>
						<?=__('add_request')?>
					</h1>
					<?=$this->Html->link(__('home'), ['/'])?>
					<i class="fa fa-circle"></i>
					<?=$this->Html->link(__('myrequests'), ['/myrequests'])?>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="pricing-item-block" class="white_bg_block ">
	<div class="container ">
		<div class="row ">
			
					<?=$this->element('user_sidebar')?>
			<!-- body -->
			<div class="col-md-9 col-sm-8 col-xs-12   ">
				<div class="col-md-12 all-categorie-list-title bt_heading_3 text-center">
					<h1>
						<?=__('myrequests')?>
					</h1>
					<div class="blind line_1"></div>
					<div class="flipInX-1 blind icon">
						<span class="icon"><i class="fa fa-stop"></i>&nbsp;&nbsp;<i class="fa fa-stop"></i></span>
					</div>
					<div class="blind line_2"></div>
				</div>
				<div class="from-list-lt row">
					<div class="col-md-12">
						<div class="col-sm-12" >
							<p><?php echo __('add_request_msg'); ?></p>
						</div>
						<div class="col-sm-12" >
							<?php echo  $this->Form->create($request, ['enctype'=>'multipart/form-data', 'class'=>'form-float form-alt', 'novalidate' => true]) ?>
							<div class="row">
								<div class="form-group col-md-12">
									<span class="from-input-ic"><i class="fa fa-user"></i></span>
									<?php echo $this->Form->input('req_title', 
										['type'=>'text', 'label'=>__('req_title'), 
										'placeholder'=>__('req_title_desc'), "chk"=>"isEmpty", 
										'class'=>'form-control'])?>
								</div>
								<div class="form-group col-md-12">
									<span class="from-input-ic"><i class="fa fa-envelope-o"></i></span>
									<?php echo $this->Form->input('req_desc', 
										['type'=>'textarea', 'label'=>__('req_body'), 
										'placeholder'=>__('req_body_desc'), "chk"=>"isParagraph", 
										'class'=>'form-control'])?>
								</div>
								<div class="form-group col-md-6 col-xs-12">
									<span class="from-input-ic"><i class="fa fa-bars"></i></span>
									<?php echo $this->Form->input('category_id', 
										['onClick'=>'getSubCat(0, "subcategory-id")', "chk"=>"isSelectEmpty",
										'onChange'=>'getSubCat(this, "subcategory-id")', 'type' => 'select', 
										'options' => @$categories, 'empty' => __('f_category'), 'label'=>__('category_id'), 
										'class'=>'form-control'])?>
								</div>
								<div class="form-group col-md-6 col-xs-12"> 
									<span class="from-input-ic"><i class="fa fa-bars"></i></span>
									<?php echo $this->Form->input('subcategory_id', 
										['type'=>'select', 'label'=>__('subcategory_id'), "chk"=>"isSelectEmpty", 
										'class'=>'form-control', 
										'options' => @$subcategories,
										'empty' => __('f_subcategory')])?>
								  </div>
								
								<div class="form-group col-md-6 col-xs-12">
									<span class="from-input-ic"><i class="fa fa-bars"></i></span>
									<?php echo $this->Form->input('country_id', [
										'onClick'=>'getSubCat(0, "city-id")', "chk"=>"isSelectEmpty",
										'onChange'=>'getSubCat(this, "city-id")', 'type' => 'select', 
										'options' => $this->Do->lcl(@$countries), 'empty' => __('f_country'), 'label'=>__('f_country'), 
										'class'=>'form-control'])?>
								</div>
								<div class="form-group col-md-6 col-xs-12"> 
									<span class="from-input-ic"><i class="fa fa-bars"></i></span>
									<?php echo $this->Form->input('city_id', [
										'type'=>'select', 'label'=>__('city_id'), "chk"=>"isSelectEmpty", 
                                        'options' => $this->Do->lcl(@$cities),
										'class'=>'form-control', 
										'empty' => __('f_city')])?>
								</div>
								<div class="clearfix form-group col-xs-12">
									<div>
										<label>
											<?=__('req_preferred_contacts');?>
										</label>
									</div>
									<div class="mychckbox_container">
                                        
                                        <?= $this->Form->select('req_preferred_contacts', 
                                            $this->Do->lcl( $this->Do->get('prefered_contacts') ),
                                                 ['multiple'=>'checkbox',
                                                  'label'=>['class'=>'mycheckbox gray'],
                                                  'chk'=>'isEmpty']); ?>
									</div>
								</div>
								<div class="clearfix form-group col-xs-12 ">
									<div>
										<label>
											<?=__('req_preferred_langs');?>
										</label>
									</div>
                                    <div class="mychckbox_container">
                                        <?= $this->Form->select('req_preferred_langs', 
                                            $this->Do->lcl( $this->Do->get('prefered_langs') ),
                                                 ['multiple'=>'checkbox',
                                                  'label'=>['class'=>'mycheckbox gray'],
                                                  'chk'=>'isEmpty']); ?>
                                    </div>
								</div>
							
								<div class="form-group col-md-12 col-xs-12">
									<?php echo $this->Form->input('seo_desc', ["chk"=>"isEmpty", 
										'type'=>'text', 'label'=>__('seo_desc'), 
										'placeholder'=>__('seo_desc'), 'class'=>'form-control'])?>
								</div>
							
                                <div class="form-group col-md-12 col-xs-12 ">
                                    <?=$this->element("tagInput", ["name"=>"seo_keywords"]);?>
                                </div>
								
								<div class="clearfix form-group col-xs-12 ">
									<label><?=__('req_photos')?></label>
									<?php echo $this->element('uploadForm', ['name'=>'req_photos', 
									"msg"=>"upload_req_photos_msg"]);?>
								</div>
							
								<div class="form-group col-md-12">
									<button class="btn pull-right" type="submit"><?php echo __('Submit')?></button>
								</div>
							</div>
							</form>
							<?php echo $this->element('contact_info', ['uinfo'=>$request->user]);?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
