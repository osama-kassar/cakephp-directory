<?php
	$this->assign('title',$request->req_title);
	$this->assign('keywords',$request->seo_keywords);
	$this->assign('description',$request->seo_desc);
?>

<!-- HEADER -->
<div class="details-lt-block">
	<div class="container header_slt_block">
		<div class="slt_item_head">
			<div class="user_logo_pic">
				<?php
			$photos = explode("|", $request->req_photos);
			echo $this->Html->image('/img/requests_photos/thumb/'.$photos[0])?>
			</div>
			<div class="slt_item_contant">
				<h1>
					<?=$request->req_title?>
				</h1>
				<!--<div class="rating-box">
					<div class="rating">
						<span>
						<i class="fa fa-star"></i>
						<i class="fa fa-star"></i>
						<i class="fa fa-star"></i>
						<i class="fa fa-star"></i>
						<i class="fa fa-star-o"></i>
						</span>
					</div>
				</div>-->
				<div class="head-bookmark-bock">
					<div class="detail-banner-btn">
						<a href="#">
							<i class="fa fa-bars"></i>
							<?=$request->category->category_name?>
						</a>
					</div>
					<div class="detail-banner-btn">
						<a href="#">
							<i class="fa fa-bars"></i>
							<?=@$request->subcategory->category_name?>
						</a>
					</div>
					<span class="location"><i class="fa fa-map-marker"></i>
						<?=__($request->city->category_name)?>
					</span>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="vfx-product-inner-item">
<div class="container">
	<div class="row">
		
		<!-- BODY -->
		<div class="col-md-9 col-sm-8 col-xs-12">
			<div class="slider">
				<div class="detail-gallery">
					<div class="detail-gallery-preview">
						<?php
							$imgs = explode("|",$request->req_photos);
							echo $this->Html->link(
									$this->Html->image("/img/requests_photos/".$imgs[0]),
									"/img/requests_photos/".$imgs[0], ["escape"=>false]
								)."</div>";
								
							echo '
							<div class="owl-carousel owl-theme owl-loaded owl-drag">
								<div class="owl-stage-outer">
									<ul class="detail-gallery-index">';
							for($i=0; $i<count($imgs); $i++){
								echo '<li class="detail-gallery-list-item active">';	
								echo $this->Html->link(
										$this->Html->image("/img/requests_photos/thumb/".$imgs[$i]), 
										"javascript:void();", 
										["data-target"=>$app_folder."/img/requests_photos/".$imgs[$i], "escape"=>false]
								);
								echo '</li>';
							}
							echo '</ul>
								</div>
							</div>';
						?>
					</div>
				</div>
				<div class="dlt-title-item">
					<h2>
						<?=$request->req_title?>
					</h2>
					<div>
						<span>
						<?=__('stat_views').' '.$request->stat_views?>
						</span> / <span>
						<?=__('req_no').' '.$request->id?>
						</span>
					</div>
					<article>
						<?=$request->req_desc?>
					</article>
					<div>
						<span><i class="fa fa-bars"></i></span>
						<span> <?=@$request->category->category_name?> </span> / 
						<span> <?=@$request->subcategory->category_name?> </span> - 
						<span><i class="fa fa-map-marker"></i></span>
						<span> <?=__( @$request->country->category_name )?> </span> / 
						<span> <?=__( @$request->city->category_name )?> </span>
					</div>
					<p>
						<?=$this->element('share',["obj"=>$request, "mdl"=>"Requests"])?>
					</p>
				</div>
				<div class="dlt-spons-item">
					<?php 
						/*$tags = explode(",",$request->seo_keywords);
						$vids = explode(",",$request->req_videos);
						if(!empty($tags[0])){
							foreach($tags as $tag){
								echo '<a href="javascript:void()"><i class="fa fa-tag"></i> '.$tag.'</a>';
							}
						}*/
					?>
					<div class="archive-tag">
						<?=$this->Do->tags($request->seo_keywords)?>
					</div>
				</div>
							<?php echo $this->element('contact_info', ['uinfo'=>$request->user]);?>
			</div>
			
			<!-- SIDE BAR -->
			<div class="col-md-3 col-sm-4 col-xs-12">
				<div class="left-slide-slt-block">
					<h3>
						<?=__('subcategories')?>
					</h3>
				</div>
				<div class="list-group" id="subcategory_list">
					<?php
					foreach($subcategories as $subcategory){  
						$lbl = ' <i class="'.($subcategory->category_params == '' ? 
								'fa fa-briefcase' : $category->category_params).'"></i> '.
								$subcategory->category_name;
						echo $this->Html->link($lbl, 
						["controller"=>"Requests", $this->Do->appendURL('category='.$subcategory->id, "params")],
						["class"=>"list-group-item ".
							(@$this->request->query['category'] == $subcategory->id ? 'active' : ''),
						"escape"=>false]
						);
					}
					?>
					<?php if(count($subcategory) > 6):?>
					<button class="show_more" ng-click="showMore('subcategory_list')">
					<i class="fa fa-arrows-v" aria-hidden="true"></i>
					<?=__('show_more')?>
					</button>
					<?php endif ?>
				</div>
				
				
				
				
				
	<?php if(!empty($similar_requests[0])):?>
				<div class="left-slide-slt-block">
					<h3>
						<?=__('similar_requests')?>
					</h3>
				</div>
				<?php 
		
			foreach($similar_requests as $req):
				if($request->id !== $req->id):?>
				<div class="feature-item-container-box listing-item">
					<div class="feature-title-item">
						<h1>
							<?=$req->category->category_name?>
						</h1>
						<?php
						$photos2 = explode("|", $req->req_photos);
						echo $this->Html->link(
							$this->Html->image('/img/requests_photos/thumb/'.$photos2[0]),
							['action'=>'view', $req->slug],
							["escape"=>false])?>
					</div>
					<div class="feature-box-text">
						<h3>
							<?=$this->Html->link(
								$req->req_title, 
								['action'=>'view', $req->slug])
							?>
						</h3>
					</div>
					<div class="feature-item-location">
						<h2><i class="fa fa-map-marker"></i>
							<?=__( $req->country->category_name )?>
						</h2>
						<span>
						<?=$this->Html->link(
							__('show_details'), 
							['action'=>'view', $req->slug])
							?>
						</span>
					</div>
				</div>
		<?php endif; endforeach; endif; ?>
				<div class="left-slide-slt-block">
					<?=$this->Html->link('<button class="btn-quote" style="width:100%;">
				'.__('all_requests').' <i class="fa fa-info-circle"></i></button>',
				'/طلبات-الاستيراد/', ['escape'=>false])?>
				</div>
			</div>
		</div>
	</div>
</div>