<?php
	$this->assign('title',$article->article_title);
	$this->assign('keywords',$article->seo_keywords);
	$this->assign('description',$article->seo_desc);
?>

<div class="details-lt-block">
	<div class="container header_slt_block">
		<div class="slt_item_head">
			<div class="row">
				<div class="col-sm-12 text-center">
					<div class="breadcrum-inner-header">
						<h1>
							<?=$article->article_title?>
						</h1>
						<?=$this->Html->link(__('home'), ['/'])?>
						<i class="fa fa-circle"></i>
						<?=$this->Html->link('<span>'.$article->category->category_name.'</span>', 
						  ['controller'=>'articles', 'cat='=>$article->category_id], 
						  ['escape'=>false])?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="vfx-product-inner-item">
	<div class="container">
		<div class="row">
			<div class="col-md-9 col-sm-8 col-xs-12">
			
    			<div><?php echo $this->element('share', ["obj"=>$article, "mdl"=>"Articles"])?></div>
				<article class="col-sm-12">
					<?=$article->article_body?>
				</article>
    			<div><?php echo $this->element('share', ["obj"=>$article, "mdl"=>"Articles"])?></div>
				
				<section class="col-sm-12">
					<div><?=$article->stat_views.' '.__('stat_views')?></div>
					<div><?=__('date').':'.$article->article_postdate?></div>
					<div><?=__('article_author').': <b>'.$article->article_author?></div></b> 
					<div><?=__('article_resource').': '.$article->article_resource?></div>
					<div class="archive-tag">
						<?=$this->Do->tags($article->seo_tags)?>
					</div>
				</section>
				
			</div>
			
			<!-- SIDE BAR -->
			<div class="col-md-3 col-sm-4 col-xs-12">
			
			<?php foreach($related as $itm):
					if($article->id !== $itm->id):?>
				<div class="feature-item-container-box listing-item">
					<div class="feature-title-item">
						<h1>
							<?=$itm->category->category_name?>
						</h1>
						<?php
						echo $this->Html->link(
							 $this->Html->image('/img/media/thumb/'.$itm->article_photo),
							['action'=>'view', $itm->slug],
							["escape"=>false])?>
					</div>
					<div class="feature-box-text">
						<h3>
							<?=$this->Html->link(
								$itm->article_title, 
								['action'=>'view', $itm->slug])
							?>
						</h3>
					</div>
					<div class="feature-item-location">
						<h2><i class="fa fa-map-marker"></i>
							<?=$itm->article_postdate?>
						</h2>
						<span>
						<?=$this->Html->link(
							__('show_details'), 
							['action'=>'view', $itm->slug])
							?>
						</span>
					</div>
				</div>
				<?php endif; endforeach; ?>
			
			
				<!--<div class="sidebar_box">
					<h3>
						<?=__('company_address')?>
					</h3>
					<i class="fa fa-map-marker"></i>
					<?=$company->company_address?>
				</div>-->
			</div>
		</div>
	</div>
</div>
