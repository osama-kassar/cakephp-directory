<style>
.from-list-lt .from-input-ic {
	top: 28px;
}
</style>
<div id="breadcrum-inner-block">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 text-center">
				<div class="breadcrum-inner-header">
					<h1>
						<?=__('dashboard')?>
					</h1>
					<?=$this->Html->link(__('home'), ['/'])?>
					<i class="fa fa-circle"></i>
					<?=$this->Html->link(__('myaccount'), ['/myaccount'])?>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="pricing-item-block" class="white_bg_block ">
	<div class="container ">
		<div class="row ">
		
			<?=$this->Element('user_sidebar')?>
			
			<!-- body -->
			<div class="col-md-9 col-sm-8 col-xs-12  ">
				<div class="col-md-12 all-categorie-list-title bt_heading_3 text-center">
					<h1>
						<?=__('myaccount')?>
					</h1>
					<div class="blind line_1"></div>
					<div class="flipInX-1 blind icon"><span class="icon"><i class="fa fa-stop"></i>&nbsp;&nbsp;<i class="fa fa-stop"></i></span></div>
					<div class="blind line_2"></div>
				</div>
				<div class="from-list-lt row">
					<div class="col-md-12">
						<div class="col-sm-12" >
							<p><?php echo __('edit_user_msg'); ?></p>
						</div>
						<div class="col-sm-12" >
							<?php echo  $this->Form->create($user, 
								['enctype'=>'multipart/form-data', 
								'type'=>'post', 'class'=>'form-float form-alt', 
								'novalidate' => true]) ?>
							<div class="row">
								<div class="form-group col-md-6 col-xs-12">
									<span class="from-input-ic"><i class="fa fa-user"></i></span>
									<?php echo $this->Form->input('firstname', 
										['type'=>'text', 'label'=>__('firstname'), "chk"=>"isEmpty", 
										'placeholder'=>__('firstname'), 'class'=>'form-control'])?>
								</div>
								<div class="form-group col-md-6 col-xs-12">
									<span class="from-input-ic"><i class="fa fa-user"></i></span>
									<?php echo $this->Form->input('lastname', 
										['type'=>'text', 'label'=>__('lastname'), "chk"=>"isEmpty", 
										'placeholder'=>__('lastname'), 'class'=>'form-control'])?>
								</div>
								<div class="form-group col-md-12 col-xs-12">
									<span class="from-input-ic"><i class="fa fa-user"></i></span>
									<?php echo $this->Form->input('username', 
										['type'=>'text', 'label'=>__('username'), 
										'placeholder'=>__('username'), 'class'=>'form-control', 
										"disabled"=>true])?>
								</div>
								<div class="form-group col-md-12 col-xs-12">
									<span class="from-input-ic"><i class="fa fa-user"></i></span>
									<?php echo $this->Form->input('password', 
										['type'=>'password', 'label'=>__('password'), "chk"=>"isEmpty", 
										'placeholder'=>__('password'), 'class'=>'form-control'])?>
								</div>
								<div class="form-group col-md-6 col-xs-12">
									<span class="from-input-ic"><i class="fa fa-bars"></i></span>
									<?php echo $this->Form->input('country_id', 
											['onClick'=>'getSubCat(0, "city-id")', "chk"=>"isSelectEmpty",
											'onChange'=>'getSubCat(this, "city-id")', 'type' => 'select', 
											'options' => @$this->Do->lcl( $countries ), 'empty' => __('country_id'), 
											'label'=>__('country_id'), 
											'class'=>'form-control'])?>
								</div>
								<div class="form-group col-md-6 col-xs-12">
									<span class="from-input-ic"><i class="fa fa-bars"></i></span>
									<?php echo $this->Form->input('city_id', 
											['type' => 'select', "chk"=>"isSelectEmpty", 
											'options' => @$cities, 'label'=>__('city_id'), 
											'class'=>'form-control'])?>
								</div>
								<div class="form-group col-md-12 col-xs-12">
									<span class="from-input-ic"><i class="fa fa-user"></i></span>
									<?php echo $this->Form->input('phone', 
										['type'=>'tel', 'label'=>__('phone'), "chk"=>"isPhone", 
										'placeholder'=>__('phone'), 'class'=>'form-control'])?>
								</div>
								<div class="form-group col-md-12 col-xs-12">
									<label><?=__('user_photo')?></label>
									<?php echo $this->element('uploadForm', ['name'=>'photo', 'photosNumber'=>1, 
											'oldPhotos'=>$user->photo, "recId"=>$user->id,
											"msg"=>"upload_user_photos_msg"]);?>
								</div>
								<div class="form-group col-md-12 col-xs-12">
									<button class="btn pull-right" type="submit"><?php echo __('Submit')?></button>
								</div>
							</div>
							<br />
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
