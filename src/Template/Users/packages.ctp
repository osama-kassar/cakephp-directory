<div id="breadcrum-inner-block">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 text-center">
				<div class="breadcrum-inner-header">
					<h1>
						<?=__('dashboard')?>
					</h1>
					<?=$this->Html->link('home', ['/'])?>
					<i class="fa fa-circle"></i>
					<?=$this->Html->link('dashboard', ['/dashboard'])?>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="pricing-item-block" class="white_bg_block">
	<div class="container">
		<div class="row">
			<?=$this->element('user_sidebar')?>
			<div class="col-sm-9">
				<?php echo $this->element('packages_elm');?>
			</div>
		</div>
	</div>
</div>
