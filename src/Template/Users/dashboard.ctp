<div id="breadcrum-inner-block">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 text-center">
				<div class="breadcrum-inner-header">
					<h1>
						<?=__('dashboard')?>
					</h1>
					<?=$this->Html->link('home', ['/'])?>
					<i class="fa fa-circle"></i>
					<?=$this->Html->link('dashboard', ['/dashboard'])?>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="pricing-item-block" class="white_bg_block">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center">
				<div class="col-md-12 all-categorie-list-title bt_heading_3">
					<h1>
						<?=__('dashboard')?>
					</h1>
					<div class="blind line_1"></div>
					<div class="flipInX-1 blind icon"><span class="icon"><i class="fa fa-stop"></i>&nbsp;&nbsp;<i class="fa fa-stop"></i></span></div>
					<div class="blind line_2"></div>
				</div>
				<div class="row">
				
					<!-- SIDEBAR -->
					<?=$this->element('user_sidebar')?>
					
					
					<!-- BODY -->
					<div class="col-md-9 col-sm-8 col-xs-12">
						<div id="dashboard_listing_blcok">
							<div class="col-md-9 col-sm-6 col-xs-12">
								<div class="statusbox">
									<h3>
                                        <?php 
                                            if($user['company'] == '' ){
                                                echo $this->Html->link(__('add_company'), 
										          ["controller"=>"Companies", "action"=>"add"] );
                                            }else{
                                                echo $this->Html->link(__('mycompany'), 
										          ["controller"=>"Companies", "action"=>"view", $user['company']['slug'] ] );
                                            }
                                        ?>
									</h3>
									<div class="statusbox-content">
										<p class="ic_status_item ic_col_one"><i class="fa fa-suitcase"></i></p>
										<h2><?=$stats['company']->stat_views ?></h2>
										<span><?=__('stat_views')?></span>
									</div>
								</div>
							</div>
							<div class="col-md-3 col-sm-6 col-xs-12">
								<div class="statusbox">
									<h3>
                                        <?php 
                                            if($user['company'] == '' ){
                                                echo $this->Html->link(__('add_company'), 
										          ["controller"=>"Companies", "action"=>"add"] );
                                            }else{
                                                echo $this->Html->link(__('mycompany'), 
										          ["controller"=>"Companies", "action"=>"view", $user['company']['slug'] ] );
                                            }
                                        ?>
									</h3>
									<div class="statusbox-content">
										<p class="ic_status_item ic_col_one"><i class="fa fa-share-alt"></i></p>
										<h2><?=$stats['company']->stat_shares ?></h2>
										<span><?=__('stat_shares')?></span>
									</div>
								</div>
							</div>
							<div class="col-md-3 col-sm-6 col-xs-12">
								<div class="statusbox">
									<h3>
										<?=$this->Html->link(__('myrequests'), 
										['controller'=>'Requests', 'action'=>'mylist'])?>
									</h3>
									<div class="statusbox-content">
										<p class="ic_status_item ic_col_one"><i class="fa fa-eye"></i></p>
										<h2><?=$stats['req_views'] ?></h2>
										<span><?=__('stat_views')?></span>
									</div>
								</div>
							</div>
							<div class="col-md-3 col-sm-6 col-xs-12">
								<div class="statusbox">
									<h3>
										<?=$this->Html->link(__('myrequests'), 
										['controller'=>'Requests', 'action'=>'mylist'])?>
									</h3>
									<div class="statusbox-content">
										<p class="ic_status_item ic_col_one"><i class="fa fa-share"></i></p>
										<h2><?=$stats['req_shares'] ?></h2>
										<span><?=__('stat_shares')?></span>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-sm-6 col-xs-12">
								<div class="statusbox">
									<h3>
										<?=$this->Html->link(__('myrequests'), 
										['controller'=>'Requests', 'action'=>'mylist'])?>
									</h3>
									<div class="statusbox-content">
										<p class="ic_status_item ic_col_one"><i class="fa fa-bars"></i></p>
										<h2><?=$stats['req_total'] ?></h2>
										<span><?=__('stat_total')?></span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
