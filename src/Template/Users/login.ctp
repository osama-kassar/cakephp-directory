<div>
	<div>
	<?= $this->Flash->render('auth') ?>
        <?= $this->Form->create(null, ['novalidate']) ?>
        <fieldset>
            <legend><h2><?= __('login') ?></h2></legend>
            <?= $this->Form->input('username', ["ng-model"=>"userdt.username"]) ?>
            <?= $this->Form->input('password', ["ng-model"=>"userdt.password"])?>
            <?php //echo $this->Form->input('doLogin', ['type'=>'checkbox', 'label'=>__('dologin')]) ?>
            <br />
            
        <?= $this->Form->button(__('login'), ['id'=>'btn']); ?>
        <?= $this->Form->end() ?>
        </fieldset>
    </div>
    
    <script>
	$('#flash').classList.add('animated', 'fadeOutUp');
	</script>
    
    <div>
    <?=$this->Html->link('<i class="fa fa-user-plus" aria-hidden="true"></i> '.
            __('register'), ['controller'=>'users', 'action'=>'register'], 
            ['title'=>__('register'), 'escape'=>false, 'class'=>'pageContent']);?>  
    <?= $this->Html->link('<i class="fa fa-key" aria-hidden="true"></i> <span>'.
            __('getpassword').'</span>', ['controller'=>'users', 'action'=>'getpassword'], 
            ['title'=>__('getpassword'), 'escape'=>false, 'class'=>'pageContent']);?>
    </div>
</div>