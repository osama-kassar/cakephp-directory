<?php

namespace App\Model\Behavior;

use Cake\ORM\Behavior;
use Cake\Event\Event;
use Cake\ORM\Entity;
use Cake\Datasource\ConnectionManager;
use Cake\Routing\Router;

class IncBehavior extends Behavior {
	
	public function beforeFind($event, $entity) {
		$params = Router::getRequest()->params;
		if($params['action'] == 'view' && empty($params['prefix'])){
			$tbl_name = explode('\\', $event->subject()->entityClass());
			$tbl = strtolower( $event->subject()->alias() );
			
			$q = "UPDATE $tbl SET stat_views = stat_views + 1 WHERE slug = '".$params['slug']."'";
			$connection = ConnectionManager::get('default');
			return $connection->execute($q);
		}
    }
}