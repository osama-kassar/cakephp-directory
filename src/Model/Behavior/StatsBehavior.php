<?php

namespace App\Model\Behavior;

use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\Behavior;
use Cake\Datasource\ConnectionManager;

class StatsBehavior extends Behavior {
   
	public function setup($model, $config = array()) {
        $this->settings[$model->alias] = array_merge($this->_defaults, (array) $config);
    }
	
    public function afterSave($model){
		if(@$_SESSION['Auth']['User']['id'] !== null){
			$dt = [
				$uid = $_SESSION['Auth']['User']['id'],//AuthComponent::User('id'),$this->Auth->User('id'),
				$url = '"'.$_SERVER['HTTP_REFERER'].'"',
				$uip = '"'.$_SERVER['REMOTE_ADDR'].'"',
				$machineinfo = '"'.implode('|',get_browser(null, true)).'"',
				$adate = '"'.date("Y-m-d H:i:s").'"',
			];
			$action_q = "INSERT INTO `dbstats`(`user_id`, `action_url`, `action_ip`, `action_machineinfo`, `action_date`) VALUES (".implode(", ", $dt).")";
			$connection = ConnectionManager::get('default');
			return $connection->execute($action_q);
		}
	}
}