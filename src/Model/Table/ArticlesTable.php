<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class ArticlesTable extends Table{

    public function initialize(array $config){
        parent::initialize($config);

        $this->table('articles');
        $this->displayField('article_title');
        $this->primaryKey('id');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Categories', [
            'foreignKey' => 'category_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('Comments', [
            'foreignKey' => 'article_id'
        ]);
		
        $this->belongsTo('Categories', [
			'className'=>'Categories',
            'foreignKey' => 'category_id',
            'joinType' => 'INNER',
			'propertyName'=>'category',
			'setCondition'=>['id'=>'category_id']
        ]);
		
        $this->belongsTo('SubCategories', [
			'className'=>'Categories',
            'foreignKey' => 'subcategory_id',
            'joinType' => 'INNER',
			'propertyName'=>'subcategory',
			'setCondition'=>['id'=>'subcategory_id']
        ]);
		
		$this->addBehavior('Inc');
    }

    public function validationDefault(Validator $validator){
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

		$validator
            ->notEmpty('slug', __('error_empty'))
			->add('slug', [
			 'unique' => [
				 'message'   => __('not_unique_slug'),
				 'provider'  => 'table',
				 'rule'      => 'validateUnique'
			 ]
		 ]);
        
        $validator
            ->requirePresence('article_title', 'create')
            ->notEmpty('article_title', __('error_empty'));

        $validator
            ->requirePresence('article_body', 'create')
            ->notEmpty('article_body', __('error_empty'));

        $validator
            ->requirePresence('article_author', 'create')
            ->notEmpty('article_author', __('error_empty'));

        $validator
            ->requirePresence('seo_desc', 'create')
            ->notEmpty('seo_desc', __('error_empty'));

        $validator
            ->requirePresence('seo_keywords', 'create')
            ->notEmpty('seo_keywords', __('error_empty'));

        return $validator;
    }

    public function buildRules(RulesChecker $rules){
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['category_id'], 'Categories'));

        return $rules;
    }
}
