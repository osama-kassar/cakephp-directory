<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class CommentsTable extends Table{
	
    public function initialize(array $config){
        parent::initialize($config);

        $this->table('comments');
        $this->displayField('comment_name');
        $this->primaryKey('id');

        $this->belongsTo('Articles', [
            'foreignKey' => 'article_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('ParentComments', [
            'className' => 'Comments',
            'conditions' => ['comment_id' => 0]
        ]);
        $this->hasMany('ChildComments', [
            'className' => 'Comments'
        ]);
		//$this->addBehavior('Select');
    }
	
    public function validationDefault(Validator $validator){
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('comment_name', 'create')
            ->notEmpty('comment_name', __('error_empty'));

        $validator
            ->requirePresence('comment_email', 'create')
			->add('comment_email', 'valid', ['rule' => 'email', 'message' => __('incorrect_email')])
            ->notEmpty('comment_email', __('error_empty'));

        $validator
            ->requirePresence('comment_body', 'create')
            ->notEmpty('comment_body', __('error_empty'));

        /*$validator
            ->requirePresence('CaptchaCode', 'create')
            ->notEmpty('CaptchaCode', __('error_empty'));*/

        return $validator;
    }

    public function buildRules(RulesChecker $rules){
        //$rules->add($rules->existsIn(['article_id'], 'Articles'));
        //$rules->add($rules->existsIn(['comment_id'], 'Comments'));

        return $rules;
    }
}
