<?php

namespace App\Model\Table;


use App\Model\Entity\User;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

use Cake\Auth\DefaultPasswordHasher;

class UsersTable extends Table{
	
	public $validationDomain = "aaa";
	
	public function initialize(array $config){
		parent::initialize($config);
		
		$this->table('users');
		$this->displayField('firstname');
		$this->primaryKey('id');
		
        $this->belongsTo('Countries', [
			'className'=>'Categories',
            'foreignKey' => 'country_id',
            'joinType' => 'INNER',
			'propertyName'=>'country'
        ]);
		
        $this->belongsTo('Cities', [
			'className'=>'Categories',
            'foreignKey' => 'city_id',
            'joinType' => 'INNER',
			'propertyName'=>'city',
			'setCondition'=>['id'=>'city_id']
        ]);
		
        $this->hasOne('Companies', [
            'foreignKey' => 'user_id',
        ]);

    }
	
	public function validationDefault(Validator $validator){
		/*$validator
		 ->notEmpty('username', __('error_empty'))
		 ->notEmpty('password', __('error_empty'))
		 ->notEmpty('confirm_password', __('error_empty'))
		 ->notEmpty('old_password', __('error_empty'))
		 ->notEmpty('new_password', __('error_empty'))
		 ->notEmpty('confirm_new_password', __('error_empty'))
		 ->notEmpty('role', __('error_empty'));*/
		 
		$validator
			->requirePresence('firstname', 'create')
			->notEmpty('firstname', __('error_empty'));
			
		$validator
			->requirePresence('lastname', 'create')
			->notEmpty('lastname', __('error_empty'));
			
		$validator
			->requirePresence('password', 'create')
			->notEmpty('password', __('error_empty'));
			
		$validator
			->requirePresence('phone', 'create')
			->add('phone', 'valid', ['rule' => 'numeric', 'message' => __('incorrect_phone')])
			->notEmpty('phone', __('error_empty'));
			
		$validator
			->requirePresence('username', 'create')
			->add('username', 'valid', ['rule' => 'email', 'message' => __('incorrect_email')])
			->notEmpty('username', __('error_empty'));
			
		$validator
			->requirePresence('username', 'create')
			->notEmpty('username', __('error_empty'));
			
		$validator
			->add('username', [
			 'unique' => [
				 'message'   => __('not_unique_username'),
				 'provider'  => 'table',
				 'rule'      => 'validateUnique'
			 ]
		 ]);
        
//		$validator
//			->requirePresence('country_id', 'create')
//			->notEmpty('country_id', __('error_empty'));
//			
//		$validator
//			->requirePresence('city_id', 'create')
//			->notEmpty('city_id', __('error_empty'));
			
			

		$validator->add('username', [
			 'unique' => [
				 'message'   => __d('aaa', 'not_unique_email'),
				 'provider'  => 'table',
				 'rule'      => 'validateUnique'
			 ]
 		]);
		
		/*$validator
			->notEmpty('confirm_email', __('error_empty'));
			
		$validator
			->add('confirm_email', [
				'compare' => [
					'rule' => ['compareWith', 'email'],
					'message'   => __('not_match_email')
					]
				]);

		$validator
			->add('username', [
				'minLength' => [
					'rule' => ['minLength', 5],
					'last' => true,
					'message' => __('too_short')
				],
			]);

		$validator
			->add('username', [
			 'unique' => [
				 'message'   => __('not_unique_username'),
				 'provider'  => 'table',
				 'rule'      => 'validateUnique'
			 ]
		 ]);*/
		 
		$validator
			->add('password', [
				'minLength' => [
					'rule' => ['minLength', 5],
					'last' => true,
					'message' => __('too_short')
				],
			]);

		/*$validator
			->add('confirm_password', [
			'compare' => [
				'rule' => ['compareWith', 'password'],
			 	'message'   => __('not_match_password')
			]
		]);*/

		$validator
			 ->add('old_password','custom',[
				 'rule'=>  function($value, $context){
					 $user = $this->get($context['data']['id']);
					 if ($user) {
						 if ((new DefaultPasswordHasher)->check($value, $user->password)) {
							 return true;
						 }
					 }
					 return false;
				 },
				 'message'=>__('old_password_not_correct'),
			 ])
			 ->notEmpty('old_password');
			 
		$validator
			->add('new_password', [
				'minLength' => [
					'rule' => ['minLength', 5],
					'last' => true,
					'message' => __('too_short')
				],
			]);
			
		$validator
			->add('confirm_new_password', [
			'compare' => [
				'rule' => ['compareWith', 'new_password'],
         		'message'   => __('not_match_password')
			]
		]);
 		return $validator;

    }
	
    public function buildRules(RulesChecker $rules){
		//$rules->add($rules->existsIn(['Companies'], 'Users'));
		return $rules;
    }
}