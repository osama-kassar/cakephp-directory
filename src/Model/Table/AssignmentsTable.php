<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class AssignmentsTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('assignments');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('assign_target')
            ->maxLength('assign_target', 255)
            ->requirePresence('assign_target', 'create')
            ->notEmptyString('assign_target');

        $validator
            ->scalar('assign_title')
            ->maxLength('assign_title', 255)
            ->requirePresence('assign_title', 'create')
            ->notEmptyString('assign_title');

        $validator
            ->scalar('assign_notes')
            ->maxLength('assign_notes', 4294967295)
            ->requirePresence('assign_notes', 'create')
            ->notEmptyString('assign_notes');

        $validator
            ->dateTime('stat_created')
            ->requirePresence('stat_created', 'create')
            ->notEmptyDateTime('stat_created');

        $validator
            ->dateTime('stat_delivered')
            ->requirePresence('stat_delivered', 'create')
            ->notEmptyDateTime('stat_delivered');

        $validator
            ->requirePresence('status', 'create')
            ->notEmptyString('status');

        return $validator;
    }

    public function buildRules(RulesChecker $rules)
    {
        //$rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }
}
