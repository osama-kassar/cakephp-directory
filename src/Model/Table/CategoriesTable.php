<?php
namespace App\Model\Table;

use App\Model\Entity\Category;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class CategoriesTable extends Table{
		
    public function initialize(array $config){
        parent::initialize($config);
		
        $this->table('categories');
        $this->displayField('category_name');
        $this->primaryKey('id');

        $this->belongsTo('ParentCategories', [
            'className' => 'Categories',
            'foreignKey' => 'parent_id',
			'dependent' => true,
			'cascadeCallbacks' => true,
        ]);
        $this->hasMany('ChildCategories', [
            'className' => 'Categories',
            'foreignKey' => 'parent_id',
			'dependent' => true,
			'cascadeCallbacks' => true,
        ]);	
		
    }
	
    public function validationDefault(Validator $validator){
        $validator
            ->requirePresence('category_name', 'create')
            ->notEmpty('category_name', __('error_empty'));

        return $validator;
    }
	
    public function buildRules(RulesChecker $rules){
        //$rules->add($rules->existsIn(['parent_id'], 'ParentCategories'));
        //$rules->add($rules->existsIn(['parent_id'], 'ChildCategories'));
        return $rules;
    }
}
