<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class RequestsTable extends Table{

    public function initialize(array $config){
        parent::initialize($config);

        $this->setTable('requests');
        $this->setDisplayField('req_title');
        $this->setPrimaryKey('id');


        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
       
		
        $this->belongsTo('Countries', [
			'className'=>'Categories',
            'foreignKey' => 'country_id',
            'joinType' => 'INNER',
			'propertyName'=>'country'
        ]);
		
        $this->belongsTo('Cities', [
			'className'=>'Categories',
            'foreignKey' => 'city_id',
            'joinType' => 'INNER',
			'propertyName'=>'city',
			'setCondition'=>['id'=>'city_id']
        ]);
		
        $this->belongsTo('Categories', [
			'className'=>'Categories',
            'foreignKey' => 'category_id',
            'joinType' => 'INNER',
			'propertyName'=>'category',
			'setCondition'=>['id'=>'category_id']
        ]);
		
        $this->belongsTo('SubCategories', [
			'className'=>'Categories',
            'foreignKey' => 'subcategory_id',
            'joinType' => 'INNER',
			'propertyName'=>'subcategory',
			'setCondition'=>['id'=>'subcategory_id']
        ]);
		$this->addBehavior('Inc');
		$this->addBehavior('Select');
    }
	
    public function validationDefault(Validator $validator){
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

		$validator
            ->notEmpty('slug', __('error_empty'))
			->add('slug', [
			 'unique' => [
				 'message'   => __('not_unique_slug'),
				 'provider'  => 'table',
				 'rule'      => 'validateUnique'
			 ]
		 ]);
        
        $validator
            ->requirePresence('req_title', 'create')
            ->notEmpty('req_title', __('error_empty'));

        $validator
            ->requirePresence('req_desc', 'create')
            ->notEmpty('req_desc', __('error_empty'));

        $validator
            ->requirePresence('category_id', 'create')
            ->notEmpty('category_id', __('error_empty'));

        $validator
            ->requirePresence('subcategory_id', 'create')
            ->notEmpty('subcategory_id', __('error_empty'));

        $validator
            ->requirePresence('country_id', 'create')
            ->notEmpty('country_id', __('error_empty'));

        $validator
            ->requirePresence('city_id', 'create')
            ->notEmpty('city_id', __('error_empty'));

        $validator
            ->requirePresence('req_preferred_langs', 'create')
            ->notEmpty('req_preferred_langs', __('error_empty'));

        $validator
            ->requirePresence('req_preferred_contacts', 'create')
            ->notEmpty('req_preferred_contacts', __('error_empty'));

        return $validator;
    }
	
    public function buildRules(RulesChecker $rules){
        //$rules->add($rules->existsIn(['user_id'], 'Users'));
        //$rules->add($rules->existsIn(['category_id'], 'Categories'));
        //$rules->add($rules->existsIn(['subcategory_id'], 'Categories'));

        return $rules;
    }
}
