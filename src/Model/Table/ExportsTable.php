<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class ExportsTable extends Table{

    public function initialize(array $config)   {
        parent::initialize($config);

        $this->setTable('exports');
        $this->setDisplayField('exp_name');
        $this->setPrimaryKey('id');

        $this->belongsTo('Companies', [
            'foreignKey' => 'company_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Categories', [
            'foreignKey' => 'category_id',
            'joinType' => 'INNER'
        ]);
        /*$this->belongsTo('Subcategories', [
            'foreignKey' => 'subcategory_id',
            'joinType' => 'INNER'
        ]);*/
        /*$this->belongsTo('Countries', [
            'foreignKey' => 'country_id',
            'joinType' => 'INNER'
        ]);*/
    }
    
    public function validationDefault(Validator $validator){
        $validator
            ->requirePresence('exp_name', 'create')
            ->notEmpty('exp_name');

        $validator
            ->integer('exp_price')
            ->requirePresence('exp_price', 'create')
            ->notEmpty('exp_price');

        $validator
            ->integer('exp_phone')
            ->requirePresence('exp_phone', 'create')
            ->notEmpty('exp_phone');

        $validator
            ->requirePresence('exp_desc', 'create')
            ->notEmpty('exp_desc');

        /*$validator
            ->requirePresence('exp_photos', 'create')
            ->notEmpty('exp_photos');*/

        $validator
            ->requirePresence('seo_keywords', 'create')
            ->notEmpty('seo_keywords');

        $validator
            ->requirePresence('seo_desc', 'create')
            ->notEmpty('seo_desc');

        return $validator;
    }
    public function buildRules(RulesChecker $rules){
        $rules->add($rules->existsIn(['company_id'], 'Companies'));
        $rules->add($rules->existsIn(['category_id'], 'Categories'));
        //$rules->add($rules->existsIn(['subcategory_id'], 'Subcategories'));
        //$rules->add($rules->existsIn(['country_id'], 'Countries'));

        return $rules;
    }
}
