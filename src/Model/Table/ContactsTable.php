<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class ContactsTable extends Table{

    public function initialize(array $config){
        parent::initialize($config);

        $this->table('contacts');
        $this->displayField('contact_name');
        $this->primaryKey('id');

    }
	
    public function validationDefault(Validator $validator){
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('contact_name', 'create')
            ->notEmpty('contact_name');

        $validator
            ->requirePresence('contact_phone', 'create')
            ->notEmpty('contact_phone', __('error_empty'));

        $validator
            ->requirePresence('contact_email', 'create')
			->add('email', 'valid', ['rule' => 'email', 'message' => __('incorrect_email')])
            ->notEmpty('contact_email', __('error_empty'));

        /*$validator
            ->requirePresence('contact_subject', 'create')
            ->notEmpty('contact_subject');*/

        $validator
            ->requirePresence('contact_message', 'create')
            ->notEmpty('contact_message', __('error_empty'));

        $validator
            ->integer('status')
            ->requirePresence('status', 'create')
            ->notEmpty('status');

        return $validator;
    }
    
    public function buildRules(RulesChecker $rules){

        return $rules;
    }
}
