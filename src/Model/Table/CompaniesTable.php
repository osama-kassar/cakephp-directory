<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class CompaniesTable extends Table{

    public function initialize(array $config){
        parent::initialize($config);

        $this->setTable('companies');
        $this->setDisplayField('company_name');
        $this->setPrimaryKey('id');
        
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
		
        $this->belongsTo('Countries', [
			'className'=>'Categories',
            'foreignKey' => 'country_id',
            'joinType' => 'INNER',
			'propertyName'=>'country'
        ]);
		
        $this->belongsTo('Cities', [
			'className'=>'Categories',
            'foreignKey' => 'city_id',
            'joinType' => 'INNER',
			'propertyName'=>'city',
			'setCondition'=>['id'=>'city_id']
        ]);
		
        $this->belongsTo('Categories', [
			'className'=>'Categories',
            'foreignKey' => 'category_id',
            'joinType' => 'INNER',
			'propertyName'=>'category',
			'setCondition'=>['id'=>'category_id']
        ]);
		
        $this->belongsTo('SubCategories', [
			'className'=>'Categories',
            'foreignKey' => 'subcategory_id',
            'joinType' => 'INNER',
			'propertyName'=>'subcategory',
			'setCondition'=>['id'=>'subcategory_id']
        ]);
		$this->addBehavior('Inc');
		$this->addBehavior('Select');
    }
    public function validationDefault(Validator $validator){
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');
			
		$validator
            ->notEmpty('slug', __('error_empty'))
			->add('slug', [
			 'unique' => [
				 'message'   => __('not_unique_slug'),
				 'provider'  => 'table',
				 'rule'      => 'validateUnique'
			 ]
		 ]);
        
		$validator
			->requirePresence('language_id', 'create')
			->notEmpty('language_id', __('error_empty'));
			
		$validator
			->requirePresence('category_id', 'create')
			->notEmpty('category_id', __('error_empty'));
			
		$validator
			->requirePresence('subcategory_id', 'create')
			->notEmpty('subcategory_id', __('error_empty'));
			
		$validator
			->requirePresence('country_id', 'create')
			->notEmpty('country_id', __('error_empty'));
			
		$validator
			->requirePresence('city_id', 'create')
			->notEmpty('city_id', __('error_empty'));
			
		$validator
			->requirePresence('company_name', 'create')
			->notEmpty('company_name', __('error_empty'));
			
		$validator
			->requirePresence('company_desc', 'create')
			->notEmpty('company_desc', __('error_empty'));
			
		$validator
			->requirePresence('company_phone', 'create')
			->notEmpty('company_phone', __('error_empty'));
			
		$validator
			->requirePresence('company_email', 'create')
			->add('email', 'valid', ['rule' => 'email', 'message' => __('incorrect_email')])
			->notEmpty('company_email', __('error_empty'));
			
		$validator
			->requirePresence('company_address', 'create')
			->notEmpty('company_address', __('error_empty'));
			
		$validator
			->requirePresence('company_location', 'create')
			->notEmpty('company_location', __('error_empty'));
			
		$validator
			->requirePresence('company_logo', 'create')
			->notEmpty('company_logo', __('error_empty'));

        return $validator;
    }

    public function buildRules(RulesChecker $rules){
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        //$rules->add($rules->existsIn(['category_id'], 'Categories'));
        //$rules->add($rules->existsIn(['subcategory_id'], 'ChildCategories'));

        return $rules;
    }
}
