<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

class Assignment extends Entity{
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
