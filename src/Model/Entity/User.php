<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\Auth\DefaultPasswordHasher;

class User extends Entity{
	public $iagree;
	public $country_id_reg;
	public $city_id_reg;
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
    
    protected function _setPassword($password){
        return (new DefaultPasswordHasher)->hash($password);
    }
}