<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Export Entity
 *
 * @property int $id
 * @property int $company_id
 * @property int $category_id
 * @property int $subcategory_id
 * @property string $exp_name
 * @property int $exp_price
 * @property string $exp_method
 * @property string $exp_deliver
 * @property int $exp_phone
 * @property int $country_id
 * @property string $exp_desc
 * @property string $exp_photos
 * @property \Cake\I18n\Time $exp_date
 * @property int $stat_views
 * @property int $stat_saved
 * @property int $stat_loved
 * @property int $stat_shared
 * @property string $seo_keywords
 * @property string $seo_desc
 * @property int $status
 *
 * @property \App\Model\Entity\Company $company
 * @property \App\Model\Entity\Category $category
 * @property \App\Model\Entity\Subcategory $subcategory
 * @property \App\Model\Entity\Country $country
 */
class Export extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
