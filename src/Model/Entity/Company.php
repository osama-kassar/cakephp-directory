<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Company Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int $category_id
 * @property int $subcategory_id
 * @property string $company_name
 * @property string $company_desc
 * @property string $company_phone
 * @property string $company_email
 * @property int $company_country
 * @property int $company_city
 * @property string $company_address
 * @property string $company_logo
 * @property string $company_photos
 * @property string $company_links
 * @property string $seo_desc
 * @property string $seo_keywords
 * @property int $stat_views
 * @property int $status
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Category $category
 * @property \App\Model\Entity\Subcategory $subcategory
 */
class Company extends Entity
{
    
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
