<?php
namespace App\Controller;

use App\Controller\AppController;

class ServicesController extends AppController{

	public function index(){
        $this->paginate = [
            'contain' => ['Companies', 'Categories']
        ];
        $services = $this->paginate($this->Services);

        $this->set(compact('services'));
        $this->set('_serialize', ['services']);
    }

    public function view($id = null){
        $service = $this->Services->get($id, [
            'contain' => ['Companies', 'Categories']
        ]);

        $this->set('service', $service);
        $this->set('_serialize', ['service']);
    }

    public function add(){
        $service = $this->Services->newEntity();
        if ($this->request->is('post')) {
            $service = $this->Services->patchEntity($service, $this->request->getData());
		  $service->service_date = date('Y-m-d H:m:s');
		  $service->company_id = $this->Auth->User('company.id');
		  $service->service_price = ($service->service_price == NULL) ? 0 : $service->service_price;

            if ($this->Services->save($service)) {
                $this->Flash->success(__('The service has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The service could not be saved. Please, try again.'));
		  //debug($service);
        }
        $categories = $this->Services->Categories->find('list', ['conditions' => ['parent_id'=>8] ] );
	   //$service_method = $this->Do->get('service_method');
	   //$service_deliver = $this->Do->get('service_deliver');
	   $service_currency = $this->Do->get('exp_currency');
	   $service_links = $this->Do->get('exp_links');
        $this->set(compact('service', 'categories', 'countries', 'service_method', 'service_deliver', 'service_currency', 'service_links'));
        $this->set('_serialize', ['service']);
    }

    public function edit($id = null){
        $service = $this->Services->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $service = $this->Services->patchEntity($service, $this->request->getData());
            if ($this->Services->save($service)) {
                $this->Flash->success(__('The service has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The service could not be saved. Please, try again.'));
        }
        $companies = $this->Services->Companies->find('list', ['limit' => 200]);
        $categories = $this->Services->Categories->find('list', ['limit' => 200]);
        $subcategories = $this->Services->Subcategories->find('list', ['limit' => 200]);
        $this->set(compact('service', 'companies', 'categories', 'subcategories', 'countries'));
        $this->set('_serialize', ['service']);
    }

    public function delete($id = null){
        $this->request->allowMethod(['post', 'delete']);
        $service = $this->Services->get($id);
        if ($this->Services->delete($service)) {
            $this->Flash->success(__('The service has been deleted.'));
        } else {
            $this->Flash->error(__('The service could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
