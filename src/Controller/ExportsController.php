<?php
namespace App\Controller;

use App\Controller\AppController;

class ExportsController extends AppController{

	public function index(){
        $this->paginate = [
            'contain' => ['Companies', 'Categories']
        ];
        $exports = $this->paginate($this->Exports);

        $this->set(compact('exports'));
        $this->set('_serialize', ['exports']);
    }

    public function view($id = null){
        $export = $this->Exports->get($id, [
            'contain' => ['Companies', 'Categories']
        ]);

        $this->set('export', $export);
        $this->set('_serialize', ['export']);
    }

    public function add(){
        $export = $this->Exports->newEntity();
        if ($this->request->is('post')) {
            $export = $this->Exports->patchEntity($export, $this->request->getData());
		  $export->exp_date = date('Y-m-d H:m:s');
		  $export->company_id = $this->Auth->User('company.id');
		  $export->exp_method = implode(',', $this->request->getData('exp_method'));
		  $export->exp_deliver = implode(',', $this->request->getData('exp_deliver'));
		  $export->exp_price = ($export->exp_price == NULL) ? 0 : $export->exp_price;

            if ($this->Exports->save($export)) {
                $this->Flash->success(__('The export has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The export could not be saved. Please, try again.'));
		  //debug($export);
        }
        $categories = $this->Exports->Categories->find('list', ['conditions' => ['parent_id'=>8] ] );
	   $exp_method = $this->Do->get('exp_method');
	   $exp_deliver = $this->Do->get('exp_deliver');
	   $exp_currency = $this->Do->get('exp_currency');
	   $exp_links = $this->Do->get('exp_links');
        $this->set(compact('export', 'categories', 'countries', 'exp_method', 'exp_deliver', 'exp_currency', 'exp_links'));
        $this->set('_serialize', ['export']);
    }

    public function edit($id = null){
        $export = $this->Exports->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $export = $this->Exports->patchEntity($export, $this->request->getData());
            if ($this->Exports->save($export)) {
                $this->Flash->success(__('The export has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The export could not be saved. Please, try again.'));
        }
        $companies = $this->Exports->Companies->find('list', ['limit' => 200]);
        $categories = $this->Exports->Categories->find('list', ['limit' => 200]);
        $subcategories = $this->Exports->Subcategories->find('list', ['limit' => 200]);
        $this->set(compact('export', 'companies', 'categories', 'subcategories', 'countries'));
        $this->set('_serialize', ['export']);
    }

    public function delete($id = null){
        $this->request->allowMethod(['post', 'delete']);
        $export = $this->Exports->get($id);
        if ($this->Exports->delete($export)) {
            $this->Flash->success(__('The export has been deleted.'));
        } else {
            $this->Flash->error(__('The export could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
