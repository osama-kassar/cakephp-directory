<?php
namespace App\Controller;

use App\Controller\AppController;


class ArticlesController extends AppController{

    public function index(){
		
		if(@$_GET['ajax'] == 1){
			$this->autoRender = false;
		}
		
		$cond=[];
		$this->request->session()->write('turkeyde.companies_filters', null);
		if(!empty( $_GET['keyword'] )){
			$cond['OR'] = ['article_body LIKE'=>"%".$_GET['keyword']."%", 'article_title LIKE'=> "%".$_GET['keyword']."%"];
			$this->request->session()->write('turkeyde.articles_filters.keyword', $_GET['keyword']);
		}
		if(@$_GET['ajax'] == 1){
			$articles = $this->Articles->find("all", ["conditions"=>$cond, 
				"fields"=>["slug", "article_title", "article_photo"],
				"limit"=>8])->toArray();
			echo  json_encode($articles); die();
		}
		if(!empty( $_GET['category'] )){
			$cond['category_id'] = $_GET['category'];
			$this->request->session()->write('turkeyde.articles_filters.category', $_GET['category']);
		}
		if(!empty( $_GET['subcategory'] )){
			$cond['subcategory_id'] = $_GET['subcategory'];
			$this->request->session()->write('turkeyde.articles_filters.subcategory', $_GET['subcategory']);
		}
		
		$articles = $this->paginate($this->Articles, [
			'contain'=>[
				'Categories'
				],
			'limit'=>12,
			'order'=>['id'=>'DESC'],
			'conditions'=>$cond
		]);
        $categories = $this->Articles->Categories->find('all', ['conditions' => ['parent_id'=>2]])->toList();
		$total = $this->Articles->find('all')->count();
		$this->set(compact('articles', 'categories', 'total'));
		$this->set('_serialize', ['articles']);
    }
	
    public function view($slug = null){
        $article = $this->Articles->findBySlug($slug)
			->contain(['Categories'])->first();
        $related = $this->Articles->find("all",[
			"conditions"=>["category_id"=>$article->category->id],
			"contain"=>["Categories"],
			"limit"=>6
		])->toArray();
		$keywords = $article->seo_keywords;
		$description = $article->seo_desc;
        $this->set( compact('article', 'keywords' ,'description', 'related') );
        $this->set('_serialize', ['article']);
	}
}
