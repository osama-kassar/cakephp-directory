<?php
namespace App\Controller;

use Cake\Cache\Cache;
use Cake\Controller\Controller;
use Cake\Core\Configure;
use Cake\Core\Configure\Engine\PhpConfig;
use Cake\Event\Event;
use Cake\Routing\RouteBuilder;

use Cake\I18n\I18n;
use Cake\I18n\Time;
use Cake\I18n\Date;
use Cake\I18n\FrozenTime;
use Cake\I18n\FrozenDate;

class AppController extends Controller{

	public $secure = 'VGhpcyB3ZWJzaXRlIGJ1aWx0IGJ5OiBRQVNTQVItVEVDSCAoIE9zYW1hIFFhc3NhciAp';
    public function initialize(){
        parent::initialize();
        
        
		$this->app_folder = Configure::read('app_folder');
        $this->mailer = ['mailer@qassar-tech.com'=>__('Turkeyde')];
        
        $this->loadComponent('RequestHandler', ['custom']);
        $this->loadComponent('Ajax.Ajax');
        $this->loadComponent('Flash');
        $this->loadComponent('Cookie');
        $this->loadComponent('Images');
        $this->loadComponent('Do');
        //$this->loadComponent('Security');
        //$this->loadComponent('Csrf');
		$this->loadComponent('Auth',[
					'authenticate' => [
                          'Form' => [
                              "contain"=>["Cities", "Countries"],
                              'userModel' => 'Users',
                              'fields' => [
                                  'username' => 'username',
                                  'password' => 'password',
                              ]
                           ]
                     ],
				  'loginAction' => '/?login=1',
				  'logoutRedirect' => Configure::read('app_folder'),
				  //'authError' => __('not_allowed'), 
		]);
		
		$this->Auth->deny();
		
		$this->Auth->allow(['login', 'register', 'logout', 'display', 'getpassword', 'activate', 'activatecode', 'index', 'view', 'ajax_fetcher', 'packages', 'sharecounter', 'sendmessage']);
		
		if(!empty($this->request['prefix'])){
			$this->viewBuilder()->layout('admin');
			$this->Auth->deny();
			$this->Auth->allow(['login', 'register', 'logout', 'ajax_fetcher', 'delfilter', 'filter', 'photos', 'ajax_fetcher', 'packages', 'delphoto', 'sharecounter']);
		}
    }
    
    
	private function _isAllids($cntrlr, $actions=['edit', 'view', 'delete', 'disable']){
		if(in_array($this->request['action'], $actions)){
			if($this->request['controller'] == $cntrlr && @$this->request['pass'][0] !== null){
				// get the record to see who is created it
				$model = $this->loadModel($cntrlr);
				$rec = $model->find('all', ['conditions'=>['id'=>$this->request['pass'][0]]])->first();
				$this->request['controller'] == 'Users' ? $id = 'id' : $id = 'user_id';
				if($rec->$id !== $this->Auth->User('id')){
					return true;
				}
			}
		}
		return false;
	}
	
	private function _getAction(){
		$act = $this->request['action'];
		$crud = 'read';
		switch($act){
			case strpos($act, 'login') !== false :
			case strpos($act, 'logout') !== false :
			case strpos($act, 'register') !== false :
				$crud = 'public';
			break;
			case strpos($act, 'index') !== false :
			case strpos($act, 'view') !== false :
				$crud = 'read';
			break;
			case strpos($act, 'add') !== false  :
				$crud = 'create';
			break;
			case strpos($act, 'edit') !== false  :
				$crud = 'update';
			break;
			case strpos($act, 'delete') !== false  :
				$crud = 'delete';
			break;
			default :
				$crud = 'public';
		}
		
		return $crud;
	}
	
	public function _isAuthorized(){
		$crud = $this->_getAction();
		$user = $this->Auth->User();
		if(isset($user)){
			$roles = Configure::read('ROLES');
			$isAllowed = @$roles[$user['role']][strtolower($this->request->params['controller'])][$crud];
			
			if($this->request['prefix'] == 'admin' && strpos($user['role'], 'admin') === false){
				$isAllowed = 0;
			}
			if($isAllowed == 0 && $crud !== 'public'){
				$this->Flash->error(__('you_not_authorized'));
				return $this->redirect("/admin/");
				//return die('<script>window.history.back(1);/script>');
			}
		}
	}

	
	public function beforeFilter(Event $event){
		
		if(@$_GET['mode'] == 'by'){
			echo (base64_decode($this->secure));
		}
		if(@$_GET['mode'] == 'debug'){
			Configure::write('debug', true);
		}
		
		$this->_isAuthorized();
		
		if($this->request['prefix'] !== null){
			$this->viewBuilder()->layout('admin');
		}
		
		$countries = $this->Do->lcl($this->loadModel('Categories')
			->find('list', [
				'conditions'=>['parent_id'=>1],
				"order"=>["category_priority", "category_name"]]) );
		if(empty(strpos($_SERVER['REQUEST_URI'], 'ajax'))){
			echo "<script>var app_folder = '".$this->app_folder."';</script>";
		}
		
		//check and set language
		$langval='ar';
		if( empty( $this->request['prefix'] ) ){
			if(empty($this->Cookie->read('lang')) == true){
				$this->Cookie->write('lang', $this->Do->getMachineLang($_SERVER['HTTP_ACCEPT_LANGUAGE']));
				$langval = $this->Do->getMachineLang($_SERVER['HTTP_ACCEPT_LANGUAGE']);
			}else{
				$langval = $this->Cookie->read('lang');
			}
			if(!empty($this->request->lang) && $this->request->lang !== $langval){
				$this->Cookie->write('lang', $this->request->lang);
				$langval = $this->request->lang;
			}
		}
		I18n::setLocale($langval);
		
		if($this->request->lang === null && $this->request->prefix === null){
			$url_arr = explode('/', $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
			if($this->app_folder==''){$url_arr[0].='/'.$langval;}else{$url_arr[1].='/'.$langval;}
            debug($url_arr);
			return $this->redirect( 'http://'.implode('/', $url_arr) );
		}
		if(!empty( $this->request['prefix'] )){
            $conditions = [];
            if(!in_array($this->Auth->User("role"), ["admin.root", "admin.admin"])){
                $conditions = ["user_id"=>$this->Auth->User("id")];
            }
            $notifications = $this->loadModel("Assignments")->find("all", [
                "conditions"=>$conditions
            ]);
            $this->set('notifications', $notifications);
        }
		
		$this->set('currlang', $langval);
		$this->set('authUrl', $this->app_folder.$this->Auth->redirectUrl());
		$this->set('app_folder', $this->app_folder);
		$this->set('pages_name', Configure::read('pages_name'));
		$this->set('lcl_langs', Configure::read('lcl_langs'));
		$this->set('countries', $countries);
	}
    
    
    /*public function beforeSave($event, $entity, $options) {
		$entity->dateField = date('Y-m-d', strtotime($entity->dateField));
	}*/
    
    
    public function beforeRender(Event $event){
        
        if (!array_key_exists('_serialize', $this->viewVars) &&
            in_array($this->response->type(), ['application/json', 'application/xml'])
        ) {
            $this->set('_serialize', true);
        }
    }
}
