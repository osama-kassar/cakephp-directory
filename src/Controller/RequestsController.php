<?php
namespace App\Controller;

use App\Controller\AppController;

class RequestsController extends AppController{
	
	
    public function index(){
		if(@$_GET['ajax'] == 1){
			$this->autoRender = false;
		}
		$cond=[];
		$this->request->session()->write('turkeyde.requests_filters', null);
		if(!empty( $_GET['keyword'] )){
			$cond['OR'] = ['req_desc LIKE'=>"%".$_GET['keyword']."%", 'req_title LIKE'=> "%".$_GET['keyword']."%"];
			$this->request->session()->write('turkeyde.requests_filters.keyword', $_GET['keyword']);
		}
		if(@$_GET['ajax'] == 1){
			$requests = $this->Requests->find("all", ["conditions"=>$cond, 
				"fields"=>["slug","req_title", "req_photos"],
				"limit"=>8])->toArray();
			echo  json_encode($requests); die();
		}
		if(!empty( $_GET['category'] )){
			$cond['category_id'] = $_GET['category'];
			$this->request->session()->write('turkeyde.requests_filters.category', $_GET['category']);
		}
		if(!empty( $_GET['subcategory'] )){
			$cond['subcategory_id'] = $_GET['subcategory'];
			$this->request->session()->write('turkeyde.requests_filters.subcategory', $_GET['subcategory']);
		}
		if(!empty( $_GET['country'] )){
			$cond['country_id'] = $_GET['country'];
			$this->request->session()->write('turkeyde.requests_filters.country', $_GET['country']);
		}
		$requests = $this->paginate($this->Requests, [
			'contain'=>[
				'Countries','Cities','Categories','SubCategories'
				],
			'limit'=>12,
			'order'=>['id'=>'DESC'],
			'conditions'=>$cond
		]);
		//debug($requests);
        $categories = $this->Requests->Categories->find('all', ['conditions' => ['parent_id'=>4]])->toList();
        $subcategories = $this->Requests->Categories->find('all', ['conditions' => [ 'parent_id'=>@$cond['category_id'] ]])->toList();
        $total = $this->Requests->find('all')->count();
        $this->set(compact('requests','categories','subcategories','countries','total'));
        $this->set('_serialize', ['requests']);
    }
	
    public function mylist(){
        
		$cond['user_id']=$this->Auth->User('id');
		$this->request->session()->write('turkeyde.requests_filters', null);
		if(!empty( $_GET['category'] )){
			$cond['category_id'] = $_GET['category'];
			$this->request->session()->write('turkeyde.requests_filters.category', $_GET['category']);
		}
		if(!empty( $_GET['subcategory'] )){
			$cond['subcategory_id'] = $_GET['subcategory'];
			$this->request->session()->write('turkeyde.requests_filters.subcategory', $_GET['subcategory']);
		}
		if(!empty( $_GET['country'] )){
			$cond['country_id'] = $_GET['country'];
			$this->request->session()->write('turkeyde.requests_filters.country', $_GET['country']);
		}
		$requests = $this->paginate($this->Requests, [
			'contain'=>[
				'Countries','Categories'
				],
			'limit'=>12,
			'order'=>['id'=>'DESC'],
			'conditions'=>$cond
		]);
        $categories = $this->Requests->Categories->find('all', ['conditions' => ['parent_id'=>4]])->toList();
        $subcategories = $this->Requests->Categories->find('all', ['conditions' => [ 'parent_id'=>@$cond['category_id'] ]])->toList();
        $this->set(compact('requests','categories','subcategories','countries'));
        $this->set('_serialize', ['requests']);
    }
	
    public function view($slug = null){
        $request = $this->Requests->findBySlug($slug)
			->contain(['Categories', 'SubCategories', 'Countries', 'Cities', 'Users', 'Users.Companies', 'Users.Countries', 'Users.Cities'])->first();
        if(empty($request)){ 
            $this->Flash->error(__('not_active_record'));
            return $this->redirect($this->referer());
        }
		$subcategories = $this->Requests->Categories->find("all", ["conditions"=>["parent_id"=>$request->category->id]])->toList();
		$similar_requests = $this->Requests->find("all", [
			"conditions"=>["subcategory_id"=>@$request->subcategory->id]])
			->contain(['Categories', 'Countries'])->toList();
        $this->set(compact('request', 'subcategories', 'similar_requests'));
        $this->set('_serialize', ['request']);
    }
	
	public function add(){
		$request = $this->Requests->newEntity();
        $request->user = $this->Requests->Users->get($this->Auth->User('id'), [
            'contain' => ['Countries', 'Cities']
        ]);
		$categories = $this->Requests->Categories->find('list', ['conditions' => ['parent_id'=>4]]);
        
		if ($this->request->is('post')) {
			$request = $this->Requests->patchEntity($request, $this->request->getData());
			
			$request->user_id = $this->Auth->User('id');
			$request->slug = $this->Do->adrs($request->req_title);
			$request->status = 0;
			$request->language_id = 1;
			
			if($this->request->getData('req_preferred_langs') == ''){
				$request->errors('req_preferred_langs', ['_required'=>__('error_empty')]);
			}else{   
                $request->req_preferred_langs = implode(',', 
				$this->request->getData('req_preferred_langs'));
            }
			if($this->request->getData('req_preferred_contacts') == ''){
				$request->errors('req_preferred_contacts', ['_required'=>__('error_empty')]);
			}else{
                $request->req_preferred_contacts = implode(',', 
				$this->request->getData('req_preferred_contacts'));
            }
			// upload photos
			for($i=0; $i<count($_FILES); $i++){
				$img='';
				if(!empty($_FILES['req_photos_'.$i]['name'])){
					$img = $_FILES['req_photos_'.$i];
					$thumb_params = array(
						array('doThumb'=>true, 'w'=>800, 'h'=>800, 'dst'=>'middle'),
						array('doThumb'=>true, 'w'=>300, 'h'=>300, 'dst'=>'thumb')
						);
					$this->Images->uploader('img/requests_photos', $img, null, $thumb_params);
				}
			}
            $cities = $this->Do->lcl( 
                $this->loadModel('Categories')->find('list')
                    ->where( ['parent_id'=>$request->country_id] )
                    ->orWhere( ['id'=>$request->country_id] )
                );

            $subcategories = $this->Do->lcl( 
                $this->loadModel('Categories')->find('list')
                    ->where( ['parent_id'=>$request->category_id] )
                    ->orWhere( ['id'=>$request->category_id] )
            );
            
			if(!empty($this->Images->photosname)){
				$request->req_photos = implode('|',$this->Images->photosname);
			}
            if ($newRec = $this->Requests->save($request)) {
                $this->Flash->success(__('save-success'));
                return $this->redirect('/requests/edit/'.$newRec->id);
            }
            $this->Flash->error(__('save-fail'));
            
            
        }
        
        $this->set(compact('request', 'categories', 'cities', 'subcategories'));
        $this->set('_serialize', ['request']);
    }
	
    public function edit($id = null){
        
        $request = $this->Requests->get($id, [
            'contain' => ['Users', 'Users.Countries', 'Users.Cities']
        ]);
        $request->req_preferred_langs = explode(",", $request->req_preferred_langs);
        $request->req_preferred_contacts = explode(",", $request->req_preferred_contacts);
        
        if ($this->request->is(['patch', 'post', 'put'])) {
            $request = $this->Requests->patchEntity($request, $this->request->getData());
			// upload photos
			for($i=0; $i<count($_FILES); $i++){
				$img='';
				if(!empty($_FILES['req_photos_'.$i]['name'])){
					$img = $_FILES['req_photos_'.$i];
					$thumb_params = array(
						array('doThumb'=>true, 'w'=>800, 'h'=>800, 'dst'=>'middle'),
						array('doThumb'=>true, 'w'=>300, 'h'=>300, 'dst'=>'thumb')
						);
					$this->Images->uploader('img/requests_photos', $img, null, $thumb_params);
				}
			}
            
			if(!empty($this->Images->photosname)){
                $all = array_filter(array_merge([$request->req_photos], $this->Images->photosname));
				$request->req_photos = implode('|',$all);
			}
            
			if($this->request->getData('req_preferred_langs') == ''){
				$request->errors('_req_preferred_langs', ['_required'=>__('error_empty')]);
			}else{
                $request->req_preferred_langs = implode(',', 
				 $this->request->getData('req_preferred_langs') );
            }
			if($this->request->getData('req_preferred_contacts') == ''){
				$request->errors('_req_preferred_contacts', ['_required'=>__('error_empty')]);
			}else{
                $request->req_preferred_contacts = implode(',', 
				 $this->request->getData('req_preferred_contacts') );
            }
            if ($this->Requests->save($request)) {
                $this->Flash->success(__('save-success'));

                return $this->redirect($this->referer());
            }
            $this->Flash->error(__('save-fail'));
        }
        $categories = $this->Requests->Categories->find('list', ['conditions' => ['parent_id'=>4]]);
        $subcategories = $this->Requests->Categories->find('list', ['conditions' => ['parent_id'=>$request->category_id]]);
		$cities = $this->Do->lcl( 
			$this->Requests->Categories->find('list')
				->where( ['parent_id'=>$request->country_id] )
				->orWhere( ['id'=>$request->country_id] )
		);
			
        $this->set(compact('request', 'categories', 'subcategories', 'countries', 'cities'));
        $this->set('_serialize', ['request']);
    }
    
    public function delete($id = null){
        $this->request->allowMethod(['post', 'delete']);
        $request = $this->Requests->get($id);
        if ($this->Requests->delete($request)) {
            $this->Flash->success(__('delete-success'));
        } else {
            $this->Flash->error(__('delete-fail'));
        }

        return $this->redirect($this->referer());
    }
}
