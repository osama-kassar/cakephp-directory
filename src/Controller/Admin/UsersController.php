<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Auth\AbstractPasswordHasher;
use Cake\Mailer\Email;

class UsersController extends AppController{
	
    public function getStatistics(  ){
		if(!empty(strpos($_SERVER['REQUEST_URI'], 'ajax'))){
			$this->autoRender = false;
        }
        
        $users = $this->loadModel( "Users" )->find("all", [
                "fields"=>["country_id", "role", "created", "status", "country"=>"Countries.category_name"],
                "contain"=>["Countries"],
                "conditions"=>["role LIKE "=>"%user.%"]
            ])->toArray();
        $requests = $this->loadModel( "Requests" )->find("all", [
                "fields"=>["category_id", "country_id", "stat_created", "status", "country"=>"Countries.category_name", "category"=>"Categories.category_name"],
                "contain"=>["Countries", "Categories"]
            ])->toArray();
        echo json_encode(["users"=>$users, "requests"=>$requests], true); die();
    }
    
    public function dashboard(){
        if(strpos($this->Auth->User('role'), 'user.' )>-1 ){
            $this->Flash->error(__('not_allowed'));
            return $this->redirect($this->referer());
        } 
		/*$usersTotal = 
        $usersNew = 
        $requestsTotal = 
        $requestsNew = 
            
        $usersPerCountry = 
        $usersPerCategory = 
        $requestsPerCountry = 
        $requestsPerCategory = */
    }

	
    public function index(){
        $cond=[];
		if( strpos($this->Auth->User('role'), '.editor_') ){
            $cond = ['role LIKE'=>'%user.%'];
        }
		$this->paginate = ['conditions'=>$cond, 'contain'=>[], 'order'=>['id'=>'DESC']];
        $users = $this->paginate($this->Users)->toArray();
		$total = $this->Users->find('all')->count();
		
        $this->set(compact('users', 'total'));
        $this->set('_serialize', ['users']);
    }
	
   public function add() {
		if(@$this->request->query['ajax'] == 1){
			$this->autoRender = false;
			$user = json_decode(file_get_contents('php://input'));
			//print_r($user);
		}
		/*if(!empty($this->Auth->User('id')) && $this->Auth->User('role') !== 'root'){
			$this->Flash->error(__('one_account_allowed'));
			$this->redirect(['controller'=>'Users', 'action'=>'dashboard']);
		}*/
		
		$user = $this->Users->newEntity();
        if ($this->request->is('post')) {
			$user = $this->Users->patchEntity($user, $this->request->data);
			$user->created = date("Y-m-d H:i:s");
			$user->modified = date("Y-m-d H:i:s");
			$user->role = 'user.free';
			$user->status = 0;
			$user->email_activate = base64_encode($user->username.'-'.$user->password);
			
			//debug ($user['iagree']);die();
			if($user['iagree'] == 'no'){
				$user->errors('iagree', ['_required'=>__('error_not_agreed')]);
			}
            if ($newRec = $this->Users->save($user)) {
				
				$email = new Email();
				$email->from(['info@qassar-tech.com' => __('Turkeyde')])
					->to($newRec->username)
					->subject(__('new_account'))
					->template('register_tm')
					->emailFormat('html')
					->viewVars(['content' => $newRec])
					->send();
					
				if(@$this->request->query['ajax'] == 1){echo 1;die();}
				$this->Flash->success(__('register-success'));
            }else{
				if(@$this->request->query['ajax'] == 1){echo json_encode($user->errors());die();}
                $this->Flash->error(__('register-fail'));
			}
        }
		$cities = $this->Do->lcl( 
			$this->loadModel('Categories')->find('list')
				->where( ['parent_id'=>$user->country_id] )
				->orWhere( ['id'=>$user->country_id] )
		);
		if(@$this->request->query['ajax'] == 1){
			return json_encode($user);
		}else{
			$this->set(compact('user', 'cities'));
			$this->set('_serialize', ['user']);
		}
    }

    public function view($id = null){
        $rec = $this->Users->get($id);
        $this->set('rec', $rec);
        $this->set('_serialize', ['rec']);
    }
	
    public function edit($id = null){
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
			
            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('save-success'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('save-fail'));
            }
        }
		$cities = $this->Do->lcl( 
			$this->loadModel('Categories')->find('list')
				->where( ['parent_id'=>$user->country_id] )
				->orWhere( ['id'=>$user->country_id] )
		);
        $this->set(compact('user', 'cities'));
        $this->set('_serialize', ['user']);
    }
	
	public function disable($id = null){
		$this->autoRender = false;
		$this->request->allowMethod(['post', 'delete']);
		
		$record = $this->Users->get($id);
		$record->status == 1 ? $enbled = 0 : $enbled = 1;
		$record->id = $id;
		$record->status = $enbled;
		if ($this->Users->save($record)){
			$this->Flash->success(__('disable-success').' '.$enbled);
		} else {
			$this->Flash->error(__('disable-fail'));
		}
		return $this->redirect(['action' => 'index']);
		
    }
	public function delete($id = null){
        $this->request->allowMethod(['post', 'delete']);
        $member = $this->Users->get($id);
        if ($this->Users->delete($member)) {
            $this->Flash->success(__('delete-success'));
        } else {
            $this->Flash->error(__('delete-fail'));
        }
        return $this->redirect($this->referer());
    }
	
	
	public function maillist(){
		$this->autoRender = false;
        if ($this->request->is('post')) {
			foreach($this->request->data['rec_list'] as $rec){
				if($rec !== '0'){
					$user = $this->Users->find('all', ['conditions'=>['id'=>$rec]]);
					$email = new Email();
					$email->from([$this->request->data['from'] => $this->request->data['from']])
						->to($user->email)
						->subject($this->request->data['subject'])
						->emailFormat('html')
						//->template('register_tm')
						//->viewVars(['content' => $u])
						->send();
				}
			}
		}
	}
}
