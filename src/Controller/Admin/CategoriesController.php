<?php
namespace App\Controller\Admin;

use App\Controller\AppController;


class CategoriesController extends AppController{
	
	private function _setLabel($ddd){
		$res=[];
		foreach($ddd as $k => $v){
			$res[$k] = __($v);
		}
		return $res;
	}
	
	public function ajax_fetcher(){
		$id = $this->request->query['id'];
		$sub = $this->Do->lcl( $this->Categories->find('list', [
			'conditions'=>['parent_id'=>$id]]) );
		//echo "subcat/ ", json_encode($sub), "/subcat";die();
		echo json_encode($sub);die();
	}
	
    public function index(){
		$cond = [];
		if(isset($_GET['parent_id']) && !empty($_GET['parent_id'])){
			$cond[] = ['parent_id' => $_GET['parent_id']];
		}
		if(isset($_GET['language_id']) && !empty($_GET['language_id'])){
			$cond[] = ['language_id' => $_GET['language_id']];
		}else{
			$cond[] = ['language_id' => 1];
		}
		
        $this->paginate = [
			'conditions' => $cond,
			'order'=>['id ASC'],
			'contain'=>['ChildCategories']
        ];
		
		$categories = $this->paginate($this->Categories);
		
        $this->set(compact('categories'));
        $this->set('_serialize', ['categories']);
    }
	
    public function view($id = null){
        $rec = $this->Categories->get($id);
        $this->set('rec', $rec);
        $this->set('_serialize', ['rec']);
    }
	
    public function add(){
		
		$childern = [];
		$pid = empty($_GET['parent_id']) ? 0 : $_GET['parent_id'];
		if(isset($_GET['parent_id'])){
			$childern = $this->Categories->find('list', ['conditions'=>["parent_id"=>$pid]]);
        	$parents = $this->Categories->find('list', ['conditions'=>['id'=>$pid]]);
		}else{
        	$parents = $this->Categories->find('list', ['conditions'=>['parent_id'=>0]]);
		}
		
        $category = $this->Categories->newEntity();
		$done=0;
        if ($this->request->is('post')){
            $category = $this->Categories->patchEntity($category, $this->request->data);
			$isArray = array_filter(explode('|', $category->category_name));
			if(!empty($isArray)){
				foreach($isArray as $catVal){
            		$category = $this->Categories->newEntity();
					$category->language_id = 1;
					$category->category_name = trim($catVal);
					$category->parent_id = !empty($_GET['parent_id']) ? $_GET['parent_id'] : 0;
					$category->status = 1;
					if ($this->Categories->save($category)){
						$done++;
					} 
				}
				if($done > 0){
					$this->Flash->success(__('save-success'));
					return $this->redirect($this->referer());
				}else {
					$this->Flash->error(__('save-fail'));
				}
			}else{
				
				if ($newRec = $this->Categories->save($category)){
					$this->Flash->success(__('save-success'));
					return $this->redirect($this->referer());
				} else {
					$this->Flash->error(__('save-fail'));
				}
			}
        }
        $this->set(compact('category', 'parents', 'childern'));
        $this->set('_serialize', ['category']);
    }
	
    public function edit($id = null){
		$category = $this->Categories->findAllById($id)->first();
		$childern = [];
		$pid = empty($_GET['parent_id']) ? 0 : $_GET['parent_id'];
		if(isset($_GET['parent_id'])){
        	$parents = $this->Categories->find('list', ['conditions'=>['id'=>$pid]]);
			$childern = $this->Categories->find('list', ['conditions'=>["parent_id"=>$pid ]]);
		}else{
        	$parents = $this->Categories->find('list', ['conditions'=>["id"=>$category->parent_id]]);
			$childern = $this->Categories->find('list', ['conditions'=>["parent_id"=>$category->parent_id]]);
		}
		
        if ($this->request->is(['patch', 'post', 'put'])){
            $category = $this->Categories->patchEntity($category, $this->request->data);
            if ($newRec = $this->Categories->save($category)){
                $this->Flash->success(__('save-success'));
                return $this->redirect($this->referer());
            } else {
                $this->Flash->error(__('save-delete'));
            }
        }
        $this->set(compact('category', 'parents', 'childern'));
        $this->set('_serialize', ['category']);
    }
	
    public function delete($id = null){
        $this->request->allowMethod(['post', 'delete']);
        if ($this->Categories->delete($id)) {
            $this->Flash->success(__('delete-success'));
        } else {
            $this->Flash->error(__('delete-fail'));
        }
        return $this->redirect($this->referer());
    }
	
	public function disable($id = null){
		
        $this->request->allowMethod(['post', 'delete']);
        $record = $this->Categories->get($id);
		$record->status == 1 ? $enbled = 0 : $enbled = 1;
		$record->id = $id;
		$record->status = $enbled;
        if ($this->Categories->save($record)){
            $this->Flash->success(__('disable-success').' '.$enbled);
        } else {
            $this->Flash->error(__('disable-fail'));
        }
        return $this->redirect($this->referer());
    }
}
