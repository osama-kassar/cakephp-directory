<?php
namespace App\Controller\Admin;

use App\Controller\AppController;


class RequestsController extends AppController{

    public function index()    {
        $this->paginate = [
            'contain' => ['Users', 'Categories', 'Countries', 'SubCategories', 'Cities']
        ];
        $requests = $this->paginate($this->Requests);

        $this->set(compact('requests'));
        $this->set('_serialize', ['requests']);
    }

    public function view($id = null){
        $rec = $this->Requests->get($id);
        $this->set('rec', $rec);
        $this->set('_serialize', ['rec']);
    }

	
	public function add(){
		$request = $this->Requests->newEntity();
		
		if ($this->request->is('post')) {
			$request = $this->Requests->patchEntity($request, $this->request->getData());
			
				
			$request->user_id = $this->Auth->User('id');
			$request->slug = $this->Do->adrs($request->req_title);
			$request->status = 0;
			
			if($request['req_preferred_langs'] == ''){
				$request->errors('_req_preferred_langs', ['_required'=>__('error_empty')]);
			}else{
                $request->req_preferred_langs = implode(',', $this->request->getData('req_preferred_langs'));
            }
			if($request['req_preferred_contacts'] == ''){
				$request->errors('_req_preferred_contacts', ['_required'=>__('error_empty')]);
			}else{
			     $request->req_preferred_contacts = implode(',', $this->request->getData('req_preferred_contacts'));
            }
			
			// upload photos
			for($i=0; $i<count($_FILES); $i++){
				$img='';
				if(!empty($_FILES['photo_'.$i]['name'])){
					$img = $_FILES['photo_'.$i];
					$thumb_params = array(
						array('doThumb'=>true, 'w'=>800, 'h'=>800, 'dst'=>'middle'),
						array('doThumb'=>true, 'w'=>300, 'h'=>300, 'dst'=>'thumb')
						);
					$this->Images->uploader('img/requests_photos', $img, null, $thumb_params);
				}
			}
			
			if(!empty($this->Images->photosname)){
				$request->req_photos = implode('|',$this->Images->photosname);
			}
			
            if ($newRec = $this->Requests->save($request)) {
                $this->Flash->success(__('save-success'));
                $this->Do->adder([
                    'user_id'=>0,
                    'assignedby_id'=>$this->Auth->User('id'),
                    'assign_target'=>'Requests, '.$newRec->id,
                    'assign_title'=>'New from admin',
                    'assign_notes'=>null,
                    'stat_created'=>date("Y-m-d H:i:s"),
                    'stat_delivered'=>null,
                    'status'=>0,
                ], 'Assignments');
                return $this->redirect('/requests/edit/'.$newRec->id);
            }
            $this->Flash->error(__('save-fail'));
        }
        $categories = $this->Requests->Categories->find('list', ['conditions' => ['parent_id'=>4]]);

		$cities = $this->Do->lcl( 
			$this->loadModel('Categories')->find('list')
				->where( ['parent_id'=>$request->country_id] )
				->orWhere( ['id'=>$request->country_id] )
		);
		
        $this->set(compact('request', 'categories'));
        $this->set('_serialize', ['request', 'cities']);
    }
	
    public function edit($id = null){
        $request = $this->Requests->get($id, [
            'contain' => []
        ]);
        $request->req_preferred_langs = explode(",", $request->req_preferred_langs);
        $request->req_preferred_contacts = explode(",", $request->req_preferred_contacts);
		
        if ($this->request->is(['patch', 'post', 'put'])) {
            $request = $this->Requests->patchEntity($request, $this->request->getData());
            
			if($this->request->getData('req_preferred_langs') == ''){
				$request->errors('req_preferred_langs', ['_required'=>__('error_empty')]);
			}else{
                $request->req_preferred_langs = implode(',', $this->request->getData('req_preferred_langs'));
            }
			if($this->request->getData('req_preferred_contacts') == ''){
				$request->errors('req_preferred_contacts', ['_required'=>__('error_empty')]);
			}else{
                $request->req_preferred_contacts = implode(',',  $this->request->getData('req_preferred_contacts'));
            }
			
            
			// upload photos
			for($i=0; $i<count($_FILES); $i++){
				$img='';
				if(!empty($_FILES['req_photos_'.$i]['name'])){
					$img = $_FILES['req_photos_'.$i];
					$thumb_params = array(
						array('doThumb'=>true, 'w'=>800, 'h'=>800, 'dst'=>'middle'),
						array('doThumb'=>true, 'w'=>300, 'h'=>300, 'dst'=>'thumb')
						);
					$this->Images->uploader('img/requests_photos', $img, null, $thumb_params);
				}
			}
			if(!empty($this->Images->photosname)){
                $all = array_filter(array_merge([$request->req_photos], $this->Images->photosname));
				$request->req_photos = implode('|',$all);
			}
            
            if ($this->Requests->save($request)) {
                $this->Flash->success(__('save-success'));

                return $this->redirect($this->referer());
            }
            $this->Flash->error(__('save-fail'));
        }
        $categories = $this->Requests->Categories->find('list', ['conditions' => ['parent_id'=>4]]);
        $subcategories = $this->Requests->Categories->find('list', ['conditions' => ['parent_id'=>$request->category_id]]);
		$cities = $this->Do->lcl( 
			$this->Requests->Categories->find('list')
				->where( ['parent_id'=>$request->country_id] )
				->orWhere( ['id'=>$request->country_id] )
		);
			
        $this->set(compact('request', 'categories', 'subcategories', 'countries', 'cities'));
        $this->set('_serialize', ['request']);
    }
    public function delete($id = null){
        $this->request->allowMethod(['post', 'delete']);
        $request = $this->Requests->get($id);
        if ($this->Requests->delete($request)) {
            $this->Flash->success(__('delete-success'));
        } else {
            $this->Flash->error(__('delete-fail'));
        }

        return $this->redirect(['action' => 'index']);
    }
	
	public function delphoto(){
		$this->autoRender = false;
		$request = $this->Requests->get( $_GET['id'] );
		$dt['id'] = $request->id;
		$dt[ $_GET['field'] ] = $this->Do->delItm( [$_GET['photo']], $request[$_GET['field']], 'img/request_photos' );
		$res = $this->Do->adder($dt, 'Requests');
		if( $res  ){
        	$this->Flash->success(__('save-success'));
         	return $this->redirect(['action' => 'edit', $request->id]);
		}
	}
	
	public function disable($id = null){
		
        $this->request->allowMethod(['post', 'delete']);
        $record = $this->Requests->get($id);
		$record->status == 1 ? $enbled = 0 : $enbled = 1;
		$record->id = $id;
		$record->status = $enbled;
        if ($this->Requests->save($record)){
            $this->Flash->success(__('disable-success').' '.$enbled);
        } else {
            $this->Flash->error(__('disable-fail'));
        }
        return $this->redirect($this->referer());
    }
}
