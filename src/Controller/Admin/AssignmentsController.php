<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

class AssignmentsController extends AppController
{
    public function index()
    {
        $this->paginate = [
            //'contain' => ['Users']
        ];
        $assignments = $this->paginate($this->Assignments);

        $this->set(compact('assignments'));
    }

    public function view($id = null)
    {
        $rec = $this->Assignments->get($id, [
            'contain' => ['Users']
        ]);
        $users = $this->loadModel("Users")->find("list", [
            "conditions"=>["role LIKE "=>"%admin.%"]
        ]);
        $langs = $this->Do->get('langs');
        $this->set(compact('rec', 'users', 'langs'));
    }

    public function add()
    {
        $assignment = $this->Assignments->newEntity();
        if ($this->request->is('post')) {
            $assignment = $this->Assignments->patchEntity($assignment, $this->request->getData());
            if ($this->Assignments->save($assignment)) {
                $this->Flash->success(__('The assignment has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The assignment could not be saved. Please, try again.'));
        }
        $users = $this->Assignments->Users->find('list', ['limit' => 200]);
        $assignedbies = $this->Assignments->Assignedbies->find('list', ['limit' => 200]);
        $this->set(compact('assignment', 'users'));
    }

    public function edit($id = null) {
        $assignment = $this->Assignments->get($id, [
            'contain' => []
        ]);
        
        if ($this->request->is(['patch', 'post', 'put'])) {
            $assignment = $this->Assignments->patchEntity($assignment, $this->request->getData());
            $assignment->assignedby_id = $this->Auth->User("id");
            $assignment->stat_started = date('Y-m-d H:i:s');
            //$assignment->stat_expiredate = date('Y-m-d H:i:s',strtotime("+".$_POST['assign']['expiredate']." day"));
            $assignment->user_id = $_POST['assign']['userid'];
            $assignment->status = 1;
                //debug($assignment->id);die();
            
            if($_POST['assign']['cloneTarget'] == '1'){
                $arr = explode(",", $assignment->assign_target);
                $rec = $this->loadModel($arr[0])->get($arr[1]);
                $rec->language_id = $_POST['assign']['lang'];
                $rec->id = null;
                
                if($newRec = $this->Do->adder($rec->toArray(), $arr[0])){
                    $assignment->assign_target = $arr[0].", ".$newRec->id;
                }
            }
            
            if($_POST['assign']['cloneAssign'] == '1'){
                $assignment->id = null;
                if($assignRec = $this->Do->adder($assignment->toArray(), "Assignments")){
                    $this->Flash->success(__('The assignment has been saved.'));
                    return $this->redirect(['action' => 'index']);
                }
            }
            //debug($assignment);die();
            if ($this->Assignments->save($assignment)) {
                $this->Flash->success(__('The assignment has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The assignment could not be saved. Please, try again.'));
        }
        $users = $this->Assignments->Users->find('list', ['limit' => 200]);
        $this->set(compact('assignment', 'users'));
    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $assignment = $this->Assignments->get($id);
        if ($this->Assignments->delete($assignment)) {
            $this->Flash->success(__('The assignment has been deleted.'));
        } else {
            $this->Flash->error(__('The assignment could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
