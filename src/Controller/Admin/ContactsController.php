<?php
namespace App\Controller\Admin;

use App\Controller\AppController;


class ContactsController extends AppController{
	
    public function index(){
		$cond = [];
		if(isset($_GET['parent_id']) && !empty($_GET['parent_id'])){
			$cond[] = ['parent_id' => $_GET['parent_id']];
		}
		
        $this->paginate = [
			'conditions' => $cond,
			'order'=>['id ASC']
        ];
		
		$contacts = $this->paginate($this->Contacts);
		
        $this->set(compact('contacts'));
        $this->set('_serialize', ['contacts']);
    }
	
    public function view($id = null){
        $rec = $this->Contacts->get($id);
        $this->set('rec', $rec);
        $this->set('_serialize', ['rec']);
    }
	
    public function add(){
        $contact = $this->Contacts->newEntity();
		$parents = $this->Contacts->find("list", ["conditions"=>["parent_id"=>0] ]);
        if ($this->request->is('post')){
            $contact = $this->Contacts->patchEntity($contact, $this->request->data);
			$contact->stat_type = 2;
			$contact->stat_info = 'agent: '.@$_SERVER['HTTP_USER_AGENT'].' <br> ip: <a href="http://whatismyipaddress.com/ip/'.@$_SERVER['REMOTE_ADDR'].'">'.@$_SERVER['REMOTE_ADDR'].'</a> <br> lang: '.@$_SERVER['HTTP_ACCEPT_LANGUAGE'].' <br> connection: '.@$_SERVER['HTTP_CONNECTION'];
			$contact->status = 1;
			$contact->stat_created = date("Y-m-d H:i:s");
			if ($newRec = $this->Contacts->save($contact)){
				$this->Flash->success(__('save-success'));
				return $this->redirect($this->referer());
			} else {
				$this->Flash->error(__('save-fail'));
			}
        }
        $this->set(compact('contact', 'parents'));
        $this->set('_serialize', ['contact']);
    }
	
    public function edit($id = null){
		$contact = $this->Contacts->findAllById($id)->first();
		$parents = $this->Contacts->find("list", ["conditions"=>["parent_id"=>0] ]);
        if ($this->request->is(['patch', 'post', 'put'])){
            $contact = $this->Contacts->patchEntity($contact, $this->request->data);
            if ($newRec = $this->Contacts->save($contact)){
                $this->Flash->success(__('save-success'));
                return $this->redirect($this->referer());
            } else {
                $this->Flash->error(__('save-fail'));
            }
        }
        $this->set(compact('contact', 'parents'));
        $this->set('_serialize', ['contact']);
    }
	
    public function delete($id = null){
        $this->request->allowMethod(['post', 'delete']);
        if ($this->Contacts->delete($id)) {
            $this->Flash->success(__('delete-success'));
        } else {
            $this->Flash->error(__('delete-fail'));
        }
        return $this->redirect($this->referer());
    }
	
	public function disable($id = null){
		
        $this->request->allowMethod(['post', 'delete']);
        $record = $this->Contacts->get($id);
		$record->status == 1 ? $enbled = 0 : $enbled = 1;
		$record->id = $id;
		$record->status = $enbled;
        if ($this->Contacts->save($record)){
            $this->Flash->success(__('disable-success').' '.$enbled);
        } else {
            $this->Flash->error(__('disable-fail'));
        }
        return $this->redirect($this->referer());
    }
}
