<?php
namespace App\Controller\Admin;

use App\Controller\AppController;


class ArticlesController extends AppController{
	
	public function uploadphoto(){
        $latest = $this->Articles->find('all',['order'=>['id'=>'DESC']])->first();
		$inc=0;
		if( !strpos($_SERVER['REQUEST_URI'], '/edit') ){$inc=1;}
		if(!empty($_FILES['upload']['name'])){
			$phname = $_FILES['upload']['name'];
			$res = ["fileName"=>$phname, "uploaded"=>0, "error"=>"", "url"=>""];
			$img = $_FILES['upload'];
			$thumb_params = array(
				//array('doThumb'=>true, 'w'=>650, 'h'=>650, 'dst'=>'middle'),
				array('doThumb'=>true, 'w'=>300, 'h'=>400, 'dst'=>'thumb')
				);
			if($this->Images->uploader('img/media', $img, ($latest->id+$inc).'_'.$phname, $thumb_params)){
				$res['uploaded'] = 1;
				$res['fileName'] = $this->Images->photosname[0];
				$res['url'] = $app_folder."/webroot/img/media/thumb/".$res['fileName'];
			}else{
				$res['error'] = ["number"=>203, "message"=>"upload fail"];
			}
		}
		echo json_encode($res); die();
	}
	
    public function index(){
		
        $this->paginate = [
            'contain' => ['Categories']
        ];
        $articles = $this->paginate($this->Articles, ['order'=>['id'=>'DESC']]);
		$totalComments = $this->loadModel('Comments')->find('all', ['conditions'=>['status'=>0]])->count();
		$totalContacts = $this->loadModel('Contacts')->find('all', ['conditions'=>['status'=>0]])->count();
        $this->set(compact('articles','totalComments','totalContacts'));
        $this->set('_serialize', ['articles']);
    }
	
    public function view($id = null){
        $rec = $this->Articles->get($id);
        $this->set('rec', $rec);
        $this->set('_serialize', ['rec']);
    }
	
    public function add(){
        $article = $this->Articles->newEntity();
        if ($this->request->is('post')) {
            $article = $this->Articles->patchEntity($article, $this->request->data);
			$article->user_id = $this->Auth->User('id');
			$article->article_postdate = date('Y-m-d');
			$article->slug = $this->Do->adrs($article->article_title);
            if ($this->Articles->save($article)) {
                $this->Flash->success(__('save-success'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('save-fail'));
            }
        }
        $users = $this->Articles->Users->find('list', ['limit' => 200]);
        $categories = $this->Articles->Categories->find('list', ['conditions' => ['parent_id'=>'2']]);
        $this->set(compact('article', 'users', 'categories'));
        $this->set('_serialize', ['article']);
    }
	
    public function edit($id = null){
        $article = $this->Articles->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $article = $this->Articles->patchEntity($article, $this->request->data);
			
            if ($this->Articles->save($article)) {
                $this->Flash->success(__('save-success'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('save-fail'));
            }
        }
        $categories = $this->Articles->Categories->find('list', ['conditions' => ["parent_id"=>2]]);
        $subcategories = $this->Articles->Categories->find('list', ['conditions' => ["parent_id"=>$article->category_id]]);
        $this->set(compact('article', 'categories', 'subcategories'));
        $this->set('_serialize', ['article']);
    }
	
	public function disable($id = null){
		
        $this->request->allowMethod(['post', 'delete']);
        $record = $this->Articles->get($id);
		$record->status == 1 ? $enbled = 0 : $enbled = 1;
		$record->id = $id;
		$record->status = $enbled;
        if ($this->Articles->save($record)){
            $this->Flash->success(__('disable-success').' '.$enbled);
        } else {
            $this->Flash->error(__('disable-fail'));
        }
        return $this->redirect($this->referer());
    }
	
    public function delete($id = null){
        $this->request->allowMethod(['post', 'delete']);
        $article = $this->Articles->get($id);
        if ($this->Articles->delete($article)) {
            $this->Flash->success(__('delete-success'));
        } else {
            $this->Flash->error(__('delete-fail'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
