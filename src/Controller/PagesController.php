<?php

namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;

class PagesController extends AppController{
	
    public function display(...$path){
        $count = count($path);
        if (!$count) {
            return $this->redirect('/');
        }
        if (in_array('..', $path, true) || in_array('.', $path, true)) {
            throw new ForbiddenException();
        }
        $page = $subpage = null;

        if (!empty($path[0])) {
            $page = $path[0];
        }
        if (!empty($path[1])) {
            $subpage = $path[1];
        }
		
		if($page == 'photos' || $page == 'sharer'){
			$this->viewBuilder()->layout('empty');
		}
		
		/*$counts = (object)[
			"companies"=>$this->loadModel('Companies')->find('all')->count(),
			"requests"=>$this->loadModel('Requests')->find('all')->count(),
			"users"=>$this->loadModel('Users')->find('all')->count(),
			"articles"=>$this->loadModel('Articles')->find('all')->count()
		];
		
        $companies_cat = $this->loadModel('Categories')->find('all', ['conditions' => ['parent_id'=>7]])->toList();
        $requests_cat = $this->loadModel('Categories')->find('all', ['conditions' => ['parent_id'=>4]])->toList();
        $articles = $this->loadModel('Articles')->find('all', ['limit' => 8, 'order' => 'Articles.id DESC'])
			->contain('Categories')->toList();*/
		
        $countries_tr = $this->Do->lcl( $this->loadModel('Categories')->find('list', ['conditions' => ['parent_id'=>391], "fields"=>["id", "category_name"]])->toArray() );
        $countries = $this->Do->lcl( $this->loadModel('Categories')->find('list', ['conditions' => ['parent_id'=>1], "fields"=>["id", "category_name"]])->toArray() );
        $this->set(compact('page', 'subpage', 'countries', 'countries_tr'));
		
        try {
            $this->render(implode('/', $path));
        } catch (MissingTemplateException $e) {
            if (Configure::read('debug')) {
                throw $e;
            }
            throw new NotFoundException();
        }
    }
}
