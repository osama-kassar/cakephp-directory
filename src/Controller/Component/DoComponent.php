<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Core\Configure;

use Cake\ORM\TableRegistry;
use Cake\I18n\I18n;
use Cake\Utility\Security;
use App\Controller\Component\ImagesComponent;
use Cake\Datasource\ConnectionManager;

class DoComponent extends Component {
	
	public function Num($n){
		$formated = number_format($n, 2, ',', '.');
		return $formated;
	}
	
	public function lcl($arr, $isObj=false){
		$res=[]; 
		foreach($arr as $k=>$v){
			$res[$k] = __($v, true);
		}
		return $res;
	}
	
	public function setPW($length = 8, $type=1){
        $chrs = ["abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789", "0123456789"];
		$pass = array(); 
		$alphaLength = strlen($chrs[$type]) - 1; 
		for ($i = 0; $i < $length; $i++) {
			$n = rand(0, $alphaLength);
			$pass[] = $chrs[$type][$n];
		}
		return implode($pass); 
    }
	
	public function call($precedure, $id){
		$conn = ConnectionManager::get('default');
		$dt = $conn->execute("CALL `$precedure`($id);")->fetchAll('assoc')[0]['val'];
		return $dt;
	}
	
	public function getFolder($dir="img/articles/"){
		$res=array();$inc=0;
		if (is_dir($dir)) {
			if ($dh = opendir($dir)) {
				while (($file = readdir($dh)) !== false) {
					if($file != '..' && $file != '.' && $file != 'thumb'){
						$inc++;
						$stat = stat($dir.$file);
						$res[$stat['mtime']+$inc] = $file;
					}
				}
				closedir($dh);
			}
			krsort($res);
		}else{
			$res = 'not dir';
		}
		return $res;
	}
	
	public function delItm($vals, $from, $path){
		$from_arr = explode('|',$from);
		for($i=0; $i<count($vals); $i++){
			if($vals[$i] !== '0'){
				ImagesComponent::deleteFile($path, $vals[$i]);
				unset($from_arr[array_search($vals[$i], $from_arr)]);
			}
		}
		return implode('|',$from_arr) ;
	}
	
	public function isChkd($vals){
		for($i=0; $i<count($vals); $i++){
			if($vals[$i] !== '0'){
				return true;
			}
		}
		return false;
	}
	
	public function get($id, $params=null){
        $res='';
		switch($id){
				
			case 'days':
				$res = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
				break;
                
			case 'exp_method':
				$res = array(1=>__('retail'), 2=>__('wholesale'));
				break;
				
			case 'langs':
				$res = array(1=>__('ar'), 2=>__('tr'), 3=>__('en'));
				break;
				
			case 'prefered_langs':
				$res = ['ar', 'en', 'tr'];
				break;
				
			case 'prefered_contacts':
				$res = ['whatsapp', 'phone', 'email'];
				break;
				
			case 'contact_reason':
				$res = ["question", "buy_request", "complain", "other"];
				break;
				
			case 'UserRoles':
				$res = Configure::read('UserRoles');
				break;
				
			case 'AdminRoles':
				$res = Configure::read('AdminRoles');
				break;
		}
		return $res;
	}
	
	public function getMachineLang($val){
		foreach(Configure::read('I18n.languages') as $lang){
			if(strpos($val, $lang) !== false){
				return $lang;
			}
		}
	}
	
	public function adder($dt, $mdl){
		$model = TableRegistry::get($mdl);
		$record = $model->newEntity();
		if(isset($dt['id'])){
			$record = $model->get($dt['id']);
		}
		foreach($dt as $k => $v){
			$record[$k] = $v;
		}
		if($newRec = $model->save($record)){
			return $newRec;
		}
		return false;
	}
	
	/*public function getConfig($key, $tar=null){
		$tar == null ? $cond = ['config_key'=>$key] : $cond = ['config_key'=>$key, 'config_tar'=>$tar];
		$res = TableRegistry::get('Configs')->find('all', ['conditions'=>$cond])->first();
		return $res->config_val;
	}*/
	
	public function calc($dt, $col){
		$res = 0;
		foreach($dt as $d){
			$res = $res + $d->$col;
		}
		return $res == 0 ? $res = 1 : $res = $res;
	}
	
	public function captchaCheck($dt){
		$salt=Security::salt();
		$code = str_replace($salt,'',$dt['CaptchaCodeSource']);
		if($code == $dt['CaptchaCode']){
			return true;
		}
		return false;
	}
	
	public function increaseHits($clm = 'readcounter'){
		$model = $this->Controller->modelClass;
		$ModelInit = ClassRegistry::init($model);
		
		$id = Router::getParams();
		$val = $ModelInit->find('first', array('conditions'=>"`".$model."`.`id` = '".$id."'", 'fields' => $clm));
		
		$ModelInit->id = $id;
		return $ModelInit->saveField($clm, ($val[$model][$clm]+1));
	}
	
	public function setLbl($dt){
		$res=[];
		foreach($dt as $k => $v){
			$res[$k] = __($v);
		}
		return $res;
	}
	
	public function adrs($text){
		$AddHtmlEnd = false;
		$text = trim ($text);
		$text = preg_replace('{(.)\1+}','$1',$text);
		$text = preg_replace('/ /', '_', $text);
		$text = preg_replace('/أ|إ|آ/', 'ا', $text);
		$text = preg_replace('/~/', '_', $text);
		$text = preg_replace('/`|\!|\@|\#|\^|\&|\(|\)|\|/', '', $text);
		$text = preg_replace('/{|}|\[|\]/', '_', $text);
		$text = preg_replace('/\+|\-|\*|\/|\=|\%|\$|×/', '', $text);
		$text = preg_replace('/,|\.|\'|;|\?|\<|\>|"|:/', '', $text);
		$text = preg_replace('/^_/', '', $text);
		$text = preg_replace('/_$/', '', $text);
		$text = preg_replace('/_/', '-', $text);
		if ($AddHtmlEnd) {
			$text .= '.html';
		}
		return $text;
	}
}
?>