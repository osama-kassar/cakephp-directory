<?php

namespace App\Controller\Component;

use Cake\Controller\Component;

class ImagesComponent extends Component {
	
	var $allowed_ext = array(
							 'images'=>array('jpg','jpeg','gif','png'),
							 'media'=>array('swf','flv'),
							 'doc'=>array('doc','pdf')
							 );
	var $max_upload_size = 3500;// per kilobyte
	var $photosname = array();
	var $Error_Msg = array();
		
    function uploader($path, $file, $name, $thumb, $crup=0){
		!empty($name) ? $newName = $name.'.'.$this->getExt($file) : $newName = date('ymd').time().count($this->photosname).'.'.$this->getExt($file);
		if(move_uploaded_file($file['tmp_name'], $path.'/'.$newName)) {
				$this->photosname[] = $newName;
			for($i=0; $i<count($thumb); $i++){
				if($thumb[$i]['doThumb']){
					$this->resizer($path.'/'.$newName, $path.'/'.$thumb[$i]['dst'].'/'.$newName, $thumb[$i]['w'], $thumb[$i]['h'], $crup);
				}
			}
			return true;
		}else{
			return false;
		}
	}
	
	function resizer($src, $dst, $width, $height, $crop=0){
		$this->Error_Msg = array();
		if(!list($w, $h) = getimagesize($src)){
			$this->Error_Msg[] = 'dimensions_error'; 
			return "Unsupported picture type!";
		}
		$type = strtolower(substr(strrchr($src,"."),1));
		if($type == 'jpeg') $type = 'jpg';
		switch($type){
			case 'bmp': $img = imagecreatefromwbmp($src); break;
			case 'gif': $img = imagecreatefromgif($src); break;
			case 'jpg': $img = imagecreatefromjpeg($src); break;
			case 'png': $img = imagecreatefrompng($src); break;
			default : return "Unsupported picture type!";
			$this->Error_Msg[] = 'type_error'; break;
		}
		
		// resize
		if($crop){
			if($w < $width or $h < $height){
				return "Picture is too small!";
				$this->Error_Msg[] = 'picture_small';
			}
			$ratio = max($width/$w, $height/$h);
			$h = $height / $ratio;
			$x = ($w - $width / $ratio) / 2;
			$w = $width / $ratio;
		}else{
			if($w < $width and $h < $height){
				return "Picture is too small!";
				$this->Error_Msg[] = 'picture_small';
			}
			$ratio = min($width/$w, $height/$h);
			$width = $w * $ratio;
			$height = $h * $ratio;
			$x = 0;
		}
		
		$new = imagecreatetruecolor($width, $height);
		
		// preserve transparency
		if($type == "gif" or $type == "png"){
			imagecolortransparent($new, imagecolorallocatealpha($new, 0, 0, 0, 127));
			imagealphablending($new, false);
			imagesavealpha($new, true);
		}
		
		imagecopyresampled($new, $img, 0, 0, $x, 0, $width, $height, $w, $h);
		
		switch($type){
			case 'bmp': imagewbmp($new, $dst); break;
			case 'gif': imagegif($new, $dst); break;
			case 'jpg': imagejpeg($new, $dst); break;
			case 'png': imagepng($new, $dst); break;
		}
		return true;
	}
	
	public function getExt($file){
		$fileext = substr($file['type'],6);
		switch($fileext){
			case 'jpeg':
			case 'jpg':
			$res = 'jpg';
			break;
			
			default:
			$res = $fileext;
			break;
		}
		return $res;
	}
	
	public function deleteFile($path, $img){
		@unlink($path.'/'.$img);
		@unlink($path.'/thumb/'.$img);
		@unlink($path.'/middle/'.$img);
	}
}

?>