<?php
namespace App\Controller;

use App\Controller\AppController;

class ProfilesController extends AppController{

    public function index(){
        $this->paginate = [
            'contain' => ['Users']
        ];
        $profiles = $this->paginate($this->Profiles);

        $this->set(compact('profiles'));
        $this->set('_serialize', ['profiles']);
    }
	
    public function view($id = null){
        $profile = $this->Profiles->get($id, [
            'contain' => ['Users', 'Categories', 'Expenses', 'Incomes']
        ]);

        $this->set('profile', $profile);
        $this->set('_serialize', ['profile']);
    }
	
    public function add(){
        $profile = $this->Profiles->newEntity();
        if ($this->request->is('post')) {
            $profile = $this->Profiles->patchEntity($profile, $this->request->data);
            if ($this->Profiles->save($profile)) {
                $this->Flash->success(__('The profile has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The profile could not be saved. Please, try again.'));
            }
        }
        $users = $this->Profiles->Users->find('list', ['limit' => 200]);
        $this->set(compact('profile', 'users'));
        $this->set('_serialize', ['profile']);
    }
	
	public function create(){
		$this->autoRender = false;
		$profile = $this->Profiles->newEntity();
		$profile->user_id = $this->Auth->User('id');
		$profile->profile_name = __('profile').' : '.$this->Auth->User('username');
		$profile->profile_desc = __('profiledesc').' : '.$this->Auth->User('id');
		$profile->status = 1;
		if ($pid = $this->Profiles->save($profile)) {
			//$pid = $this->Profiles->getLastInsertID();
			$this->Flash->success(__('The profile has been saved.'));
			return $this->redirect(['action' => 'edit', $pid->id]);
		} else {
			$this->Flash->error(__('Profiles not added yet, Please add profile'));
			return $this->redirect(['action'=>'add']);
		}
	}
	
    public function edit($id = null){
        $profile = $this->Profiles->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $profile = $this->Profiles->patchEntity($profile, $this->request->data);
			
            if ($this->Profiles->save($profile)) {
                $this->Flash->success(__('message-edit'));
				/*if($this->Do->get('lang_id')[$profile->language_id] !== $this->Do->get('currlang')){
					return $this->redirect('/'.$this->Do->get('lang_id')[$profile->language_id].
						substr($_SERVER['REQUEST_URI'],strlen($app_folder)));
				}*/
                return $this->redirect($this->referer());
            } else {
                $this->Flash->error(__('error-edit'));
            }
        }
        $users = $this->Profiles->Users->find('list', ['limit' => 200]);
        $this->set(compact('profile', 'users'));
        $this->set('_serialize', ['profile']);
    }
	
    public function delete($id = null){
        $this->request->allowMethod(['post', 'delete']);
        $profile = $this->Profiles->get($id);
        if ($this->Profiles->delete($profile)) {
            $this->Flash->success(__('The profile has been deleted.'));
        } else {
            $this->Flash->error(__('The profile could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
