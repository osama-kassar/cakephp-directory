<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Mailer\Email;


class ContactsController extends AppController{
	
    public function index(){
		$cond = [];
		if(isset($_GET['parent_id']) && !empty($_GET['parent_id'])){
			$cond[] = ['parent_id' => $_GET['parent_id']];
		}
		
        $this->paginate = [
			'conditions' => $cond,
			'order'=>['id ASC']
        ];
		
		$contacts = $this->paginate($this->Contacts);
		
        $this->set(compact('contacts'));
        $this->set('_serialize', ['contacts']);
    }
	
    public function view($id = null){
        $rec = $this->Contacts->get($id);
        $this->set('rec', $rec);
        $this->set('_serialize', ['rec']);
    }
	
    public function sendmessage(){
		if($this->request->query['ajax'] == 1){
			$this->autoRender = false;
		}
        if ($this->request->is('post')){
			
			if(empty($_GET['tarSlug'])){ echo 0; return ; };
			$company = $this->loadModel('Companies')->findBySlug($_GET['tarSlug'])->first();
			//debug($contact['tar']->company_email);
			$contact = $this->Contacts->newEntity();
			$contact = $this->Contacts->patchEntity($contact, $this->request->data);
			$contact->stat_type = 1;
			$contact->stat_info = 'agent: '.@$_SERVER['HTTP_USER_AGENT'].' <br> ip: <a href="http://whatismyipaddress.com/ip/'.@$_SERVER['REMOTE_ADDR'].'">'.@$_SERVER['REMOTE_ADDR'].'</a> <br> lang: '.@$_SERVER['HTTP_ACCEPT_LANGUAGE'].' <br> connection: '.@$_SERVER['HTTP_CONNECTION'];
			$contact->status = 0;
			$contact->stat_created = date("Y-m-d H:i:s");
			
			$email = new Email();
			$email->from(['info@qassar-tech.com' => __('Turkeyde')])
				->to($company->company_email)
				->subject(__('contact_from').' '.$contact->contact_name)
				->template('contact_company_tm')
				->emailFormat('html')
				->viewVars(['content' => $contact]);
				
			if( $email->send() ){
				$newRec = $this->Contacts->save($contact);
				if(@$_GET['ajax'] == 1){echo 1; return ;}
                //$this->Flash->success(__('send-success'));
			}else{
				if(@$_GET['ajax'] == 1){echo 0; return ;}
                //$this->Flash->error(__('send-fail'));
			}
        }
        $this->set(compact('contact'));
        $this->set('_serialize', ['contact']);
    }
	
    public function edit($id = null){
		$contact = $this->Contacts->findAllById($id)->first();
		$parents = $this->Contacts->find("list", ["conditions"=>["parent_id"=>0] ]);
        if ($this->request->is(['patch', 'post', 'put'])){
            $contact = $this->Contacts->patchEntity($contact, $this->request->data);
            if ($newRec = $this->Contacts->save($contact)){
                $this->Flash->success(__('save-success'));
                return $this->redirect($this->referer());
            } else {
                $this->Flash->error(__('save-fail'));
            }
        }
        $this->set(compact('contact', 'parents'));
        $this->set('_serialize', ['contact']);
    }
	
    public function delete($id = null){
        $this->request->allowMethod(['post', 'delete']);
        if ($this->Contacts->delete($id)) {
            $this->Flash->success(__('delete-success'));
        } else {
            $this->Flash->error(__('delete-fail'));
        }
        return $this->redirect($this->referer());
    }
	
	public function disable($id = null){
		
        $this->request->allowMethod(['post', 'delete']);
        $record = $this->Contacts->get($id);
		$record->status == 1 ? $enbled = 0 : $enbled = 1;
		$record->id = $id;
		$record->status = $enbled;
        if ($this->Contacts->save($record)){
            $this->Flash->success(__('disable-success').' '.$enbled);
        } else {
            $this->Flash->error(__('disable-success'));
        }
        return $this->redirect($this->referer());
    }
}
