<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Form\ContactForm;
use Cake\Auth\AbstractPasswordHasher;
use Cake\Core\Configure;
use Cake\Mailer\Email;

class UsersController extends AppController{
	
	public function dashboard() {
		$user = $this->Auth->User();
		$req = $this->loadModel('Requests');
        
		$views = $this->loadModel('Requests')->find("all", [
			"conditions"=>["user_id"=>$user['id']],
			"fields"=>["val"=>"SUM(Requests.stat_views)"]
		])->toArray()[0]->val;
        
		$shares = $this->loadModel('Requests')->find("all", [
			"conditions"=>["user_id"=>$user['id']],
			"fields"=>["val"=>"SUM(Requests.stat_shares)"]
		])->toArray()[0]->val;
        
        $company = $this->Users->Companies->find("all",[
				"conditions"=>["user_id"=>$user['id']], 
				"fields"=>["stat_views","stat_shares"] ])->first();
        $total_req = $this->loadModel('Requests')->find("all",["conditions"=>["user_id"=>$user['id']] ])->count();
		$stats = [
			//"req_views"=>$this->Do->call('requests_stat_views', $user['id']),
			//"req_shares"=>$this->Do->call('requests_stat_shares', $user['id']),
			"req_views"=>empty($views) ? 0 : $views,
			"req_shares"=>empty($shares) ? 0 : $shares,
			"req_total"=>empty($total_req) ? 0 : $total_req ,
			"company"=>empty($company) ? (object)["stat_views"=>0, "stat_shares"=>0] : $company
		];
        $this->set(compact('user','stats'));
	}
	public function login() {
		if(!empty(strpos($_SERVER['REQUEST_URI'], 'ajax'))){
			$this->autoRender = false;
			$inp = json_decode(file_get_contents('php://input'));
			$_POST['username'] = $inp->username;
			$_POST['password'] = $inp->password;
		}
		if(!is_null($this->Auth->user('id'))){
			echo '<script>parent.window.location = "'.$this->app_folder.$this->Auth->redirectUrl().'"</script>';
		}
		if ($this->request->is('post')) {
            $oldLogin = date("Y-m-d H:i:s");
			$user = $this->Auth->identify(["contain"=>["Countries","Cities"]]);
            
			if (!$user) { 
                echo json_encode(["status"=>-1, "role"=>'']); return ;
            }
            
            $user["oldLogin"] = $oldLogin;
			$stat = -1;
			
            
			$com = $this->Users->Companies->find('all',[
				'conditions'=>['user_id'=>$user['id']]
			]);
			
			(!empty($com->toArray())) ? $user['company']=$com->first()->toArray() : $user['company']='';
			$user['status'] == 1 ? $stat = 1 : $stat = 0;
			$user['email_activate'] == 1 ? $stat = 1 : $stat = 0;
			//$user['sms_activate'] == 1 ? $stat = 1 : $stat = -2;
            $redirectPath = '';
            
			if ($stat == 1) {
				$this->Auth->setUser($user);
				$this->Cookie->write('User', $user);
            }
			echo json_encode(["status"=>$stat, "role"=>$user['role']]); return ;
		}
    }
	
    public function logout() {
        $this->autoRender = false;
        if( !empty( $this->Auth->User("id") ) ){
            echo $this->Auth->User("id");
            $this->Cookie->delete('User');
            return $this->redirect($this->Auth->logout());
        }
    }
    
    public function register() {
		if(!empty(strpos($_SERVER['REQUEST_URI'], 'ajax'))){
			$this->autoRender = false;
			$user = json_decode(file_get_contents('php://input'));
		}
		if(!empty($this->Auth->User('id')) && $this->Auth->User('role') !== 'root'){
			$this->Flash->error(__('one_account_allowed'));
			$this->redirect(['controller'=>'Users', 'action'=>'dashboard']);
		}
		
		$user = $this->Users->newEntity();
        if ($this->request->is('post')) {
			$user = $this->Users->patchEntity($user, $this->request->data);
			$user->created = date("Y-m-d H:i:s");
			$user->modified = date("Y-m-d H:i:s");
			$user->role = 'user.free';
			$user->status = 0;
			$user->email_activate = base64_encode($user->username.'-'.$user->password);
			$user->sms_activate = 1;
            
			if($user['iagree'] !== 'yes'){
				$user->errors('iagree', ['_required'=>__('error_not_agreed')]);
			}
			if($user['maillist'] == 'yes'){
                $contact = $this->Do->adder([
                    "parent_id"=>0,
                    "country_id"=>0,
                    "city_id"=>0,
                    "contact_name"=>$user->firstname." ".$user->lastname,
                    "contact_phone"=>$user->phone,
                    "contact_email"=>$user->username,
                    "contact_subject"=>"From Registeration",
                    "contact_message"=>"Please add me to mail list",
                    "contact_reason"=>"Follower",
                    "stat_info"=>'agent: '.@$_SERVER['HTTP_USER_AGENT'].' <br> ip: <a href="http://whatismyipaddress.com/ip/'.@$_SERVER['REMOTE_ADDR'].'">'.@$_SERVER['REMOTE_ADDR'].'</a> <br> lang: '.@$_SERVER['HTTP_ACCEPT_LANGUAGE'].' <br> connection: '.@$_SERVER['HTTP_CONNECTION'],
                    "stat_type"=>0,
                    "stat_priority"=>2,
                    "stat_created"=>date("Y-m-d H:i:s"),
                    "status"=>1
                ], 'Contacts');
			}
            if ($newRec = $this->Users->save($user)) {
				
                
                $newRec['app_folder'] = $this->app_folder;
				$email = new Email();
				$email->from( $this->mailer )
					->to($newRec->username)
					->subject(__('new_account'))
					->template('register_tm')
					->emailFormat('html')
					->viewVars(['content' => $newRec]);
                
				if($email->send()){
                    if(@$_GET['ajax'] == 1){echo 1; return ;}
                    $this->Flash->success(__('send-success'));
                }else{
                    if(@$_GET['ajax'] == 1){echo 0; return ;}
                    $this->Flash->success(__('send-fail'));
                }
            }else{
				if($this->request->query['ajax'] == 1){echo json_encode($user->errors());die();}
                $this->Flash->error(__('register-fail'));
			}
        }
		$cities = $this->Do->lcl( 
			$this->loadModel('Categories')->find('list')
				->where( ['parent_id'=>$user->country_id] )
				->orWhere( ['id'=>$user->country_id] )
		);
		if($this->request->query['ajax'] == 1){
			return json_encode($user);
		}else{
			$this->set(compact('user', 'cities'));
			$this->set('_serialize', ['user']);
		}
    }
	
	public function activate($code=null){
		$this->autoRender = false;
		$checkUser = $this->Users->find('all',['conditions'=>['email_activate'=>$code]])->first();
        
		if($checkUser == null){
			$this->Flash->error(__('expired_link'));
			return $this->redirect('/');
		}
		if($checkUser->email_activate == $code){
			$user = $this->Users->newEntity();
			$user->id = $checkUser->id;
			$user->email_activate = '1';
			$user->status = '1';
			if($this->Users->save($user)){
				$this->Flash->success(__('account_activated'));
			}else{
				$this->Flash->error(__('error_activated_msg'));
			}
			$this->redirect("/?login=1");
		}else{
			$this->Flash->error(__('wrong_id'));
			$this->redirect("/");
		}
	}
	
	public function activatesms($code=null){
		$this->autoRender = false;
		$checkUser = $this->Users->find('all',['conditions'=>['email_activate'=>$code]])->first();
		
		if($checkUser->email_activate == '1'){
			$this->Flash->success(__('account_activated'));
			$this->redirect(['controller'=>'Users', 'action'=>'login']);
		}
		if($checkUser->email_activate == $code){
			$user = $this->Users->newEntity();
			$user->id = $checkUser->id;
			$user->email_activate = '1';
			$user->status = '1';
			if($this->Users->save($user)){
				$this->Flash->success(__('account_activated'));
			}else{
				$this->Flash->error(__('error_activated_msg'));
			}
			$this->redirect("/?login=1");
		}else{
			$this->Flash->error(__('wrong_id'));
			$this->redirect("/");
		}
	}
	
	public function getpassword(){
		
		if($this->request->query['ajax'] == 1){
			$this->autoRender = false;
		}
		
		if(!empty( $this->Auth->User('id') )){
			return $this->redirect(['controller'=>'Users', 'action'=>'dashboard']);
		}
		
        if ($this->request->is('post')) {
			$user = $this->Users->newEntity();
			$checkUser = $this->Users->findByUsername( @$this->request->data['email'] )->first();
			if(empty($checkUser)){echo -1; return ;}
			$newPW = $this->Do->setPW();
			$checkUser->password = $newPW;
            $checkUser->app_folder = $this->app_folder;
			if($upRec = $this->Users->save($checkUser)){
				$checkUser->new_password = $newPW;
				$email = new Email();
				$email->from($this->mailer)
					->to($upRec->username)
					->subject(__('new_password_subject'))
					->template('getpassword_tm')
					->emailFormat('html')
					->viewVars(['content' => $checkUser]);
					
                if($email->send()){
                    if(@$_GET['ajax'] == 1){echo 1; return ;}
                    $this->Flash->success(__('send-success'));
                    $this->redirect(['controller'=>'users', 'action'=>'login']);
                }else{
                    if(@$_GET['ajax'] == 1){echo 0; return ;}
                    $this->Flash->success(__('send-success'));
                }
			}else{
				if(@$_GET['ajax'] == 1){echo 0; return ;}
				$this->Flash->error(__('email_not_found'));
			}
		}
        $this->set('user', $user);
	}
	
    public function view($id = null){
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        $this->set('user', $user);
        $this->set('_serialize', ['user']);
    }
	
	public function packages(){
		$id = $this->Auth->User('id'); 
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('save-success'));
                return $this->redirect($this->referer());
            } else {
                $this->Flash->error(__('save-fail'));
            }
        }
		$this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }
	
	
	public function delphoto(){
		$this->autoRender = false;
		$id = $this->Auth->User('id'); 
		$user = $this->Users->get( $id );
		$dt['id'] = $id;
		$dt[ $_GET['field'] ] = $this->Do->delItm( [$_GET['photo']], $user[$_GET['field']], 'img/users_photos' );
		$res = $this->Do->adder($dt, 'Users');
		if( $res  ){
            $user->photo = $res->photo;
            $this->request->session()->write('Auth.User.photo', '');
        	$this->Flash->success(__('save-success'));
         	return $this->redirect('/myaccount');
		}
	}
	
    public function edit($id = null){
		$id = $this->Auth->User('id'); 
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            //debug($this->Auth->User());die();
            if(!empty($this->request->data['new_password'])){
				$user->password = $this->request->data['new_password'];
			}
            $user = $this->Users->patchEntity($user, $this->request->data);	
			
			// delete multiple selected photos
			/*if($this->Do->isChkd( $user->photo_list ) || !empty($_FILES['photo_0']['name'])){
				$user->photo = $this->Do->delItm( [$user->photo], $user->photo, 'img/users_photos' );
			}*/
			if(!empty($_FILES['photo_0']['name'])){
				$img = $_FILES['photo_0'];
				$thumb_params = array(
					array('doThumb'=>true, 'w'=>300, 'h'=>300, 'dst'=>'thumb')
					);
				$this->Do->delItm( [$user->photo], $user->photo, 'img/users_photos' );
				$this->Images->uploader('img/users_photos', $img, 'profile_'.$user->id, $thumb_params, 1);
				$user->photo = $this->Images->photosname[0];
				$this->request->session()->write('Auth.User.photo', $this->Images->photosname[0]);
			}
            if ($this->Users->save($user)) {
                $this->Flash->success(__('save-success'));
                return $this->redirect($this->referer());
            } else {
                $this->Flash->error(__('save-fail'));
            }
        }
		$languages = $this->Do->get('lang');
		
        $cities = $this->Do->lcl( 
			$this->loadModel('Categories')->find('list')
				->where( ['parent_id'=>$user->country_id] )
				->orWhere( ['id'=>$user->country_id] )
		);
        
		$this->set(compact('user','languages','countries','cities'));
        $this->set('_serialize', ['user']);
    }
	
	public function delete($id = null){
		$this->autoRender = false;
		$this->request->allowMethod(['post', 'delete']);
		
		$record = $this->Users->get($id);
		$record->status == 1 ? $enbled = 0 : $enbled = 1;
		$record->id = $id;
		$record->status = $enbled;
		if ($this->Users->save($record)){
			$this->Flash->success(__('disable-success').' '.$enbled);
		} else {
			$this->Flash->error(__('disable-fail'));
		}
		return $this->redirect(['action' => 'index']);
		
    }
    
}