<?php
namespace App\Controller;

use App\Controller\AppController;

class CompaniesController extends AppController{
	
	
    // GENERAL FUNCTIONS ///////////////////////////////////////
	public function delphoto(){
		$this->autoRender = false;
		$rec = $this->loadModel( $_GET['mdl'] )->get( $_GET['id'] );
		$dt['id'] = $rec->id;
		$dt[ $_GET['field'] ] = $this->Do->delItm( [$_GET['photo']], $rec[$_GET['field']], 'img/'. strtolower($_GET['mdl']) .'_photos' );
		$res = $this->Do->adder($dt, $_GET['mdl']);
		if( $res  ){
        	$this->Flash->success(__('save-success'));
            if($_GET['mdl'] == 'Users'){ $this->request->session()->write('Auth.User.photo', ''); }
		}else{
        	$this->Flash->success(__('save-fail'));
        }
        return $this->redirect($this->referer());
	}
    public function sharecounter(){
        $this->autoRender = false;
        $id = $_GET['id']; $mdl = $_GET['mdl'];
        if(empty($id) || empty($mdl)){ return false;}
        $rec = $this->loadModel($mdl)->get($id);
        if(!empty($rec->toArray())){
            $updt = $this->Do->adder(["id"=>$rec->id, "stat_shares"=>($rec->stat_shares*1)+1], $mdl);
            if(count($updt)>0){echo '1';}else{echo '0';}
        }
    }
    // END GENERAL FUNCTIONS ///////////////////////////////////////
    
    public function index(){
		if(@$_GET['ajax'] == 1){
			$this->autoRender = false;
		}
		$cond=[];
		$this->request->session()->write('turkeyde.companies_filters', null);
		if(!empty( $_GET['keyword'] )){
			$cond['OR'] = ['company_desc LIKE'=>"%".$_GET['keyword']."%", 'company_name LIKE'=> "%".$_GET['keyword']."%"];
			$this->request->session()->write('turkeyde.companies_filters.keyword', $_GET['keyword']);
		}
		if(@$_GET['ajax'] == 1){
			$companies = $this->Companies->find("all", ["conditions"=>$cond, 
				"fields"=>["slug","company_name", "company_logo"],
				"limit"=>8])->toArray();
			echo  json_encode($companies); die();
		}
		if(!empty( $_GET['category'] )){
			$cond['category_id'] = $_GET['category'];
			$this->request->session()->write('turkeyde.companies_filters.category', $_GET['category']);
		}
		if(!empty( $_GET['country'] )){
			$cond['city_id'] = $_GET['country'];
			$this->request->session()->write('turkeyde.companies_filters.country', $_GET['country']);
		}
		$companies = $this->paginate($this->Companies, [
			'contain'=>[
				'Countries','Cities','Categories','SubCategories'
				],
			'limit'=>12,
			'order'=>['company_priority'=>'ASC', 'id'=>'DESC'],
			'conditions'=>$cond
		]);
        $categories = $this->Companies->Categories->find('all', ['conditions' => ['parent_id'=>7]])->toList();
        $countries =  
			$this->Companies->Categories->find('list', [
				"conditions" => ['parent_id'=>391], 
				"fields"=>["id", "category_name"],
				"order"=>["category_priority", "category_name"]
				])->toArray() ;
		$total = $this->Companies->find('all')->count();
        $this->set(compact('companies','categories','countries','total'));
        $this->set('_serialize', ['companies']);
    }
	
    public function view($slug = null){
		$company = $this->Companies->findBySlug($slug)
			->contain(['Categories', 'SubCategories', 'Cities'])->first();
        if(empty($company)){ 
            $this->Flash->default(__('not_active_record'));
			return $this->redirect( $this->referer() );
        }
        $this->set('company', $company);
        $this->set('_serialize', ['company']);
    }
	
    public function add(){
		if(!empty($this->Auth->User('company'))){ 
			return $this->redirect('/mycompany' );
		}
        $company = $this->Companies->newEntity();
        if ($this->request->is('post')) {
            
            $company = $this->Companies->patchEntity($company, $this->request->getData());
            
			$company->user_id = $this->Auth->User('id');
			$company->slug = $this->Do->adrs($company->company_name);
			$company->country_id = 391;
			$company->seo_desc = strlen($company->seo_desc)>0 ? $company->seo_desc : substr($company->company_desc, 0, 250);
			//$company->seo_keywords = implode(", ", $this->request->getData('seo_keywords'));
			$company->status = 0;
			$company->company_preferred_langs = @implode(',', 
				 $this->request->getData('company_preferred_langs'));
			
			$company->company_links = $company->company_links_0.':link:'.
									 $company->company_links_1.':link:'.
									 $company->company_links_2.':link:'.
									 $company->company_links_3.':link:'.
									 $company->company_links_4;
            
			$days = @implode("-", $this->request->getData('company_worktime'));
            $company->company_worktime = $days .', '. $company->company_worktime_from.', ' .  $company->company_worktime_to;
			
			if($company->company_preferred_langs == ''){
				$company->errors('_company_preferred_langs', ['_required'=>__('error_empty')]);
			}
			//upload logo
			if(!empty($_FILES['logo_0']['name'])){
				$img = $_FILES['logo_0'];
				$thumb_params = array(
					array('doThumb'=>true, 'w'=>650, 'h'=>650, 'dst'=>'middle'),
					array('doThumb'=>true, 'w'=>300, 'h'=>300, 'dst'=>'thumb')
				);
				$this->Images->uploader('img/companies_photos', $img, "logo_".$company->user_id, $thumb_params);
				$company->company_logo = $this->Images->photosname[0];
				unset($this->Images->photosname[0]);
			}
            
			// upload photos
			for($i=0; $i<count($_FILES); $i++){
				$img='';
				if(!empty($_FILES['photo_'.$i]['name'])){
					$img = $_FILES['photo_'.$i];
					$thumb_params = array(
						array('doThumb'=>true, 'w'=>650, 'h'=>650, 'dst'=>'middle'),
						array('doThumb'=>true, 'w'=>300, 'h'=>300, 'dst'=>'thumb')
						);
					$this->Images->uploader('img/companies_photos', $img, null, $thumb_params);
				}
			}
			if(!empty($this->Images->photosname)){
				$company->company_photos = implode('|',$this->Images->photosname);
			}
			
            if ($newRec = $this->Companies->save($company)) {
                $this->request->session()->write('Auth.User.company', $newRec); 
                $this->Flash->success(__('save-success'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('save-fail'));
        }
        $categories = $this->Companies->Categories->find('list', ['conditions' => ['parent_id'=>7]]);
		$subcategories = $this->Companies->Categories->find('list', ['conditions' => ['parent_id'=>$company->category_id]]);
		$countries = $this->Do->lcl( $this->Companies->Categories->find('list', ['conditions' => ['id'=>391]])->toArray() );
		$cities = $this->Do->lcl( 
			$this->loadModel('Categories')->find('list')
				->where( ['parent_id'=>391] )
				->orWhere( ['id'=>$company->country_id] )
		);
        $this->set(compact('company', 'cities', 'categories', 'subcategories', 'countries'));
        $this->set('_serialize', ['company']);
    }
	
    public function edit($id = null){
		if($this->Auth->User('company') == ''){return $this->redirect(["action"=>"add"]);}
		$id = $this->Auth->User('company.id'); 
        $company = $this->Companies->get($id, [
            'contain' => []
        ]);
		$company->company_links = explode(':link:', $company->company_links);
        $company->company_preferred_langs = explode(",", $company->company_preferred_langs);
        $all = explode(",", $company->company_worktime);
        $company->company_worktime = explode("-", $all[0]);
        $company->company_worktime_from = $all[1];
        $company->company_worktime_to = $all[2];
        if ($this->request->is('post')) {
            $company = $this->Companies->patchEntity($company, $this->request->getData());
			$company->user_id = $this->Auth->User('id');
			$company->status = 1;
			$company->company_preferred_langs = implode(',', 
				$this->request->getData('company_preferred_langs'));
            
			$company->company_links= $company->company_links_0.':link:'.
									 $company->company_links_1.':link:'.
									 $company->company_links_2.':link:'.
									 $company->company_links_3.':link:'.
									 $company->company_links_4;
            
			$days = implode("-",$this->request->getData('company_worktime'));
            $company->company_worktime = $days .', '. $company->company_worktime_from.', ' .  $company->company_worktime_to;
            
			if($company['company_preferred_langs'] == ''){
				$company->errors('_company_preferred_langs', ['_required'=>__('error_empty')]);
			}
			//upload logo
			if(!empty($_FILES['company_logo_0']['name'])){
				$img = $_FILES['company_logo_0'];
				$thumb_params = array(
					array('doThumb'=>true, 'w'=>650, 'h'=>650, 'dst'=>'middle'),
					array('doThumb'=>true, 'w'=>300, 'h'=>300, 'dst'=>'thumb')
				);
				$this->Do->delItm( [$company->company_logo], $company->company_logo, 'img/companies_photos' );
				$this->Images->uploader('img/companies_photos', $img, "logo_".$company->id, $thumb_params);
				$company->company_logo = $this->Images->photosname[0];
				unset($this->Images->photosname[0]);
			}
			
			// upload photos
			for($i=0; $i<count($_FILES); $i++){
				$img='';
				if(!empty($_FILES['company_photos_'.$i]['name'])){
					$img = $_FILES['company_photos_'.$i];
					$thumb_params = array(
						array('doThumb'=>true, 'w'=>650, 'h'=>650, 'dst'=>'middle'),
						array('doThumb'=>true, 'w'=>300, 'h'=>300, 'dst'=>'thumb')
						);
					$this->Images->uploader('img/companies_photos', $img, null, $thumb_params);
				}
			}
			if(!empty($this->Images->photosname)){
				$this->Images->photosname[]=$company->company_photos;
				$company->company_photos = implode('|', $this->Images->photosname );
			}
			
            if ($this->Companies->save($company)) {
                $this->Flash->success(__('save-success'));

                return $this->redirect($this->referer());
            }
            $this->Flash->error(__('save-fail'));
        }
        $categories = $this->Companies->Categories->find('list', ['conditions' => ['parent_id'=>7]]);
        $subcategories = $this->Companies->Categories->find('list', ['conditions' => ['parent_id'=>$company->category_id]]);
        $countries = $this->Do->lcl( $this->Companies->Categories->find('list', ['conditions' => ['id'=>391]])->toArray() );
        
		$cities = $this->Do->lcl( 
			$this->loadModel('Categories')->find('list')
				->where( ['parent_id'=>391] )
				->orWhere( ['id'=>$company->country_id] )
		);
        
		$req_preferred_contacts = $this->Companies->Categories->find('list', ['conditions' => ['parent_id'=>5]]);
        $req_preferred_langs = $this->Companies->Categories->find('list', ['conditions' => ['parent_id'=>6]]);
		
		
        $this->set(compact('company', 'categories', 'subcategories', 'cities', 'countries', 'req_preferred_contacts','req_preferred_langs'));
        $this->set('_serialize', ['company']);
    }
	
    public function delete($id = null){
        $this->request->allowMethod(['post', 'delete']);
        $company = $this->Companies->get($id);
        if ($this->Companies->delete($company)) {
            $this->Flash->success(__('delete-success'));
        } else {
            $this->Flash->error(__('delete-fail'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
