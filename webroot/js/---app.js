

var ptrn=[];
	ptrn['isEmail'] 		= /^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,7}$/;
	ptrn['isNumber'] 		= /^[0-9]$/;
	ptrn['isInteger'] 		= /^[\s\d]$/;
	ptrn['isFloat'] 		= /^[0-9]?\d+(\.\d+)?$/;
	ptrn['isVersion'] 		= /^(?:(\d+)\.)?(?:(\d+)\.)?(\*|\d+)$/
	ptrn['isPassword'] 		= /^[A-Za-z0-9@#$%^&*()!_-]{4,32}$/;
	ptrn['isParagraph'] 	= /^.{40,255}$/;
	ptrn['isEmpty'] 		= /^((?!undefined).){3,255}$/;
	ptrn['isZipcode'] 		= /^\+[0-9]{1,4}$/;
	ptrn['isPhone'] 		= /^[\s\d]{9,12}$/;

var errorMsg=[];
	errorMsg['isEmail'] 		= 'E-Mail format is not correct!';
	errorMsg['isNumber']	   = 'Not correct number format!';
	errorMsg['isInteger'] 	  = 'Not correct integer format!';
	errorMsg['isFloat'] 		= 'Not correct float number format!';
	errorMsg['isVersion']	  = 'Not correct version format!';
	errorMsg['isPassword'] 	 = 'Only Alphabet, Numbers and symboles @ # $ % ^ & * ( ) ! _ - allowed';
	errorMsg['isParagraph'] 	= 'Paragraph should be between 40 and 255 character';
	errorMsg['isEmpty'] 		= 'Please insert correct value!';
	errorMsg['isPhone'] 		= 'Phone format is not correct!';

function _setError(elm ,msg="", clr=false){
	if(clr){return elm.style.borderBottom = "1px solid green";}
	elm.style.borderBottom = "1px solid red";
}



var app = angular.module('turkeyde', []);
app.controller('ctrl', function($scope, $http, $location) {alert()
	
	$scope.app_folder = '/turkeyde';
	$scope.fsearch={"page" : "companies"};
	$scope.userdt  = {};
	$scope.currUrl  = window.location.href;
	var path = '<?php echo $authUrl?>';
	$scope.dmn = window.location.origin;
	var _setCvr = function(tar ,param){
		var elm = document.getElementById( tar );
		if(param==1){
			elm.style.opacity=1;
			elm.style.zIndex=200;
		}else{
			elm.style.opacity=0;
			elm.style.zIndex=-1;
		}
	}
	var getErrors = function (obj){
		$('.error-message').remove();
		const result = '';
		for(const prop in obj) {
			const value = obj[prop];
			var arr = $.map(value, function(val, index) {
				return [val];
			});
			$('[name="'+prop+'"]').parent().append('<div class="error-message">'+arr.join('<br>')+'</div>');
		}
			//return arr.join('<br>')
	}
	
	$scope.showMore = function(elm){
		$('#'+elm).toggleClass("do_show_more");
	}
	$scope.doLogin = function(){
		_setCvr('cvr', 1)
		$http({
			method  : "POST",
			url	 : $scope.app_folder+"/login?ajax=1",
			data	: $scope.userdt 
		})
		.then(function(res) {
			if(res.data !== '-1'){
				$('#flash').html("<div class='message success' onclick=$(this).addClass('fadeOut');><span class='fadeIn'><?=__('login-success')?></span></div>");
				//$('#login', window.parent.document).modal('hide');
				//$('#login', top.document).modal('hide');
				$('#login').modal('hide');
				if(path == $scope.app_folder+'/'){
					window.location.reload();
				}else{
					window.location.href = path;
				}
			}else{
				$('#flash').html("<div class='message error' onclick=$(this).addClass('fadeOut');><span class='fadeIn'><?=__('login-fail')?></span></div>");
			}
			_setCvr('cvr', 0)
		});
	}
	
	$scope.registerdt={iagree: 'no'}
	$scope.doRegister = function(){
		_setCvr('cvr',1);
		$http({
			method  : "POST",
			url	 : $scope.app_folder+"/register?ajax=1",
			data	: $scope.registerdt 
		})
		.then(function(res) {
			if(res.data !== '1'){
				getErrors(res.data)
				$('#flash').html("<div class='message error' onclick=$(this).addClass('fadeOut');><span class='fadeIn'><?=__('register-fail')?></span></div>");
			}else{
				$('#flash').html("<div class='message success' onclick=$(this).addClass('fadeOut');><span class='fadeIn'><?=__('register-success')?></span></div>");
				//$('#login', window.parent.document).modal('hide');
				//$('#login', top.document).modal('hide');
				$('#register').modal('hide');
				$('#login').modal('show');
			}
		});
		_setCvr('cvr', 0)
		console.log($scope.registerdt);
	}
	
	$scope.getInfo = function(){
		$http.get($scope.app_folder+"/articles?ajax=1")
		.then(function(dt){
			alert(dt)
		})
	}
	
	$scope.callAction = function(url){
		$http.get($scope.app_folder+url).then(function(res){
			console.log(res.data)
		})
	}
	
	var setShare = function(tar){
		var pageUrl = document.URL;
		var title = document.title;
		var p = document.getElementsByTagName("article")[0].innerHTML;
		var url = '';
		if(tar == 'facebook'){
			url = decodeURI('https://www.facebook.com/sharer/sharer.php?u='+pageUrl);
		}
		if(tar == 'twitter'){
			url = decodeURI('https://twitter.com/home?status='+pageUrl);
		}
		if(tar == 'google-plus'){
			url = decodeURI('https://plus.google.com/share?url='+pageUrl);
		}
		if(tar == 'linkedin'){
			url = decodeURI('https://www.linkedin.com/shareArticle?mini=true&url='+pageUrl+'&title='+title+'&summary='+p+'&source='+pageUrl);
		}
		if(tar == 'pinterest'){
			url = decodeURI('https://pinterest.com/pin/create/button/?url='+pageUrl+'&media='+p+'&description='+p);
		}
		if(tar == 'email'){
			url = 'mailto:email';
		}
		return url;
	}
	
    $scope.openWin = function(tar){
		var urlTar = setShare(tar);
		var conf = "directories=no, menubar=no, status=no, location=no, titlebar=no, toolbar=no, scrollbars=no, width=700, height=400, resizeable=no, top=150, left=250";
		var popup=window.open(urlTar, '_blank', conf);//'http://localhost/turkeyde/pages/sharer'
		//var dom = popup.document.body;
    }
	
	$scope.goTo = function(path, ext=null){
		/*if(path=='-1'){ return window.history.back(); }
		if(path=='/--'){ return null; }
		if(path==''){ return $location.path('/');}
		if(ext!==null){ return window.location.href= path;} 
		var urlparams = path.split('?')
		if(urlparams[1]){
			var itms = urlparams[1].split('=')
			return $location.path( urlparams[0] ).search(itms[0], itms[1]);
		}*/
		return window.location.href = $scope.dmn+$scope.app_folder+path
		}
})
.directive('setFixed', ['$window', function ($window) {
	return {
		restrict: 'A',
		link: function(scope, element, attrs) {
			angular.element(document).bind('scroll', function() {
				if ($window.pageYOffset > 1) {
					element[0].classList.add('smallHeader')
				} else {
					element[0].classList.remove('smallHeader')
				}
			});
		}
	};
}])
.directive('chk', function () {
    return {
        restrict: 'AE',
        require: 'ngModel',
        scope: {chk: '@'},
        link: function (scope, element, attrs, ctrl) {
				alert()
				ctrl.$parsers.unshift(function (viewValue) {
					
					alert(scope.chk)
					if ( ptrn[scope.chk].test(viewValue) ) {
						_setError(element[0] , '', true);
						ctrl.$setValidity('checked', true);
						return viewValue;
					} else {
						_setError(element[0] ,errorMsg[scope.chk]);
						ctrl.$setValidity('checked', false);
						return undefined;
					}
				});
				ctrl.$formatters.push(function (viewValue) {
					//if(viewValue.length<1){ return }
					if ( ptrn[scope.chk].test(viewValue) ) {
						_setError(element[0] , '', true);
						ctrl.$setValidity('checked', true);
						return viewValue;
					} else {
						_setError(element[0] ,errorMsg[scope.chk]);
						ctrl.$setValidity('checked', false);
						return undefined;
					}
				});
        }
    };
})
