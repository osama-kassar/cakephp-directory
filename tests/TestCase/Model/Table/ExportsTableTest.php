<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ExportsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ExportsTable Test Case
 */
class ExportsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ExportsTable
     */
    public $Exports;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.exports',
        'app.companies',
        'app.users',
        'app.profiles',
        'app.countries',
        'app.child_countries',
        'app.categories',
        'app.child_categories',
        'app.subcategories'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Exports') ? [] : ['className' => 'App\Model\Table\ExportsTable'];
        $this->Exports = TableRegistry::get('Exports', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Exports);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
